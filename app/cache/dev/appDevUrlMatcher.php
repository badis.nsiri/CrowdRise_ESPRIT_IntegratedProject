<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/js/35a8e64')) {
            // _assetic_35a8e64
            if ($pathinfo === '/js/35a8e64.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '35a8e64',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_35a8e64',);
            }

            // _assetic_35a8e64_0
            if ($pathinfo === '/js/35a8e64_comments_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '35a8e64',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_35a8e64_0',);
            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // crowd_rise_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'crowd_rise_homepage');
            }

            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\DefaultController::indexAction',  '_route' => 'crowd_rise_homepage',);
        }

        // crowd_rise_frontOffice
        if ($pathinfo === '/indexFrontOffice') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexFrontOfficeAction',  '_route' => 'crowd_rise_frontOffice',);
        }

        // projet
        if ($pathinfo === '/projet') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexProjetAction',  '_route' => 'projet',);
        }

        // projet1
        if (0 === strpos($pathinfo, '/ProjetById') && preg_match('#^/ProjetById/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'projet1')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexProjetByIdAction',));
        }

        // deleteMyCompte
        if (0 === strpos($pathinfo, '/deleteMYCompte') && preg_match('#^/deleteMYCompte/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteMyCompte')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseLoginController::deleteCompteAction',));
        }

        if (0 === strpos($pathinfo, '/probleme')) {
            // probleme
            if (preg_match('#^/probleme/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'probleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexProblemeAction',));
            }

            // problemeRes
            if (0 === strpos($pathinfo, '/problemeReso') && preg_match('#^/problemeReso/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'problemeRes')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::ProblemeResAction',));
            }

        }

        if (0 === strpos($pathinfo, '/idee')) {
            // idee
            if (preg_match('#^/idee/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'idee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexIdeeAction',));
            }

            // AllIdee
            if ($pathinfo === '/idees') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::showAllIdeeAction',  '_route' => 'AllIdee',);
            }

        }

        // AllProjet
        if ($pathinfo === '/projets') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::showAllProjetAction',  '_route' => 'AllProjet',);
        }

        // AllEvenement
        if ($pathinfo === '/evenements') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::showAllEvenementAction',  '_route' => 'AllEvenement',);
        }

        // AllProbleme
        if ($pathinfo === '/problemes') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::showAllProblemeAction',  '_route' => 'AllProbleme',);
        }

        if (0 === strpos($pathinfo, '/formulaire')) {
            // formulaireProjet
            if ($pathinfo === '/formulaireProjet') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexFormulaireProjetAction',  '_route' => 'formulaireProjet',);
            }

            // formulaireIdee
            if ($pathinfo === '/formulaireidee') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexFormulaireIdeeAction',  '_route' => 'formulaireIdee',);
            }

            // formulaireProbleme
            if ($pathinfo === '/formulaireProbleme') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexFormulaireProblemeAction',  '_route' => 'formulaireProbleme',);
            }

        }

        if (0 === strpos($pathinfo, '/modifier')) {
            if (0 === strpos($pathinfo, '/modifierpro')) {
                // modifier_probleme
                if (0 === strpos($pathinfo, '/modifierprobleme') && preg_match('#^/modifierprobleme/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier_probleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexModifierProblemAction',));
                }

                // modifier_projet
                if (0 === strpos($pathinfo, '/modifierprojet') && preg_match('#^/modifierprojet/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier_projet')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexModifierProjetAction',));
                }

            }

            // modifier_idee
            if (0 === strpos($pathinfo, '/modifieridee') && preg_match('#^/modifieridee/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier_idee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::indexModifierIdeeAction',));
            }

        }

        // EvenementFormulaire
        if ($pathinfo === '/formulaireEvenement') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::indexFormulaireEvennementAction',  '_route' => 'EvenementFormulaire',);
        }

        // modifier_evenement
        if (0 === strpos($pathinfo, '/modifierEvenement') && preg_match('#^/modifierEvenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier_evenement')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::indexModifierEvennementAction',));
        }

        // evenement
        if (0 === strpos($pathinfo, '/Evenement') && preg_match('#^/Evenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'evenement')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::indexEvennementAction',));
        }

        if (0 === strpos($pathinfo, '/supprimer')) {
            // supprimer_evenement
            if (0 === strpos($pathinfo, '/supprimerEvenement') && preg_match('#^/supprimerEvenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer_evenement')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::supprimerEvenementAction',));
            }

            if (0 === strpos($pathinfo, '/supprimerpro')) {
                // supprimer_projet
                if (0 === strpos($pathinfo, '/supprimerprojet') && preg_match('#^/supprimerprojet/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer_projet')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::supprimerProjetAction',));
                }

                // supprimer_probleme
                if (0 === strpos($pathinfo, '/supprimerprobleme') && preg_match('#^/supprimerprobleme/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer_probleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::supprimerProblemeAction',));
                }

            }

            // supprimer_idee
            if (0 === strpos($pathinfo, '/supprimeridee') && preg_match('#^/supprimeridee/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer_idee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::supprimerIdeeAction',));
            }

        }

        if (0 === strpos($pathinfo, '/delete')) {
            // deleteCommentaireIdee
            if (0 === strpos($pathinfo, '/deleteIdee') && preg_match('#^/deleteIdee/(?P<id>[^/]++)/(?P<id2>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteCommentaireIdee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::DeleteCommentaireByIdeeAction',));
            }

            if (0 === strpos($pathinfo, '/deletePro')) {
                // deleteCommentaireProjet
                if (0 === strpos($pathinfo, '/deleteProjet') && preg_match('#^/deleteProjet/(?P<id>[^/]++)/(?P<id2>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteCommentaireProjet')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::DeleteCommentaireByProjetAction',));
                }

                // deleteCommentaireProbleme
                if (0 === strpos($pathinfo, '/deleteProbleme') && preg_match('#^/deleteProbleme/(?P<id>[^/]++)/(?P<id2>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteCommentaireProbleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::DeleteCommentaireByProblemeAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/addCommentaire')) {
            // addCommentaireIdee
            if (0 === strpos($pathinfo, '/addCommentaireIdee') && preg_match('#^/addCommentaireIdee/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'addCommentaireIdee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::AjouterCommentaireIdeeAction',));
            }

            if (0 === strpos($pathinfo, '/addCommentairePro')) {
                // addCommentaireProjet
                if (0 === strpos($pathinfo, '/addCommentaireProjet') && preg_match('#^/addCommentaireProjet/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'addCommentaireProjet')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::AjouterCommentaireProjetAction',));
                }

                // addCommentaireProbleme
                if (0 === strpos($pathinfo, '/addCommentaireProbleme') && preg_match('#^/addCommentaireProbleme/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'addCommentaireProbleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::AjouterCommentaireProblemeAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/updateCommentaire')) {
            if (0 === strpos($pathinfo, '/updateCommentairePro')) {
                // updateCommentaireProjet
                if (0 === strpos($pathinfo, '/updateCommentaireProjet') && preg_match('#^/updateCommentaireProjet/(?P<id>[^/]++)/(?P<id2>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateCommentaireProjet')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::ModifierCommentaireProjetAction',));
                }

                // updateCommentaireProbleme
                if (0 === strpos($pathinfo, '/updateCommentaireProbleme') && preg_match('#^/updateCommentaireProbleme/(?P<id>[^/]++)/(?P<id2>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateCommentaireProbleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::ModifierCommentaireProblemeAction',));
                }

            }

            // updateCommentaireIdee
            if (0 === strpos($pathinfo, '/updateCommentaireIdee') && preg_match('#^/updateCommentaireIdee/(?P<id>[^/]++)/(?P<id2>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateCommentaireIdee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::ModifierCommentaireIdeeAction',));
            }

        }

        if (0 === strpos($pathinfo, '/commentPro')) {
            // commentProjet
            if (0 === strpos($pathinfo, '/commentProjet') && preg_match('#^/commentProjet/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentProjet')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::showCommentaireByProjetAction',));
            }

            // commentProbleme
            if (0 === strpos($pathinfo, '/commentProbleme') && preg_match('#^/commentProbleme/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'commentProbleme')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CommentaireController::showCommentaireByProblemeAction',));
            }

        }

        // login1
        if ($pathinfo === '/login1') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseLoginController::loginAction',  '_route' => 'login1',);
        }

        // acceuilLogin
        if ($pathinfo === '/acceuilL') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::loginAcceuilAction',  '_route' => 'acceuilLogin',);
        }

        // erreurLogin
        if ($pathinfo === '/erreurLogin') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::erreurLoginAction',  '_route' => 'erreurLogin',);
        }

        // projet2
        if (0 === strpos($pathinfo, '/ProjetById') && preg_match('#^/ProjetById/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'projet2')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\FinancementController::DonAction',));
        }

        // Don
        if (0 === strpos($pathinfo, '/Don') && preg_match('#^/Don/(?P<id>[^/]++)/(?P<iduser>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Don');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Don')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\FinancementController::DonAction',));
        }

        // Pret
        if (0 === strpos($pathinfo, '/Pret') && preg_match('#^/Pret/(?P<id>[^/]++)/(?P<iduser>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Pret');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pret')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\FinancementController::PretAction',));
        }

        // Recompense
        if (0 === strpos($pathinfo, '/Recompense') && preg_match('#^/Recompense/(?P<id>[^/]++)/(?P<iduser>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Recompense');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Recompense')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\FinancementController::RecompenseAction',));
        }

        // Investissement
        if (0 === strpos($pathinfo, '/Investissement') && preg_match('#^/Investissement/(?P<id>[^/]++)/(?P<iduser>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Investissement');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Investissement')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\FinancementController::InvestissementAction',));
        }

        // showProfil
        if ($pathinfo === '/show') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'showProfil',);
        }

        // editProfil
        if ($pathinfo === '/edit') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'editProfil',);
        }

        // changePassword
        if ($pathinfo === '/changePassword') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'changePassword',);
        }

        // ResettingFormSendMail
        if ($pathinfo === '/sendEmail') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'ResettingFormSendMail',);
        }

        // SearchUser
        if ($pathinfo === '/SearchUser') {
            return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::searchUserAction',  '_route' => 'SearchUser',);
        }

        if (0 === strpos($pathinfo, '/image')) {
            // image_idee
            if (0 === strpos($pathinfo, '/imageIdee') && preg_match('#^/imageIdee/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'image_idee')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::AfficheImageIdeeAction',));
            }

            // image_evenement
            if (0 === strpos($pathinfo, '/imageEvenement') && preg_match('#^/imageEvenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'image_evenement')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::AfficheImageEvenementAction',));
            }

            // image_user
            if (0 === strpos($pathinfo, '/imageUser') && preg_match('#^/imageUser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'image_user')), array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::AfficheImageUserAction',));
            }

        }

        if (0 === strpos($pathinfo, '/list')) {
            // list_idee
            if ($pathinfo === '/listIdees') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::listIdeesAction',  '_route' => 'list_idee',);
            }

            // list_Evenement
            if ($pathinfo === '/listEvenements') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::listEvenementAction',  '_route' => 'list_Evenement',);
            }

            // list_probleme
            if ($pathinfo === '/listProblemes') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::listProblemesAction',  '_route' => 'list_probleme',);
            }

        }

        if (0 === strpos($pathinfo, '/Search')) {
            // SearchIdee
            if ($pathinfo === '/SearchIdee') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\CrowdRiseMainController::rechercherIdeeAction',  '_route' => 'SearchIdee',);
            }

            // SearchEvenement
            if ($pathinfo === '/SearchEvenement') {
                return array (  '_controller' => 'CrowdRiseBundle\\Controller\\EvennementController::rechercherEvenementAction',  '_route' => 'SearchEvenement',);
            }

        }

        if (0 === strpos($pathinfo, '/ap')) {
            // homepage
            if ($pathinfo === '/app/example') {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            }

            if (0 === strpos($pathinfo, '/api/threads')) {
                // fos_comment_new_threads
                if (0 === strpos($pathinfo, '/api/threads/new') && preg_match('#^/api/threads/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_new_threads;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadsAction',  '_format' => 'html',));
                }
                not_fos_comment_new_threads:

                // fos_comment_edit_thread_commentable
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/commentable/edit(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_edit_thread_commentable;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_edit_thread_commentable')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::editThreadCommentableAction',  '_format' => 'html',));
                }
                not_fos_comment_edit_thread_commentable:

                // fos_comment_new_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_new_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_new_thread_comments:

                // fos_comment_remove_thread_comment
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/remove(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_remove_thread_comment;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_remove_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::removeThreadCommentAction',  '_format' => 'html',));
                }
                not_fos_comment_remove_thread_comment:

                // fos_comment_edit_thread_comment
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/edit(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_edit_thread_comment;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_edit_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::editThreadCommentAction',  '_format' => 'html',));
                }
                not_fos_comment_edit_thread_comment:

                // fos_comment_new_thread_comment_votes
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_new_thread_comment_votes;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadCommentVotesAction',  '_format' => 'html',));
                }
                not_fos_comment_new_thread_comment_votes:

                // fos_comment_get_thread
                if (preg_match('#^/api/threads/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread:

                // fos_comment_get_threads
                if (preg_match('#^/api/threads(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_threads;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadsActions',  '_format' => 'html',));
                }
                not_fos_comment_get_threads:

                // fos_comment_post_threads
                if (preg_match('#^/api/threads(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_comment_post_threads;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadsAction',  '_format' => 'html',));
                }
                not_fos_comment_post_threads:

                // fos_comment_patch_thread_commentable
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/commentable(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PATCH') {
                        $allow[] = 'PATCH';
                        goto not_fos_comment_patch_thread_commentable;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_patch_thread_commentable')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::patchThreadCommentableAction',  '_format' => 'html',));
                }
                not_fos_comment_patch_thread_commentable:

                // fos_comment_get_thread_comment
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread_comment;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread_comment:

                // fos_comment_patch_thread_comment_state
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/state(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PATCH') {
                        $allow[] = 'PATCH';
                        goto not_fos_comment_patch_thread_comment_state;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_patch_thread_comment_state')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::patchThreadCommentStateAction',  '_format' => 'html',));
                }
                not_fos_comment_patch_thread_comment_state:

                // fos_comment_put_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_fos_comment_put_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_put_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::putThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_put_thread_comments:

                // fos_comment_get_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread_comments:

                // fos_comment_post_thread_comments
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_comment_post_thread_comments;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadCommentsAction',  '_format' => 'html',));
                }
                not_fos_comment_post_thread_comments:

                // fos_comment_get_thread_comment_votes
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_comment_get_thread_comment_votes;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentVotesAction',  '_format' => 'html',));
                }
                not_fos_comment_get_thread_comment_votes:

                // fos_comment_post_thread_comment_votes
                if (preg_match('#^/api/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_comment_post_thread_comment_votes;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadCommentVotesAction',  '_format' => 'html',));
                }
                not_fos_comment_post_thread_comment_votes:

            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/admin')) {
            // easyadmin
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'easyadmin');
                }

                return array (  '_controller' => 'JavierEguiluz\\Bundle\\EasyAdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'easyadmin',);
            }

            // admin
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin');
                }

                return array (  '_controller' => 'JavierEguiluz\\Bundle\\EasyAdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'admin',);
            }

        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
