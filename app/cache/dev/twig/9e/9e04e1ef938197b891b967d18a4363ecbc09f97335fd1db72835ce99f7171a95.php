<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:showAllProbleme.html.twig */
class __TwigTemplate_0271b07d618e5bfc8338b6e539ba39d657de3ecf1aa62af0cd089be7a4b8787d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2><a href=\"";
        echo $this->env->getExtension('routing')->getPath("list_probleme");
        echo "\">Problème</a></h2>



<hr />

<div class=\"row\">

    <ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">


        <div role=\"main\" class=\"main shop\">

            <div class=\"container\">



                <div class=\"row\">
                    <div class=\"col-md-12\">

                        <div class=\"row featured-boxes\">
                            <div class=\"col-md-12\">
                                <div class=\"featured-box featured-box-secundary featured-box-cart\">
                                    <div class=\"box-content\">
                                        <form method=\"post\" action=\"\">
                                            <table cellspacing=\"0\" class=\"shop_table cart\">
                                                <thead>
                                                    <tr>
                                                        <th class=\"product-remove\">
                                                            &nbsp;
                                                        </th>

                                                        <th class=\"product-name\">
                                                            Problème
                                                        </th>
                                                        <th class=\"product-price\">
                                                            Thême
                                                        </th>
                                                        <th class=\"product-quantity\">

                                                        </th>
                                                        <th class=\"product-subtotal\">
                                                            Ajouté par
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    ";
        // line 48
        $context["c"] = 0;
        // line 49
        echo "                                                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["problemes"]) ? $context["problemes"] : $this->getContext($context, "problemes")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "  
                                                            ";
            // line 50
            $context["c"] = ((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")) + 1);
            // line 51
            echo "                                                                ";
            if (((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")) <= 4)) {
                // line 52
                echo "                                                                    <tr class=\"cart_table_item\">
                                                                        <td class=\"product-remove\">
                                                                            <span class=\"onsale\">";
                // line 54
                if (($this->getAttribute($context["i"], "etat", array()) == "Resolu")) {
                    echo "                                        


                                                                                <ul class=\"list list-skills icons list-unstyled list-inline\">
                                                                                    <li><i class=\"fa fa-check-circle\"> </i>    </li> <br>

                                                                                </ul>


                                                                                </div>";
                }
                // line 63
                echo "</span>

                                                                        </td>

                                                                        <td class=\"product-name\">
                                                                            <a href=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("probleme", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "titre", array()), "html", null, true);
                echo "</a>
                                                                        </td>
                                                                        <td class=\"product-price\">
                                                                            <span class=\"amount\">";
                // line 71
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "theme", array()), "html", null, true);
                echo "</span>
                                                                        </td>
                                                                        <td class=\"product-quantity\">

                                                                        </td>
                                                                        <td class=\"product-subtotal\">
                                                                            <span class=\"amount\">";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "user", array()), "username", array()), "html", null, true);
                echo "</span>
                                                                        </td>
                                                                    </tr>

                                                                ";
            }
            // line 82
            echo "                                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "
                                                                </tbody>
                                                            </table>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        </ul>







                                    </div>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:showAllProbleme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 83,  133 => 82,  125 => 77,  116 => 71,  108 => 68,  101 => 63,  88 => 54,  84 => 52,  81 => 51,  79 => 50,  72 => 49,  70 => 48,  19 => 1,);
    }
}
/* <h2><a href="{{path('list_probleme')}}">Problème</a></h2>*/
/* */
/* */
/* */
/* <hr />*/
/* */
/* <div class="row">*/
/* */
/*     <ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/* */
/* */
/*         <div role="main" class="main shop">*/
/* */
/*             <div class="container">*/
/* */
/* */
/* */
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/* */
/*                         <div class="row featured-boxes">*/
/*                             <div class="col-md-12">*/
/*                                 <div class="featured-box featured-box-secundary featured-box-cart">*/
/*                                     <div class="box-content">*/
/*                                         <form method="post" action="">*/
/*                                             <table cellspacing="0" class="shop_table cart">*/
/*                                                 <thead>*/
/*                                                     <tr>*/
/*                                                         <th class="product-remove">*/
/*                                                             &nbsp;*/
/*                                                         </th>*/
/* */
/*                                                         <th class="product-name">*/
/*                                                             Problème*/
/*                                                         </th>*/
/*                                                         <th class="product-price">*/
/*                                                             Thême*/
/*                                                         </th>*/
/*                                                         <th class="product-quantity">*/
/* */
/*                                                         </th>*/
/*                                                         <th class="product-subtotal">*/
/*                                                             Ajouté par*/
/*                                                         </th>*/
/*                                                     </tr>*/
/*                                                 </thead>*/
/*                                                 <tbody>*/
/*                                                     {% set c=0   %}*/
/*                                                         {% for i in problemes %}  */
/*                                                             {% set c=c+1 %}*/
/*                                                                 {% if c<=4 %}*/
/*                                                                     <tr class="cart_table_item">*/
/*                                                                         <td class="product-remove">*/
/*                                                                             <span class="onsale">{% if i.etat=='Resolu' %}                                        */
/* */
/* */
/*                                                                                 <ul class="list list-skills icons list-unstyled list-inline">*/
/*                                                                                     <li><i class="fa fa-check-circle"> </i>    </li> <br>*/
/* */
/*                                                                                 </ul>*/
/* */
/* */
/*                                                                                 </div>{% endif %}</span>*/
/* */
/*                                                                         </td>*/
/* */
/*                                                                         <td class="product-name">*/
/*                                                                             <a href="{{ path('probleme',{'id':i.id})}}">{{i.titre}}</a>*/
/*                                                                         </td>*/
/*                                                                         <td class="product-price">*/
/*                                                                             <span class="amount">{{i.theme}}</span>*/
/*                                                                         </td>*/
/*                                                                         <td class="product-quantity">*/
/* */
/*                                                                         </td>*/
/*                                                                         <td class="product-subtotal">*/
/*                                                                             <span class="amount">{{i.user.username}}</span>*/
/*                                                                         </td>*/
/*                                                                     </tr>*/
/* */
/*                                                                 {% endif %}*/
/*                                                                 {% endfor %}*/
/* */
/*                                                                 </tbody>*/
/*                                                             </table>*/
/*                                                         </form>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/*                                         </ul>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     </div>*/
