<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig */
class __TwigTemplate_354a8ff959f0a50dab10ee9c729bcc0ee91776490c97381b08b0dab6beb741e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig", 8)->display($context);
        echo " 
        <div class=\"body\">


            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                     <li class=\"active\">Evenement</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Evenement</h1>
                            </div>
                        </div>
                    </div>
            </section>


                <h2>Evenement</h2>
<div class=\"search\">
    <form id=\"searchForm\" action=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("SearchEvenement");
        echo "\" method=\"POST\">
        <div class=\"input-group\">
            <input type=\"text\" class=\"form-control search\" name=\"q\" id=\"q\" placeholder=\"Search...\" required>
            <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
            </span>
        </div>
    </form>
</div>
<hr />

<div class=\"row\">

    <ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">
        ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "  
            <li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
                <div class=\"portfolio-item img-thumbnail\">
                    <a href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("evenement", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\" class=\"thumb-info\">
                        <img alt=\"\" class=\"img-responsive\" src=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
            echo "\">
                        <span class=\"thumb-info-title\">
                            <span class=\"thumb-info-inner\">";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "nom", array()), "html", null, true);
            echo "</span>
                            <span class=\"thumb-info-type\">CrowdRise</span>
                        </span>
                        <span class=\"thumb-info-action\">
                            <span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
                        </span>
                    </a>
                </div>
            </li> 
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "    </ul>
</div>
            <section class=\"call-to-action featured footer\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"center\">
                            <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987\" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
                        </div>
                    </div>
                </div>
            </section>

            ";
        // line 77
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig", 77)->display($context);
        // line 78
        echo "        </div>
        ";
        // line 79
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig", 79)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 79,  126 => 78,  124 => 77,  110 => 65,  94 => 55,  89 => 53,  85 => 52,  77 => 49,  60 => 35,  30 => 8,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %} */
/*         <div class="body">*/
/* */
/* */
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                      <li class="active">Evenement</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Evenement</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*             </section>*/
/* */
/* */
/*                 <h2>Evenement</h2>*/
/* <div class="search">*/
/*     <form id="searchForm" action="{{path('SearchEvenement')}}" method="POST">*/
/*         <div class="input-group">*/
/*             <input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>*/
/*             <span class="input-group-btn">*/
/*                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>*/
/*             </span>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* <hr />*/
/* */
/* <div class="row">*/
/* */
/*     <ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/*         {% for i in evenements %}  */
/*             <li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/*                 <div class="portfolio-item img-thumbnail">*/
/*                     <a href="{{ path('evenement',{'id':i.id})}}" class="thumb-info">*/
/*                         <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/*                         <span class="thumb-info-title">*/
/*                             <span class="thumb-info-inner">{{i.nom}}</span>*/
/*                             <span class="thumb-info-type">CrowdRise</span>*/
/*                         </span>*/
/*                         <span class="thumb-info-action">*/
/*                             <span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/*                         </span>*/
/*                     </a>*/
/*                 </div>*/
/*             </li> */
/*         {% endfor %}*/
/*     </ul>*/
/* </div>*/
/*             <section class="call-to-action featured footer">*/
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="center">*/
/*                             <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
