<?php

/* CrowdRiseBundle:Profile:edit.html.twig */
class __TwigTemplate_95f62103ff1d1332e33aef9c6bdd76806392f86e4cc1846a73bc724456a8ed0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " 
";
        // line 2
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "CrowdRiseBundle:Profile:edit.html.twig", 2)->display($context);
        // line 3
        echo " 
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 3,  22 => 2,  19 => 1,);
    }
}
/*  */
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/*  */
/* */
