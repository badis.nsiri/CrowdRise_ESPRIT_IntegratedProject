<?php

/* CrowdRiseBundle:image:affiche.html.twig */
class __TwigTemplate_450b3cbd5af1d35c0e1e1c0bf8fc721f5ef1c6c6ad44a2a97a00b06c0294c844 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div>

<img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->env->getExtension('routing')->getPath("my_image_route", array("id" => $this->getAttribute((isset($context["images"]) ? $context["images"] : $this->getContext($context, "images")), "id", array())))), "html", null, true);
        echo "\"/>

</div>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:image:affiche.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 4,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <div>*/
/* */
/* <img src="{{ asset(path('my_image_route', {'id': images.id})) }}"/>*/
/* */
/* </div>*/
