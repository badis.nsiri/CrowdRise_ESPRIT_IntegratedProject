<?php

/* CrowdRiseBundle:Profile:edit_content.html.twig */
class __TwigTemplate_46014f875e9310786a0c9fc6fcdc1865792dd2bcf4798e17b5daaf690839a4f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " 
";
        // line 3
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 6
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:Profile:edit_content.html.twig", 6)->display($context);
        // line 7
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 12
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:Profile:edit_content.html.twig", 12)->display($context);
        // line 13
        echo "            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Features</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Modifier mon profile</h1>
                            </div>
                        </div>
                    </div>
                </section>


                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <section class=\"panel\">
                                <header class=\"panel-heading\">


                                    <h2 class=\"panel-title\"></h2>
                                </header>

                                <div class=\"panel-body\">


                                    <form class=\"form-horizontal form-bordered\" method=\"POST\">
                                        ";
        // line 48
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("fos_user_profile_edit"), "attr" => array("class" => "fos_user_profile_edit")));
        echo "
                                        <p color=\"red\">  ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "current_password", array()), 'errors');
        echo " </p>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\" for=\"inputDefault\">Nom :</label>
                                            <div class=\"col-md-6\">
                                                ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\" for=\"inputDefault\">Prenom :</label>
                                            <div class=\"col-md-6\">
                                                ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\" for=\"inputDefault\">Date de naissance  :</label>
                                            <div class=\"col-md-6\">
                                                ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateNaiss", array()), 'widget');
        echo "  

                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\">Left Icon</label>
                                            <div class=\"col-md-6\">
                                                <div class=\"input-group input-group-icon\">
                                                    <span class=\"input-group-addon\">
                                                        <span class=\"icon\"><i class=\"fa fa-user\"></i></span>
                                                    </span>
                                                    ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\" for=\"inputDefault\">Num�ro de t�l�phone  :</label>
                                            <div class=\"col-md-6\">
                                                ";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tel", array()), 'widget');
        echo "  

                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\" for=\"inputDefault\">Date de naissance  :</label>
                                            <div class=\"col-md-6\">
                                                ";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), 'widget');
        echo "  

                                            </div>
                                        </div>


                                        <div class=\"form-group\">
                                            <label class=\" col-md-3 control-label\">Email : </label>
                                            <div class=\"col-lg-6\">
                                                <p class=\"form-control-static\">";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "</p>
                                            </div>
                                        </div>

                                        <div class=\"form-group\">
                                            <label class=\" col-md-3 control-label\"> ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "current_password", array()), 'label');
        echo " </label>
                                            <div class=\"col-lg-6\">
                                                <td></td>
                                               ";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "current_password", array()), 'widget');
        echo "
                                            
                                            </div>
                                        </div>
                                        <div>
                                            <input type=\"submit\" value=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enregistrer"), "html", null, true);
        echo "\" />
                                        </div>
                                        ";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo " 
                                    </form>

                            </section>

                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-lg-12\">


                        </div>

                        ";
        // line 130
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:Profile:edit_content.html.twig", 130)->display($context);
        // line 131
        echo "                    </div>

                    ";
        // line 133
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:Profile:edit_content.html.twig", 133)->display($context);
        // line 134
        echo "                    </body>
                    </html>
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Profile:edit_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 134,  202 => 133,  198 => 131,  196 => 130,  179 => 116,  174 => 114,  166 => 109,  160 => 106,  152 => 101,  140 => 92,  130 => 85,  120 => 78,  106 => 67,  96 => 60,  86 => 53,  79 => 49,  75 => 48,  38 => 13,  36 => 12,  29 => 7,  27 => 6,  22 => 3,  19 => 1,);
    }
}
/*  */
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Features</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Modifier mon profile</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/* */
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="col-lg-12">*/
/*                             <section class="panel">*/
/*                                 <header class="panel-heading">*/
/* */
/* */
/*                                     <h2 class="panel-title"></h2>*/
/*                                 </header>*/
/* */
/*                                 <div class="panel-body">*/
/* */
/* */
/*                                     <form class="form-horizontal form-bordered" method="POST">*/
/*                                         {{ form_start(form, { 'action': path('fos_user_profile_edit'), 'attr': { 'class': 'fos_user_profile_edit' } }) }}*/
/*                                         <p color="red">  {{ form_errors(form.current_password) }} </p>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label" for="inputDefault">Nom :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 {{ form_widget(form.nom,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label" for="inputDefault">Prenom :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 {{ form_widget(form.prenom,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label" for="inputDefault">Date de naissance  :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 {{ form_widget(form.dateNaiss)}}  */
/* */
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label">Left Icon</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 <div class="input-group input-group-icon">*/
/*                                                     <span class="input-group-addon">*/
/*                                                         <span class="icon"><i class="fa fa-user"></i></span>*/
/*                                                     </span>*/
/*                                                     {{ form_widget(form.username,{"attr": {'class': "form-control"}} )}}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label" for="inputDefault">Num�ro de t�l�phone  :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 {{ form_widget(form.tel)}}  */
/* */
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label" for="inputDefault">Date de naissance  :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 {{ form_widget(form.country)}}  */
/* */
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/*                                         <div class="form-group">*/
/*                                             <label class=" col-md-3 control-label">Email : </label>*/
/*                                             <div class="col-lg-6">*/
/*                                                 <p class="form-control-static">{{ form_widget(form.email) }}</p>*/
/*                                             </div>*/
/*                                         </div>*/
/* */
/*                                         <div class="form-group">*/
/*                                             <label class=" col-md-3 control-label"> {{ form_label(form.current_password )}} </label>*/
/*                                             <div class="col-lg-6">*/
/*                                                 <td></td>*/
/*                                                {{ form_widget(form.current_password) }}*/
/*                                             */
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div>*/
/*                                             <input type="submit" value="{{ 'Enregistrer'|trans }}" />*/
/*                                         </div>*/
/*                                         {{ form_rest(form) }} */
/*                                     </form>*/
/* */
/*                             </section>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-lg-12">*/
/* */
/* */
/*                         </div>*/
/* */
/*                         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*                     </div>*/
/* */
/*                     {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}*/
/*                     </body>*/
/*                     </html>*/
/* */
