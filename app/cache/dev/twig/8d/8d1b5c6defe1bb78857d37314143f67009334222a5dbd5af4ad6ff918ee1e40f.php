<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig */
class __TwigTemplate_89b7a247aea6b129c72bfcb50d9a60ae75fb07de819e2f2142d662a06327e240 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 11
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig", 11)->display($context);
        // line 12
        echo "            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Evenement</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Evenement</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class=\"container\">

                    <div class=\"portfolio-title\">
                        <div class=\"row\">
                            <div class=\"portfolio-nav-all col-md-1\">
                                <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("list_Evenement");
        echo "\" data-tooltip data-original-title=\"Back to list\"><i class=\"fa fa-th\"></i></a>
                            </div>
                            <div class=\"col-md-10 center\">
                                <h2 class=\"shorter\">";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "nom", array()), "html", null, true);
        echo "</h2>
                            </div>






                        </div>
                    </div>

                    <hr class=\"tall\">

                    <div class=\"row\">
                        <div class=\"col-md-4\">

                            <div class=\"owl-carousel\" data-plugin-options='{\"items\": 1}'>
                                <div>
                                    <div class=\"thumbnail\">
                                        <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("image_evenement", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
        echo "\">
                                    </div>
                                </div>
                                <div>
                                    <div class=\"thumbnail\">
                                        <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-1.jpg"), "html", null, true);
        echo "\">
                                    </div>
                                </div>
                                <div>
                                    <div class=\"thumbnail\">
                                        <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-2.jpg"), "html", null, true);
        echo "\">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class=\"col-md-8\">

                            <div class=\"portfolio-info\">
                                <div class=\"row\">
                                    <div class=\"col-md-12 center\">
                                        <ul>

                                            <li>
                                                <i class=\"fa fa-calendar\"></i>Debut : ";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "dateDebut", array()), "format", array(0 => "M  j, Y"), "method"), "html", null, true);
        echo " <br> ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "heureDebut", array()), "format", array(0 => "h:m a"), "method"), "html", null, true);
        echo "
                                            </li>
                                            <li>
                                                <i class=\"fa fa-calendar\"></i> Fin :  ";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "dateFin", array()), "format", array(0 => "M  j, Y"), "method"), "html", null, true);
        echo " <br> ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "heureFin", array()), "format", array(0 => "h:m a"), "method"), "html", null, true);
        echo "
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <h4>Evenement <strong>Description</strong></h4>


                            <p class=\"taller\">";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "description", array()), "html", null, true);
        echo "</p>
                            <ul class=\"portfolio-details\">
                                <li>

                                    <p><strong>Thême:</strong></p>

                                    <ul class=\"list list-skills icons list-unstyled list-inline\">


                                        <li><i class=\"fa fa-check-circle\"> </i>  <li>";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "theme", array()), "html", null, true);
        echo "</li> <br>

                                    </ul>
                                </li>
                            </ul>


                            <div class=\"form-group\">



                                <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Commentaire</label>
                                <div class=\"col-md-6\">
                                    <textarea class=\"form-control\" rows=\"3\" data-plugin-maxlength maxlength=\"5000 \"></textarea>


                                </div>




                            </div>


                            ";
        // line 130
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "user", array()), "id", array()))) {
            // line 131
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_evenement", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Modifier</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
                                <a href=\"";
            // line 132
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer_evenement", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Supprimer</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
                            ";
        }
        // line 134
        echo "
                        </div>
                    </div>

                    <hr class=\"tall\" />




                </div>

            </div>

            <section class=\"call-to-action featured footer\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"center\">
                            <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987\" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
                        </div>
                    </div>
                </div>
            </section>

            ";
        // line 157
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig", 157)->display($context);
        // line 158
        echo "        </div>
        ";
        // line 159
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig", 159)->display($context);
        echo "                                                                       
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 159,  225 => 158,  223 => 157,  198 => 134,  193 => 132,  188 => 131,  186 => 130,  159 => 106,  147 => 97,  131 => 86,  123 => 83,  106 => 69,  98 => 64,  90 => 59,  68 => 40,  62 => 37,  35 => 12,  33 => 11,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Evenement</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Evenement</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <div class="container">*/
/* */
/*                     <div class="portfolio-title">*/
/*                         <div class="row">*/
/*                             <div class="portfolio-nav-all col-md-1">*/
/*                                 <a href="{{path('list_Evenement')}}" data-tooltip data-original-title="Back to list"><i class="fa fa-th"></i></a>*/
/*                             </div>*/
/*                             <div class="col-md-10 center">*/
/*                                 <h2 class="shorter">{{m.nom}}</h2>*/
/*                             </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall">*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-4">*/
/* */
/*                             <div class="owl-carousel" data-plugin-options='{"items": 1}'>*/
/*                                 <div>*/
/*                                     <div class="thumbnail">*/
/*                                         <img alt="" class="img-responsive" src="{{path('image_evenement',{'id':m.id})}}">*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div>*/
/*                                     <div class="thumbnail">*/
/*                                         <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-1.jpg')}}">*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div>*/
/*                                     <div class="thumbnail">*/
/*                                         <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-2.jpg')}}">*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/*                         </div>*/
/*                         <div class="col-md-8">*/
/* */
/*                             <div class="portfolio-info">*/
/*                                 <div class="row">*/
/*                                     <div class="col-md-12 center">*/
/*                                         <ul>*/
/* */
/*                                             <li>*/
/*                                                 <i class="fa fa-calendar"></i>Debut : {{m.dateDebut.format('M  j, Y')}} <br> {{m.heureDebut.format('h:m a')}}*/
/*                                             </li>*/
/*                                             <li>*/
/*                                                 <i class="fa fa-calendar"></i> Fin :  {{m.dateFin.format('M  j, Y')}} <br> {{m.heureFin.format('h:m a')}}*/
/*                                             </li>*/
/* */
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/*                             <h4>Evenement <strong>Description</strong></h4>*/
/* */
/* */
/*                             <p class="taller">{{m.description}}</p>*/
/*                             <ul class="portfolio-details">*/
/*                                 <li>*/
/* */
/*                                     <p><strong>Thême:</strong></p>*/
/* */
/*                                     <ul class="list list-skills icons list-unstyled list-inline">*/
/* */
/* */
/*                                         <li><i class="fa fa-check-circle"> </i>  <li>{{m.theme}}</li> <br>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/* */
/*                             <div class="form-group">*/
/* */
/* */
/* */
/*                                 <label class="col-md-3 control-label" for="textareaDefault">Commentaire</label>*/
/*                                 <div class="col-md-6">*/
/*                                     <textarea class="form-control" rows="3" data-plugin-maxlength maxlength="5000 "></textarea>*/
/* */
/* */
/*                                 </div>*/
/* */
/* */
/* */
/* */
/*                             </div>*/
/* */
/* */
/*                             {% if app.user.id == m.user.id %}*/
/*                                 <a href="{{path('modifier_evenement', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Modifier</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*                                 <a href="{{path('supprimer_evenement', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Supprimer</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*                             {% endif %}*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall" />*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/* */
/*             </div>*/
/* */
/*             <section class="call-to-action featured footer">*/
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="center">*/
/*                             <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}                                                                       */
/*     </body>*/
/* </html>*/
