<?php

/* CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig */
class __TwigTemplate_cdc4bf2e6a96682b679461faa48882b266e4dbcc3d52d8b2ecdfd51cdf42d66e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Vendor -->
\t\t<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery.appear/jquery.appear.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery.easing/jquery.easing.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery-cookie/jquery-cookie.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/bootstrap/bootstrap.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/common/common.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery.validation/jquery.validation.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery.stellar/jquery.stellar.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jquery.gmap/jquery.gmap.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/isotope/jquery.isotope.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/owlcarousel/owl.carousel.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/jflickrfeed/jflickrfeed.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/magnific-popup/jquery.magnific-popup.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/vide/vide.js"), "html", null, true);
        echo "\"></script>
                
                <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery.appear/jquery.appear.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery.easing/jquery.easing.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery-cookie/jquery-cookie.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/bootstrap/bootstrap.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/common/common.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery.validation/jquery.validation.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery.stellar/jquery.stellar.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jquery.gmap/jquery.gmap.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/isotope/jquery.isotope.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/owlcarousel/owl.carousel.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/jflickrfeed/jflickrfeed.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/magnific-popup/jquery.magnific-popup.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/vide/vide.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<!-- Theme Base, Components and Settings -->
\t\t<script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/js/theme.js"), "html", null, true);
        echo "\"></script>
                
                <script src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/theme.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<!-- Specific Page Vendor and Views -->
\t\t<script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/circle-flip-slideshow/js/jquery.flipshow.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/js/views/view.home.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<!-- Theme Custom -->
\t\t<script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/js/custom.js"), "html", null, true);
        echo "\"></script>
                
                <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/custom.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<!-- Theme Initialization Files -->
\t\t<script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/js/theme.init.js"), "html", null, true);
        echo "\"></script>
                
                <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/theme.init.js"), "html", null, true);
        echo "\"></script>

\t\t<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
\t\t<script type=\"text/javascript\">
\t\t
\t\t\tvar _gaq = _gaq || [];
\t\t\t_gaq.push(['_setAccount', 'UA-12345678-1']);
\t\t\t_gaq.push(['_trackPageview']);
\t\t
\t\t\t(function() {
\t\t\tvar ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
\t\t\tga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
\t\t\tvar s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
\t\t\t})();
\t\t
\t\t</script>
\t\t -->
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 53,  185 => 51,  179 => 48,  174 => 46,  168 => 43,  164 => 42,  160 => 41,  156 => 40,  150 => 37,  145 => 35,  139 => 32,  135 => 31,  131 => 30,  127 => 29,  123 => 28,  119 => 27,  115 => 26,  111 => 25,  107 => 24,  103 => 23,  99 => 22,  95 => 21,  91 => 20,  87 => 19,  83 => 18,  78 => 16,  74 => 15,  70 => 14,  66 => 13,  62 => 12,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  42 => 7,  38 => 6,  34 => 5,  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <!-- Vendor -->*/
/* 		<script src="{{asset('FontOffice/vendor/jquery/jquery.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery.appear/jquery.appear.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery.easing/jquery.easing.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery-cookie/jquery-cookie.js') }}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/bootstrap/bootstrap.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/common/common.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery.validation/jquery.validation.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery.stellar/jquery.stellar.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jquery.gmap/jquery.gmap.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/isotope/jquery.isotope.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/owlcarousel/owl.carousel.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/jflickrfeed/jflickrfeed.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/vide/vide.js')}}"></script>*/
/*                 */
/*                 <script src="{{asset('vendor/jquery/jquery.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery.appear/jquery.appear.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery.easing/jquery.easing.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery-cookie/jquery-cookie.js')}}"></script>*/
/* 		<script src="{{asset('vendor/bootstrap/bootstrap.js')}}"></script>*/
/* 		<script src="{{asset('vendor/common/common.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery.validation/jquery.validation.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery.stellar/jquery.stellar.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jquery.gmap/jquery.gmap.js')}}"></script>*/
/* 		<script src="{{asset('vendor/isotope/jquery.isotope.js')}}"></script>*/
/* 		<script src="{{asset('vendor/owlcarousel/owl.carousel.js')}}"></script>*/
/* 		<script src="{{asset('vendor/jflickrfeed/jflickrfeed.js')}}"></script>*/
/* 		<script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>*/
/* 		<script src="{{asset('vendor/vide/vide.js')}}"></script>*/
/* 		*/
/* 		<!-- Theme Base, Components and Settings -->*/
/* 		<script src="{{asset('FontOffice/js/theme.js')}}"></script>*/
/*                 */
/*                 <script src="{{asset('js/theme.js')}}"></script>*/
/* 		*/
/* 		<!-- Specific Page Vendor and Views -->*/
/* 		<script src="{{asset('FontOffice/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/vendor/circle-flip-slideshow/js/jquery.flipshow.js')}}"></script>*/
/* 		<script src="{{asset('FontOffice/js/views/view.home.js')}}"></script>*/
/* 		*/
/* 		<!-- Theme Custom -->*/
/* 		<script src="{{asset('FontOffice/js/custom.js')}}"></script>*/
/*                 */
/*                 <script src="{{asset('js/custom.js')}}"></script>*/
/* 		*/
/* 		<!-- Theme Initialization Files -->*/
/* 		<script src="{{asset('FontOffice/js/theme.init.js')}}"></script>*/
/*                 */
/*                 <script src="{{asset('js/theme.init.js')}}"></script>*/
/* */
/* 		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.*/
/* 		<script type="text/javascript">*/
/* 		*/
/* 			var _gaq = _gaq || [];*/
/* 			_gaq.push(['_setAccount', 'UA-12345678-1']);*/
/* 			_gaq.push(['_trackPageview']);*/
/* 		*/
/* 			(function() {*/
/* 			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;*/
/* 			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';*/
/* 			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);*/
/* 			})();*/
/* 		*/
/* 		</script>*/
/* 		 -->*/
/* */
