<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig */
class __TwigTemplate_a7e419a1f2dda9aa014f76dab4380c5308a04401d78e688b07de79515220b43c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t\t
";
        // line 3
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 6
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig", 6)->display($context);
        // line 7
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 12
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig", 12)->display($context);
        echo "               

            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Search</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Search Results for: test</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class=\"container\">

                    <div class=\"row\">
                        <div class=\"col-md-9\">

                            <div class=\"alert alert-info\">
                                <p><strong>Info:</strong> The search feature is not functional in the HTML version.</p>
                            </div>

                            <h5 class=\"dark shorter\">New search:</h5>
                            <p>If you are not happy with the results below please do another search:</p>

                            <form>
                                <div class=\"input-group\">
                                    <input class=\"form-control\" placeholder=\"Search...\" name=\"s\" id=\"s\" type=\"text\">
                                    <span class=\"input-group-btn\">
                                        <button type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i></button>
                                    </span>
                                </div>
                            </form>

                            <hr />

                            <h4 class=\"dark\">6 search results for: <strong>test</strong></h4>

                            <ul class=\"simple-post-list\">
                                <li>
                                    <div class=\"post-info\">
                                        <a href=\"blog-post.html\">Nullam Vitae Nibh Un Odiosters</a>
                                        <div class=\"post-meta\">
                                            Jan 10, 2013
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"post-info\">
                                        <a href=\"blog-post.html\">Vitae Nibh Un Odiosters</a>
                                        <div class=\"post-meta\">
                                            Jan 10, 2013
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"post-info\">
                                        <a href=\"blog-post.html\">Odiosters Nullam Vitae</a>
                                        <div class=\"post-meta\">
                                            Jan 10, 2013
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"post-info\">
                                        <a href=\"blog-post.html\">Nullam Vitae Nibh Un Odiosters</a>
                                        <div class=\"post-meta\">
                                            Jan 10, 2013
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"post-info\">
                                        <a href=\"blog-post.html\">Vitae Nibh Un Odiosters</a>
                                        <div class=\"post-meta\">
                                            Jan 10, 2013
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"post-info\">
                                        <a href=\"blog-post.html\">Odiosters Nullam Vitae</a>
                                        <div class=\"post-meta\">
                                            Jan 10, 2013
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <ul class=\"pagination pull-right\">
                                <li><a href=\"#\">«</a></li>
                                <li class=\"active\"><a href=\"#\">1</a></li>
                                <li><a href=\"#\">2</a></li>
                                <li><a href=\"#\">3</a></li>
                                <li><a href=\"#\">»</a></li>
                            </ul>

                        </div>

                        <div class=\"col-md-3\">
                            <aside class=\"sidebar\">

                                <h4>Categories</h4>
                                <ul class=\"nav nav-list primary push-bottom\">
                                    <li><a href=\"#\">Design</a></li>
                                    <li><a href=\"#\">Photos</a></li>
                                    <li><a href=\"#\">Videos</a></li>
                                    <li><a href=\"#\">Lifestyle</a></li>
                                    <li><a href=\"#\">Technology</a></li>
                                </ul>

                                <div class=\"tabs\">
                                    <ul class=\"nav nav-tabs\">
                                        <li class=\"active\"><a href=\"#popularPosts\" data-toggle=\"tab\"><i class=\"fa fa-star\"></i> Popular</a></li>
                                        <li><a href=\"#recentPosts\" data-toggle=\"tab\">Recent</a></li>
                                    </ul>
                                    <div class=\"tab-content\">
                                        <div class=\"tab-pane active\" id=\"popularPosts\">
                                            <ul class=\"simple-post-list\">
                                                <li>
                                                    <div class=\"post-image\">
                                                        <div class=\"img-thumbnail\">
                                                            <a href=\"blog-post.html\">
                                                                <img src=\"img/blog/blog-thumb-1.jpg\" alt=\"\">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class=\"post-info\">
                                                        <a href=\"blog-post.html\">Nullam Vitae Nibh Un Odiosters</a>
                                                        <div class=\"post-meta\">
                                                            Jan 10, 2013
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class=\"post-image\">
                                                        <div class=\"img-thumbnail\">
                                                            <a href=\"blog-post.html\">
                                                                <img src=\"img/blog/blog-thumb-2.jpg\" alt=\"\">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class=\"post-info\">
                                                        <a href=\"blog-post.html\">Vitae Nibh Un Odiosters</a>
                                                        <div class=\"post-meta\">
                                                            Jan 10, 2013
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class=\"post-image\">
                                                        <div class=\"img-thumbnail\">
                                                            <a href=\"blog-post.html\">
                                                                <img src=\"img/blog/blog-thumb-3.jpg\" alt=\"\">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class=\"post-info\">
                                                        <a href=\"blog-post.html\">Odiosters Nullam Vitae</a>
                                                        <div class=\"post-meta\">
                                                            Jan 10, 2013
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class=\"tab-pane\" id=\"recentPosts\">
                                            <ul class=\"simple-post-list\">
                                                <li>
                                                    <div class=\"post-image\">
                                                        <div class=\"img-thumbnail\">
                                                            <a href=\"blog-post.html\">
                                                                <img src=\"img/blog/blog-thumb-2.jpg\" alt=\"\">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class=\"post-info\">
                                                        <a href=\"blog-post.html\">Vitae Nibh Un Odiosters</a>
                                                        <div class=\"post-meta\">
                                                            Jan 10, 2013
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class=\"post-image\">
                                                        <div class=\"img-thumbnail\">
                                                            <a href=\"blog-post.html\">
                                                                <img src=\"img/blog/blog-thumb-3.jpg\" alt=\"\">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class=\"post-info\">
                                                        <a href=\"blog-post.html\">Odiosters Nullam Vitae</a>
                                                        <div class=\"post-meta\">
                                                            Jan 10, 2013
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class=\"post-image\">
                                                        <div class=\"img-thumbnail\">
                                                            <a href=\"blog-post.html\">
                                                                <img src=\"img/blog/blog-thumb-1.jpg\" alt=\"\">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class=\"post-info\">
                                                        <a href=\"blog-post.html\">Nullam Vitae Nibh Un Odiosters</a>
                                                        <div class=\"post-meta\">
                                                            Jan 10, 2013
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </aside>
                        </div>
                    </div>

                </div>

            </div>
            <section class=\"call-to-action featured footer\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"center\">
                            <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987\" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
                        </div>
                    </div>
                </div>
            </section>

            ";
        // line 256
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig", 256)->display($context);
        // line 257
        echo "        </div>
        ";
        // line 258
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig", 258)->display($context);
        echo "                                                                       
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 258,  285 => 257,  283 => 256,  36 => 12,  29 => 7,  27 => 6,  22 => 3,  19 => 1,);
    }
}
/* 		*/
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}               */
/* */
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Search</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Search Results for: test</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <div class="container">*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-9">*/
/* */
/*                             <div class="alert alert-info">*/
/*                                 <p><strong>Info:</strong> The search feature is not functional in the HTML version.</p>*/
/*                             </div>*/
/* */
/*                             <h5 class="dark shorter">New search:</h5>*/
/*                             <p>If you are not happy with the results below please do another search:</p>*/
/* */
/*                             <form>*/
/*                                 <div class="input-group">*/
/*                                     <input class="form-control" placeholder="Search..." name="s" id="s" type="text">*/
/*                                     <span class="input-group-btn">*/
/*                                         <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </form>*/
/* */
/*                             <hr />*/
/* */
/*                             <h4 class="dark">6 search results for: <strong>test</strong></h4>*/
/* */
/*                             <ul class="simple-post-list">*/
/*                                 <li>*/
/*                                     <div class="post-info">*/
/*                                         <a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>*/
/*                                         <div class="post-meta">*/
/*                                             Jan 10, 2013*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <div class="post-info">*/
/*                                         <a href="blog-post.html">Vitae Nibh Un Odiosters</a>*/
/*                                         <div class="post-meta">*/
/*                                             Jan 10, 2013*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <div class="post-info">*/
/*                                         <a href="blog-post.html">Odiosters Nullam Vitae</a>*/
/*                                         <div class="post-meta">*/
/*                                             Jan 10, 2013*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <div class="post-info">*/
/*                                         <a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>*/
/*                                         <div class="post-meta">*/
/*                                             Jan 10, 2013*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <div class="post-info">*/
/*                                         <a href="blog-post.html">Vitae Nibh Un Odiosters</a>*/
/*                                         <div class="post-meta">*/
/*                                             Jan 10, 2013*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <div class="post-info">*/
/*                                         <a href="blog-post.html">Odiosters Nullam Vitae</a>*/
/*                                         <div class="post-meta">*/
/*                                             Jan 10, 2013*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                             <ul class="pagination pull-right">*/
/*                                 <li><a href="#">«</a></li>*/
/*                                 <li class="active"><a href="#">1</a></li>*/
/*                                 <li><a href="#">2</a></li>*/
/*                                 <li><a href="#">3</a></li>*/
/*                                 <li><a href="#">»</a></li>*/
/*                             </ul>*/
/* */
/*                         </div>*/
/* */
/*                         <div class="col-md-3">*/
/*                             <aside class="sidebar">*/
/* */
/*                                 <h4>Categories</h4>*/
/*                                 <ul class="nav nav-list primary push-bottom">*/
/*                                     <li><a href="#">Design</a></li>*/
/*                                     <li><a href="#">Photos</a></li>*/
/*                                     <li><a href="#">Videos</a></li>*/
/*                                     <li><a href="#">Lifestyle</a></li>*/
/*                                     <li><a href="#">Technology</a></li>*/
/*                                 </ul>*/
/* */
/*                                 <div class="tabs">*/
/*                                     <ul class="nav nav-tabs">*/
/*                                         <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li>*/
/*                                         <li><a href="#recentPosts" data-toggle="tab">Recent</a></li>*/
/*                                     </ul>*/
/*                                     <div class="tab-content">*/
/*                                         <div class="tab-pane active" id="popularPosts">*/
/*                                             <ul class="simple-post-list">*/
/*                                                 <li>*/
/*                                                     <div class="post-image">*/
/*                                                         <div class="img-thumbnail">*/
/*                                                             <a href="blog-post.html">*/
/*                                                                 <img src="img/blog/blog-thumb-1.jpg" alt="">*/
/*                                                             </a>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="post-info">*/
/*                                                         <a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>*/
/*                                                         <div class="post-meta">*/
/*                                                             Jan 10, 2013*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <div class="post-image">*/
/*                                                         <div class="img-thumbnail">*/
/*                                                             <a href="blog-post.html">*/
/*                                                                 <img src="img/blog/blog-thumb-2.jpg" alt="">*/
/*                                                             </a>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="post-info">*/
/*                                                         <a href="blog-post.html">Vitae Nibh Un Odiosters</a>*/
/*                                                         <div class="post-meta">*/
/*                                                             Jan 10, 2013*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <div class="post-image">*/
/*                                                         <div class="img-thumbnail">*/
/*                                                             <a href="blog-post.html">*/
/*                                                                 <img src="img/blog/blog-thumb-3.jpg" alt="">*/
/*                                                             </a>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="post-info">*/
/*                                                         <a href="blog-post.html">Odiosters Nullam Vitae</a>*/
/*                                                         <div class="post-meta">*/
/*                                                             Jan 10, 2013*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                             </ul>*/
/*                                         </div>*/
/*                                         <div class="tab-pane" id="recentPosts">*/
/*                                             <ul class="simple-post-list">*/
/*                                                 <li>*/
/*                                                     <div class="post-image">*/
/*                                                         <div class="img-thumbnail">*/
/*                                                             <a href="blog-post.html">*/
/*                                                                 <img src="img/blog/blog-thumb-2.jpg" alt="">*/
/*                                                             </a>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="post-info">*/
/*                                                         <a href="blog-post.html">Vitae Nibh Un Odiosters</a>*/
/*                                                         <div class="post-meta">*/
/*                                                             Jan 10, 2013*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <div class="post-image">*/
/*                                                         <div class="img-thumbnail">*/
/*                                                             <a href="blog-post.html">*/
/*                                                                 <img src="img/blog/blog-thumb-3.jpg" alt="">*/
/*                                                             </a>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="post-info">*/
/*                                                         <a href="blog-post.html">Odiosters Nullam Vitae</a>*/
/*                                                         <div class="post-meta">*/
/*                                                             Jan 10, 2013*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <div class="post-image">*/
/*                                                         <div class="img-thumbnail">*/
/*                                                             <a href="blog-post.html">*/
/*                                                                 <img src="img/blog/blog-thumb-1.jpg" alt="">*/
/*                                                             </a>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="post-info">*/
/*                                                         <a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>*/
/*                                                         <div class="post-meta">*/
/*                                                             Jan 10, 2013*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                             </ul>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                             </aside>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </div>*/
/* */
/*             </div>*/
/*             <section class="call-to-action featured footer">*/
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="center">*/
/*                             <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}                                                                       */
/*     </body>*/
/* </html>*/
