<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig */
class __TwigTemplate_237a430ea3ea05c905954b63c5f72fe16c939eef0bfc75110b8691516c6b3f01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig", 8)->display($context);
        echo " 
        <div class=\"body\">


            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Ideé</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Idée</h1>
                            </div>
                        </div>
                    </div>
            </section>

                     <h2>Idee</h2>
<div class=\"search\">
    <form id=\"searchForm\" action=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("SearchIdee");
        echo "\" method=\"POST\">
        <div class=\"input-group\">
            <input type=\"text\" class=\"form-control search\" name=\"q\" id=\"q\" placeholder=\"Search...\" required>
            <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
            </span>
        </div>
    </form>
</div>
<hr />

<div class=\"row\">

    <ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">
        ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["idees"]) ? $context["idees"] : $this->getContext($context, "idees")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "  
            <li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
                <div class=\"portfolio-item img-thumbnail\">
                    <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("idee", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\" class=\"thumb-info\">
                        <img alt=\"\" class=\"img-responsive\" src=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("image_idee", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">
                        <span class=\"thumb-info-title\">
                            <span class=\"thumb-info-inner\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "titre", array()), "html", null, true);
            echo "</span>
                            <span class=\"thumb-info-type\">";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "user", array()), "username", array()), "html", null, true);
            echo "</span>
                        </span>
                        <span class=\"thumb-info-action\">
                            <span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
                        </span>
                    </a>
                </div>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "    </ul>
</div>

            <section class=\"call-to-action featured footer\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"center\">
                            <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987\" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
                        </div>
                    </div>
                </div>
            </section>

            ";
        // line 77
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig", 77)->display($context);
        // line 78
        echo "        </div>
        ";
        // line 79
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig", 79)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 79,  129 => 78,  127 => 77,  112 => 64,  97 => 55,  93 => 54,  88 => 52,  84 => 51,  76 => 48,  59 => 34,  30 => 8,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %} */
/*         <div class="body">*/
/* */
/* */
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Ideé</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Idée</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*             </section>*/
/* */
/*                      <h2>Idee</h2>*/
/* <div class="search">*/
/*     <form id="searchForm" action="{{path('SearchIdee')}}" method="POST">*/
/*         <div class="input-group">*/
/*             <input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>*/
/*             <span class="input-group-btn">*/
/*                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>*/
/*             </span>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* <hr />*/
/* */
/* <div class="row">*/
/* */
/*     <ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/*         {% for i in idees %}  */
/*             <li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/*                 <div class="portfolio-item img-thumbnail">*/
/*                     <a href="{{ path('idee',{'id':i.id})}}" class="thumb-info">*/
/*                         <img alt="" class="img-responsive" src="{{path('image_idee',{'id':i.id})}}">*/
/*                         <span class="thumb-info-title">*/
/*                             <span class="thumb-info-inner">{{i.titre}}</span>*/
/*                             <span class="thumb-info-type">{{i.user.username}}</span>*/
/*                         </span>*/
/*                         <span class="thumb-info-action">*/
/*                             <span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/*                         </span>*/
/*                     </a>*/
/*                 </div>*/
/*             </li>*/
/*         {% endfor %}*/
/*     </ul>*/
/* </div>*/
/* */
/*             <section class="call-to-action featured footer">*/
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="center">*/
/*                             <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
