<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig */
class __TwigTemplate_f0e73a370e47fb243cfe103ba19f5b9b491179345af627e089f169d8b4cb58be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig", 3)->display($context);
        // line 4
        echo "    </head>
    <body>


        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig", 8)->display($context);
        echo "   

        <section class=\"page-top\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <ul class=\"breadcrumb\">
                            <li><a href=\"#\">Home</a></li>
                            <li class=\"active\">Probleme</li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h1>Formulaire de l'Idée</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class=\"container\">


            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <section class=\"panel\">
                        <header class=\"panel-heading\">
                            <div class=\"panel-actions\">
                                <a href=\"#\" class=\"fa fa-caret-down\"></a>
                                <a href=\"#\" class=\"fa fa-times\"></a>
                            </div>

                            <h2 class=\"panel-title\">Veuillez saisir les champs</h2>
                        </header>
                        <div class=\"panel-body\">


                            <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\"  ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'enctype');
        echo ">

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "titre", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Thème</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>  
                                </div>





                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Image</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "file", array()), 'widget');
        echo "    
                                    </div>  
                                </div>

                                ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "_token", array()), 'widget');
        echo "

                                ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                                <input type=\"submit\" value=\"Ajouter\" />

                            </form>
                        </div>

                    </section>

                </div>
            </div>
        </div>

        ";
        // line 98
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig", 98)->display($context);
        // line 99
        echo "
        ";
        // line 100
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig", 100)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 100,  147 => 99,  145 => 98,  130 => 86,  125 => 84,  118 => 80,  104 => 69,  93 => 61,  80 => 51,  70 => 44,  31 => 8,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}   */
/* */
/*         <section class="page-top">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <ul class="breadcrumb">*/
/*                             <li><a href="#">Home</a></li>*/
/*                             <li class="active">Probleme</li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h1>Formulaire de l'Idée</h1>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <div class="container">*/
/* */
/* */
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <section class="panel">*/
/*                         <header class="panel-heading">*/
/*                             <div class="panel-actions">*/
/*                                 <a href="#" class="fa fa-caret-down"></a>*/
/*                                 <a href="#" class="fa fa-times"></a>*/
/*                             </div>*/
/* */
/*                             <h2 class="panel-title">Veuillez saisir les champs</h2>*/
/*                         </header>*/
/*                         <div class="panel-body">*/
/* */
/* */
/*                             <form method="POST" class="form-horizontal form-bordered form-bordered"  {{ form_enctype(f) }}>*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.titre,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Thème</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>  */
/*                                 </div>*/
/* */
/* */
/* */
/* */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Image</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.file)}}    */
/*                                     </div>  */
/*                                 </div>*/
/* */
/*                                 {{ form_widget(f._token) }}*/
/* */
/*                                 {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                                 <input type="submit" value="Ajouter" />*/
/* */
/*                             </form>*/
/*                         </div>*/
/* */
/*                     </section>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
