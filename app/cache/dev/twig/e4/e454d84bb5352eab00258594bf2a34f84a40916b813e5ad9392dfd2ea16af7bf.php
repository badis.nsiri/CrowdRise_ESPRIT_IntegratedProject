<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_21bb8455d04114d3eac0f964919bdfcc2ee2289288e7c60b0006b1e49553c0d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'erreur_login' => array($this, 'block_erreur_login'),
            'fos_user_login' => array($this, 'block_fos_user_login'),
            'UserNameOrEmail' => array($this, 'block_UserNameOrEmail'),
            'Password' => array($this, 'block_Password'),
            'RememberMe' => array($this, 'block_RememberMe'),
            'loginButton' => array($this, 'block_loginButton'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 15
    public function block_erreur_login($context, array $blocks = array())
    {
        echo "  
   ";
        // line 17
        echo "    ";
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            echo " errr  ";
        }
    }

    // line 20
    public function block_fos_user_login($context, array $blocks = array())
    {
        // line 21
        echo "

    <form action=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
        ";
        // line 25
        $this->displayBlock('UserNameOrEmail', $context, $blocks);
        // line 35
        echo "        ";
        $this->displayBlock('Password', $context, $blocks);
        // line 47
        echo "

        ";
        // line 49
        $this->displayBlock('RememberMe', $context, $blocks);
        // line 64
        echo "    </form>
";
    }

    // line 25
    public function block_UserNameOrEmail($context, array $blocks = array())
    {
        // line 26
        echo "            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        <label for=\"username\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adresse e-mail ou Nom d'utilisateur", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                        <input type=\"text\" class=\"form-control input-lg\" id=\"username\" name=\"_username\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
                    </div>
                </div>
            </div>
        ";
    }

    // line 35
    public function block_Password($context, array $blocks = array())
    {
        // line 36
        echo "            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        <a class=\"pull-right\" id=\"headerRecover\" href=\"#\">(Lost Password?)</a>
                        <label for=\"password\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe :", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                        <input type=\"password\" class=\"form-control input-lg\" id=\"password\" name=\"_password\" required=\"required\" />
                    </div>
                </div>
            </div>

        ";
    }

    // line 49
    public function block_RememberMe($context, array $blocks = array())
    {
        // line 50
        echo "            <div class=\"row\">
                <div class=\"col-md-6\">
                    <span class=\"remember-box checkbox\">
                        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                        <label for=\"remember_me\">";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Garder ma session active", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                    </span>
                </div>
                ";
        // line 57
        $this->displayBlock('loginButton', $context, $blocks);
        // line 62
        echo "            </div>   
        ";
    }

    // line 57
    public function block_loginButton($context, array $blocks = array())
    {
        // line 58
        echo "                    <div class=\"col-md-6\">
                        <input type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\" value=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("connexion", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                    </div>
                ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 59,  142 => 58,  139 => 57,  134 => 62,  132 => 57,  126 => 54,  120 => 50,  117 => 49,  106 => 40,  100 => 36,  97 => 35,  88 => 30,  84 => 29,  79 => 26,  76 => 25,  71 => 64,  69 => 49,  65 => 47,  62 => 35,  60 => 25,  56 => 24,  52 => 23,  48 => 21,  45 => 20,  38 => 17,  33 => 15,  11 => 1,);
    }
}
/* {% extends "CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig" %}*/
/* {#*/
/* {% if error %}*/
/*     {% block erreur_login %}  */
/*         {% include "CrowdRiseBundle:Security:loginAcceuil.html.twig" %}*/
/*             errr*/
/*     {% endblock  %}*/
/*    */
/* */
/* */
/* */
/* */
/* {% else %}*/
/* #}*/
/* {% block erreur_login %}  */
/*    {# {% include "CrowdRiseBundle:Security:loginAcceuil.html.twig" %} #}*/
/*     {% if error %} errr  {% endif %}*/
/* {% endblock  %}*/
/* */
/* {% block fos_user_login %}*/
/* */
/* */
/*     <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*         <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*         {% block UserNameOrEmail %}*/
/*             <div class="row">*/
/*                 <div class="form-group">*/
/*                     <div class="col-md-12">*/
/*                         <label for="username">{{ "Adresse e-mail ou Nom d'utilisateur"|trans({}, 'FOSUserBundle') }}</label>*/
/*                         <input type="text" class="form-control input-lg" id="username" name="_username" value="{{ last_username }}" required="required" />*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         {% endblock %}*/
/*         {% block Password %}*/
/*             <div class="row">*/
/*                 <div class="form-group">*/
/*                     <div class="col-md-12">*/
/*                         <a class="pull-right" id="headerRecover" href="#">(Lost Password?)</a>*/
/*                         <label for="password">{{ 'Mot de passe :'|trans({}, 'FOSUserBundle') }}</label>*/
/*                         <input type="password" class="form-control input-lg" id="password" name="_password" required="required" />*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         {% endblock %}*/
/* */
/* */
/*         {% block RememberMe %}*/
/*             <div class="row">*/
/*                 <div class="col-md-6">*/
/*                     <span class="remember-box checkbox">*/
/*                         <input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/*                         <label for="remember_me">{{ 'Garder ma session active'|trans({}, 'FOSUserBundle') }}</label>*/
/*                     </span>*/
/*                 </div>*/
/*                 {% block loginButton %}*/
/*                     <div class="col-md-6">*/
/*                         <input type="submit" id="_submit" name="_submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading..." value="{{ 'connexion'|trans({}, 'FOSUserBundle') }}" />*/
/*                     </div>*/
/*                 {% endblock %}*/
/*             </div>   */
/*         {% endblock %}*/
/*     </form>*/
/* {% endblock %}*/
