<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_089b12324842da8cfe36dda69502b034021c6e3b298f2580d69c0621ce9daeb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo " 
";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('routing')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
        echo "
";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control input-lg")));
        echo "
";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'label');
        echo "
";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "form-control input-lg")));
        echo "
<table>
    <thead> <!-- En-tête du tableau -->
        <tr>
 ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
            <th>";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'label');
        echo "</th> 
            <th> ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'label');
        echo "</th>
        </tr>
    </thead>
    <tr>

        <td>
            ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control input-lg")));
        echo "
        </td>

        <td>
            ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control input-lg")));
        echo "
        </td>

     
                ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo " 

     

    </tr>
   
</table>


<div>
    <input type=\"submit\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Inscription", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
</div>
";
        // line 39
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 39,  89 => 37,  76 => 27,  69 => 23,  62 => 19,  53 => 13,  49 => 12,  45 => 11,  38 => 7,  34 => 6,  30 => 5,  26 => 4,  22 => 3,  19 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/*  */
/* {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}*/
/* {{ form_label(form.email)}}*/
/* {{ form_widget(form.email,{"attr": {'class': "form-control input-lg"}} )}}*/
/* {{ form_label(form.username) }}*/
/* {{ form_widget(form.username,{"attr": {'class': "form-control input-lg"}} )}}*/
/* <table>*/
/*     <thead> <!-- En-tête du tableau -->*/
/*         <tr>*/
/*  {{ form_errors(form) }}*/
/*             <th>{{ form_label(form.plainPassword.first)}}</th> */
/*             <th> {{ form_label(form.plainPassword.second)}}</th>*/
/*         </tr>*/
/*     </thead>*/
/*     <tr>*/
/* */
/*         <td>*/
/*             {{ form_widget(form.plainPassword.first,{"attr": {'class': "form-control input-lg"}} )}}*/
/*         </td>*/
/* */
/*         <td>*/
/*             {{ form_widget(form.plainPassword.second,{"attr": {'class': "form-control input-lg"}} )}}*/
/*         </td>*/
/* */
/*      */
/*                 {{ form_widget(form.file)}} */
/* */
/*      */
/* */
/*     </tr>*/
/*    */
/* </table>*/
/* */
/* */
/* <div>*/
/*     <input type="submit" value="{{ 'Inscription'|trans }}" />*/
/* </div>*/
/* {{ form_end(form) }}*/
