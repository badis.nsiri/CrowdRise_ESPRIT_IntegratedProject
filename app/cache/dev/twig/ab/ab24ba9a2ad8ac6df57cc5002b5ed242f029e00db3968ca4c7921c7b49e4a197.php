<?php

/* CrowdRiseBundle:Includes:Index/indexFooter.html.twig */
class __TwigTemplate_c67ae8f73ec88188038c457718fcaa4aefe208dd2ca5753911a0890e2705fd23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer id=\"footer\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"footer-ribbon\">
\t\t\t\t\t\t\t<span>Get in Touch</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<div class=\"newsletter\">
\t\t\t\t\t\t\t\t<h4>Newsletter</h4>
\t\t\t\t\t\t\t\t<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
\t\t\t
\t\t\t\t\t\t\t\t<div class=\"alert alert-success hidden\" id=\"newsletterSuccess\">
\t\t\t\t\t\t\t\t\t<strong>Success!</strong> You've been added to our email list.
\t\t\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t\t\t<div class=\"alert alert-danger hidden\" id=\"newsletterError\"></div>
\t\t\t
\t\t\t\t\t\t\t\t<form id=\"newsletterForm\" action=\"php/newsletter-subscribe.php\" method=\"POST\">
\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Email Address\" name=\"newsletterEmail\" id=\"newsletterEmail\" type=\"text\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\">Go!</button>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<h4>Latest Tweets</h4>
\t\t\t\t\t\t\t<div id=\"tweet\" class=\"twitter\" data-plugin-tweets data-plugin-options='{\"username\": \"\", \"count\": 2}'>
\t\t\t\t\t\t\t\t<p>Please wait...</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t<div class=\"contact-details\">
\t\t\t\t\t\t\t\t<h4>Contact Us</h4>
\t\t\t\t\t\t\t\t<ul class=\"contact\">
\t\t\t\t\t\t\t\t\t<li><p><i class=\"fa fa-map-marker\"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
\t\t\t\t\t\t\t\t\t<li><p><i class=\"fa fa-phone\"></i> <strong>Phone:</strong> (123) 456-7890</p></li>
\t\t\t\t\t\t\t\t\t<li><p><i class=\"fa fa-envelope\"></i> <strong>Email:</strong> <a href=\"mailto:mail@example.com\">mail@example.com</a></p></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-2\">
\t\t\t\t\t\t\t<h4>Follow Us</h4>
\t\t\t\t\t\t\t<div class=\"social-icons\">
\t\t\t\t\t\t\t\t<ul class=\"social-icons\">
\t\t\t\t\t\t\t\t\t<li class=\"facebook\"><a href=\"http://www.facebook.com/\" target=\"_blank\" data-placement=\"bottom\" data-tooltip title=\"Facebook\">Facebook</a></li>
\t\t\t\t\t\t\t\t\t<li class=\"twitter\"><a href=\"http://www.twitter.com/\" target=\"_blank\" data-placement=\"bottom\" data-tooltip title=\"Twitter\">Twitter</a></li>
\t\t\t\t\t\t\t\t\t<li class=\"linkedin\"><a href=\"http://www.linkedin.com/\" target=\"_blank\" data-placement=\"bottom\" data-tooltip title=\"Linkedin\">Linkedin</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"footer-copyright\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-1\">
\t\t\t\t\t\t\t\t<a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("crowd_rise_frontOffice");
        echo "\" class=\"logo\">
\t\t\t\t\t\t\t\t\t<img alt=\"Porto Website Template\" class=\"img-responsive\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/logo-footer.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-7\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t<nav id=\"sub-menu\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"page-faq.html\">FAQ's</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"sitemap.html\">Sitemap</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"contact-us.html\">Contact</a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</nav>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</footer>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Includes:Index/indexFooter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 61,  80 => 60,  19 => 1,);
    }
}
/* <footer id="footer">*/
/* 				<div class="container">*/
/* 					<div class="row">*/
/* 						<div class="footer-ribbon">*/
/* 							<span>Get in Touch</span>*/
/* 						</div>*/
/* 						<div class="col-md-3">*/
/* 							<div class="newsletter">*/
/* 								<h4>Newsletter</h4>*/
/* 								<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>*/
/* 			*/
/* 								<div class="alert alert-success hidden" id="newsletterSuccess">*/
/* 									<strong>Success!</strong> You've been added to our email list.*/
/* 								</div>*/
/* 			*/
/* 								<div class="alert alert-danger hidden" id="newsletterError"></div>*/
/* 			*/
/* 								<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">*/
/* 									<div class="input-group">*/
/* 										<input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">*/
/* 										<span class="input-group-btn">*/
/* 											<button class="btn btn-default" type="submit">Go!</button>*/
/* 										</span>*/
/* 									</div>*/
/* 								</form>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col-md-3">*/
/* 							<h4>Latest Tweets</h4>*/
/* 							<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options='{"username": "", "count": 2}'>*/
/* 								<p>Please wait...</p>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col-md-4">*/
/* 							<div class="contact-details">*/
/* 								<h4>Contact Us</h4>*/
/* 								<ul class="contact">*/
/* 									<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>*/
/* 									<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</p></li>*/
/* 									<li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>*/
/* 								</ul>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="col-md-2">*/
/* 							<h4>Follow Us</h4>*/
/* 							<div class="social-icons">*/
/* 								<ul class="social-icons">*/
/* 									<li class="facebook"><a href="http://www.facebook.com/" target="_blank" data-placement="bottom" data-tooltip title="Facebook">Facebook</a></li>*/
/* 									<li class="twitter"><a href="http://www.twitter.com/" target="_blank" data-placement="bottom" data-tooltip title="Twitter">Twitter</a></li>*/
/* 									<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" data-placement="bottom" data-tooltip title="Linkedin">Linkedin</a></li>*/
/* 								</ul>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 				<div class="footer-copyright">*/
/* 					<div class="container">*/
/* 						<div class="row">*/
/* 							<div class="col-md-1">*/
/* 								<a href="{{ path('crowd_rise_frontOffice') }}" class="logo">*/
/* 									<img alt="Porto Website Template" class="img-responsive" src="{{asset('FontOffice/img/logo-footer.png')}}">*/
/* 								</a>*/
/* 							</div>*/
/* 							<div class="col-md-7">*/
/* 								*/
/* 							</div>*/
/* 							<div class="col-md-4">*/
/* 								<nav id="sub-menu">*/
/* 									<ul>*/
/* 										<li><a href="page-faq.html">FAQ's</a></li>*/
/* 										<li><a href="sitemap.html">Sitemap</a></li>*/
/* 										<li><a href="contact-us.html">Contact</a></li>*/
/* 									</ul>*/
/* 								</nav>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</footer>*/
