<?php

/* CrowdRiseBundle:ChangePassword:changePassword_content.html.twig */
class __TwigTemplate_5361aedcbe6c98237943197214967c2bacd5318007ee741fc4f564570e34cd0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:ChangePassword:changePassword_content.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 11
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:ChangePassword:changePassword_content.html.twig", 11)->display($context);
        // line 12
        echo "            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Features</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Forms Basic</h1>
                            </div>
                        </div>
                    </div>
                </section>


                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <section class=\"panel\">
                                <header class=\"panel-heading\">


                                    <h2 class=\"panel-title\">Form Elements</h2>
                                </header>

                                <div class=\"panel-body\">
                                    ";
        // line 44
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("fos_user_change_password"), "attr" => array("class" => "fos_user_change_password")));
        echo "

                                    <form class=\"form-horizontal form-bordered\" method=\"get\">



                                        <div class=\"form-group\">
                                            <label class=\" col-md-3 control-label\">mot de passe actuel: </label>
                                            <div class=\"col-lg-6\">
                                                <p class=\"form-control-static\">";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "current_password", array()), 'widget');
        echo "</p>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\" col-md-3 control-label\">";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'label');
        echo " </label>
                                            <div class=\"col-lg-6\">
                                                <p class=\"form-control-static\">";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget');
        echo "</p>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\" col-md-3 control-label\">";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'label');
        echo "</label>
                                            <div class=\"col-lg-6\">
                                                <p class=\"form-control-static\">";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget');
        echo "</p>
                                            </div>
                                        </div>



                                        <div>
                                            <input type=\"submit\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Changer"), "html", null, true);
        echo "\" />
                                        </div>  
                                        ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

                                    </form>
                                    ";
        // line 77
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

                                </div>

                            </section>

                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-lg-12\">


                        </div>

                        ";
        // line 92
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:ChangePassword:changePassword_content.html.twig", 92)->display($context);
        // line 93
        echo "                    </div>

                    ";
        // line 95
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:ChangePassword:changePassword_content.html.twig", 95)->display($context);
        // line 96
        echo "                    </body>
                    </html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:ChangePassword:changePassword_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 96,  150 => 95,  146 => 93,  144 => 92,  126 => 77,  120 => 74,  115 => 72,  105 => 65,  100 => 63,  93 => 59,  88 => 57,  81 => 53,  69 => 44,  35 => 12,  33 => 11,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Features</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Forms Basic</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/* */
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="col-lg-12">*/
/*                             <section class="panel">*/
/*                                 <header class="panel-heading">*/
/* */
/* */
/*                                     <h2 class="panel-title">Form Elements</h2>*/
/*                                 </header>*/
/* */
/*                                 <div class="panel-body">*/
/*                                     {{ form_start(form, { 'action': path('fos_user_change_password'), 'attr': { 'class': 'fos_user_change_password' } }) }}*/
/* */
/*                                     <form class="form-horizontal form-bordered" method="get">*/
/* */
/* */
/* */
/*                                         <div class="form-group">*/
/*                                             <label class=" col-md-3 control-label">mot de passe actuel: </label>*/
/*                                             <div class="col-lg-6">*/
/*                                                 <p class="form-control-static">{{ form_widget(form.current_password) }}</p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class=" col-md-3 control-label">{{ form_label(form.plainPassword.first) }} </label>*/
/*                                             <div class="col-lg-6">*/
/*                                                 <p class="form-control-static">{{ form_widget(form.plainPassword.first) }}</p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class=" col-md-3 control-label">{{ form_label(form.plainPassword.second) }}</label>*/
/*                                             <div class="col-lg-6">*/
/*                                                 <p class="form-control-static">{{ form_widget(form.plainPassword.second) }}</p>*/
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/* */
/*                                         <div>*/
/*                                             <input type="submit" value="{{ 'Changer'|trans }}" />*/
/*                                         </div>  */
/*                                         {{ form_widget(form) }}*/
/* */
/*                                     </form>*/
/*                                     {{ form_end(form) }}*/
/* */
/*                                 </div>*/
/* */
/*                             </section>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-lg-12">*/
/* */
/* */
/*                         </div>*/
/* */
/*                         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*                     </div>*/
/* */
/*                     {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}*/
/*                     </body>*/
/*                     </html>*/
