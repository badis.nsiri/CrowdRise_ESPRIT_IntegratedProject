<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:showAllProjet.html.twig */
class __TwigTemplate_7e7d3baae3d8dcc59a068241946bc86510bbf9b0b2c277f3c576cb65800f699c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<h2>Projet</h2>



<hr />

<div class=\"row\">

    <ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">
        ";
        // line 11
        $context["c"] = 0;
        // line 12
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["projets"]) ? $context["projets"] : $this->getContext($context, "projets")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "  
            ";
            // line 13
            $context["c"] = ((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")) + 1);
            // line 14
            echo "            ";
            if (((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")) <= 4)) {
                // line 15
                echo "                <li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
                    <div class=\"portfolio-item img-thumbnail\">
                        <a href=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet1", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                echo "\" class=\"thumb-info\">
                            <img alt=\"\" class=\"img-responsive\" src=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
                echo "\">
                            <span class=\"thumb-info-title\">
                                <span class=\"thumb-info-inner\">";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "titre", array()), "html", null, true);
                echo "</span>
                                <span class=\"thumb-info-type\">CrowdRise</span>
                            </span>
                            <span class=\"thumb-info-action\">
                                <span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
                            </span>
                        </a>
                    </div>
                </li>
            ";
            }
            // line 30
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "    </ul>
</div>

";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:showAllProjet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 31,  71 => 30,  58 => 20,  53 => 18,  49 => 17,  45 => 15,  42 => 14,  40 => 13,  33 => 12,  31 => 11,  19 => 1,);
    }
}
/* */
/* <h2>Projet</h2>*/
/* */
/* */
/* */
/* <hr />*/
/* */
/* <div class="row">*/
/* */
/*     <ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/*         {% set c=0   %}*/
/*         {% for i in projets %}  */
/*             {% set c=c+1 %}*/
/*             {% if c<=4 %}*/
/*                 <li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/*                     <div class="portfolio-item img-thumbnail">*/
/*                         <a href="{{ path('projet1',{'id':i.id})}}" class="thumb-info">*/
/*                             <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/*                             <span class="thumb-info-title">*/
/*                                 <span class="thumb-info-inner">{{i.titre}}</span>*/
/*                                 <span class="thumb-info-type">CrowdRise</span>*/
/*                             </span>*/
/*                             <span class="thumb-info-action">*/
/*                                 <span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/*                             </span>*/
/*                         </a>*/
/*                     </div>*/
/*                 </li>*/
/*             {%endif %}*/
/*         {% endfor %}*/
/*     </ul>*/
/* </div>*/
/* */
/* */
