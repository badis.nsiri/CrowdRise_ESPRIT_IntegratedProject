<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig */
class __TwigTemplate_5248d4198314b9cf4e79ac4c14c83f539ae974f61a026a3c4ba0e99e24b599e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig", 3)->display($context);
        // line 4
        echo "    </head>
    <body>

        ";
        // line 7
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig", 7)->display($context);
        echo "   


        <section class=\"page-top\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <ul class=\"breadcrumb\">
                            <li><a href=\"#\">Home</a></li>
                            <li class=\"active\">Idée</li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h1>Formulaire de l'Idée</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <section class=\"panel\">
                        <header class=\"panel-heading\">
                            <div class=\"panel-actions\">
                                <a href=\"#\" class=\"fa fa-caret-down\"></a>
                                <a href=\"#\" class=\"fa fa-times\"></a>
                            </div>

                            <h2 class=\"panel-title\">Veuillez saisir les champs</h2>
                        </header>
                        <div class=\"panel-body\">


                            <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" >

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "titre", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Theme</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Image</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "file", array()), 'widget');
        echo "    
                                    </div>  
                                </div>





                                <input type=\"submit\" value=\"Modifier\" />
                                ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                            </form>
                        </div>

                    </section>

                </div>
            </div>
        </div>
        ";
        // line 92
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig", 92)->display($context);
        // line 93
        echo "
        ";
        // line 94
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig", 94)->display($context);
        echo "           

    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 94,  135 => 93,  133 => 92,  121 => 83,  109 => 74,  98 => 66,  88 => 59,  75 => 49,  30 => 7,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}   */
/* */
/* */
/*         <section class="page-top">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <ul class="breadcrumb">*/
/*                             <li><a href="#">Home</a></li>*/
/*                             <li class="active">Idée</li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h1>Formulaire de l'Idée</h1>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <section class="panel">*/
/*                         <header class="panel-heading">*/
/*                             <div class="panel-actions">*/
/*                                 <a href="#" class="fa fa-caret-down"></a>*/
/*                                 <a href="#" class="fa fa-times"></a>*/
/*                             </div>*/
/* */
/*                             <h2 class="panel-title">Veuillez saisir les champs</h2>*/
/*                         </header>*/
/*                         <div class="panel-body">*/
/* */
/* */
/*                             <form method="POST" class="form-horizontal form-bordered form-bordered" >*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.titre,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Theme</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Image</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.file)}}    */
/*                                     </div>  */
/*                                 </div>*/
/* */
/* */
/* */
/* */
/* */
/*                                 <input type="submit" value="Modifier" />*/
/*                                 {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                             </form>*/
/*                         </div>*/
/* */
/*                     </section>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}           */
/* */
/*     </body>*/
/* </html>*/
