<?php

/* CrowdRiseBundle:Profile:show_content.html.twig */
class __TwigTemplate_e628fde26c8632a296ce0b3d16092ecf50a8523d24d7f2df27acb69c18fecf81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " 
";
        // line 3
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 6
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:Profile:show_content.html.twig", 6)->display($context);
        // line 7
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 12
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:Profile:show_content.html.twig", 12)->display($context);
        // line 13
        echo "            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Features</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Mon Profile</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <center>
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                <section class=\"panel\">
                                    <header class=\"panel-heading\">


                                        <h2 class=\"panel-title\">Mon Profile</h2>
                                    </header>


                                    <div  >
                                        <form class=\"form-horizontal form-bordered\" method=\"POST\">
                                            <div class=\"col-lg-12\">
                                                <div class=\"form-group\">

                                                    <div class=\"col-lg-6\">
                                                        <p class=\"form-control-static\">    </p>
                                                        <label>     

                                                            <div class=\"img-thumbnail\" >
                                                                <img src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("image_user", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\" alt=\"\">
                                                            </div></label>

                                                    </div>
                                                </div>    


                                            </div>
                                            <div  >
                                                <div class=\"form-group\">
                                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Nom de l'utilisateur :</label>
                                                    <div class=\"col-md-6\">
                                                        <label>   ";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</label>
                                                    </div>
                                                </div>
                                                <div class=\"form-group\">
                                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Nom :</label>
                                                    <div class=\"col-md-6\">
                                                        <label>   ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "nom", array()), "html", null, true);
        echo "</label>
                                                    </div>
                                                </div>
                                                <div class=\"form-group\">
                                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Prenom :</label>
                                                    <div class=\"col-md-6\">
                                                        <label>   ";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "prenom", array()), "html", null, true);
        echo "</label>
                                                    </div>
                                                </div>


                                                <div class=\"form-group\">
                                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Numéro de téléphone :</label>
                                                    <div class=\"col-md-6\">
                                                        <label>   ";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "tel", array()), "html", null, true);
        echo "</label>
                                                    </div>
                                                </div>


                                                <div class=\"form-group\">
                                                    <label class=\" col-md-3 control-label\">Email : </label>
                                                    <div class=\"col-lg-6\">
                                                        <p class=\"form-control-static\">    </p>
                                                        <label>   ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</label>
                                                    </div>
                                                </div>
                                            </div> 

                                            


                                        </form>
                                    </div>  

                                </section>

                            </div>
                        </div>
                </center>
                <div class=\"row\">


                    ";
        // line 115
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:Profile:show_content.html.twig", 115)->display($context);
        // line 116
        echo "                </div>

                ";
        // line 118
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:Profile:show_content.html.twig", 118)->display($context);
        // line 119
        echo "                </body>
                </html>
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 119,  166 => 118,  162 => 116,  160 => 115,  138 => 96,  126 => 87,  115 => 79,  106 => 73,  97 => 67,  82 => 55,  38 => 13,  36 => 12,  29 => 7,  27 => 6,  22 => 3,  19 => 1,);
    }
}
/*  */
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Features</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Mon Profile</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <center>*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-lg-12">*/
/*                                 <section class="panel">*/
/*                                     <header class="panel-heading">*/
/* */
/* */
/*                                         <h2 class="panel-title">Mon Profile</h2>*/
/*                                     </header>*/
/* */
/* */
/*                                     <div  >*/
/*                                         <form class="form-horizontal form-bordered" method="POST">*/
/*                                             <div class="col-lg-12">*/
/*                                                 <div class="form-group">*/
/* */
/*                                                     <div class="col-lg-6">*/
/*                                                         <p class="form-control-static">    </p>*/
/*                                                         <label>     */
/* */
/*                                                             <div class="img-thumbnail" >*/
/*                                                                 <img src="{{path('image_user',{'id':app.user.id})}}" alt="">*/
/*                                                             </div></label>*/
/* */
/*                                                     </div>*/
/*                                                 </div>    */
/* */
/* */
/*                                             </div>*/
/*                                             <div  >*/
/*                                                 <div class="form-group">*/
/*                                                     <label class="col-md-3 control-label" for="inputDefault">Nom de l'utilisateur :</label>*/
/*                                                     <div class="col-md-6">*/
/*                                                         <label>   {{ user.username }}</label>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div class="form-group">*/
/*                                                     <label class="col-md-3 control-label" for="inputDefault">Nom :</label>*/
/*                                                     <div class="col-md-6">*/
/*                                                         <label>   {{ user.nom }}</label>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div class="form-group">*/
/*                                                     <label class="col-md-3 control-label" for="inputDefault">Prenom :</label>*/
/*                                                     <div class="col-md-6">*/
/*                                                         <label>   {{ user.prenom }}</label>*/
/*                                                     </div>*/
/*                                                 </div>*/
/* */
/* */
/*                                                 <div class="form-group">*/
/*                                                     <label class="col-md-3 control-label" for="inputDefault">Numéro de téléphone :</label>*/
/*                                                     <div class="col-md-6">*/
/*                                                         <label>   {{ user.tel }}</label>*/
/*                                                     </div>*/
/*                                                 </div>*/
/* */
/* */
/*                                                 <div class="form-group">*/
/*                                                     <label class=" col-md-3 control-label">Email : </label>*/
/*                                                     <div class="col-lg-6">*/
/*                                                         <p class="form-control-static">    </p>*/
/*                                                         <label>   {{ user.email }}</label>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div> */
/* */
/*                                             */
/* */
/* */
/*                                         </form>*/
/*                                     </div>  */
/* */
/*                                 </section>*/
/* */
/*                             </div>*/
/*                         </div>*/
/*                 </center>*/
/*                 <div class="row">*/
/* */
/* */
/*                     {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*                 </div>*/
/* */
/*                 {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}*/
/*                 </body>*/
/*                 </html>*/
/* */
