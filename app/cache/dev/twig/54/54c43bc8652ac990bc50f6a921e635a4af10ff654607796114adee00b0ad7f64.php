<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig */
class __TwigTemplate_042067ae79b38f74220cc601e3b4a2bf5368d4bc5b96970f337d07448c5cb8a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig", 8)->display($context);
        echo " 
        <div class=\"body\">


            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("crowd_rise_homepage");
        echo "\">Home</a></li>
                                    <li class=\"active\"><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("list_idee");
        echo "\">Ideé</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Idée</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class=\"container\">

                    <div class=\"portfolio-title\">
                        <div class=\"row\">
                            <div class=\"portfolio-nav-all col-md-1\">
                                <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("list_idee");
        echo "\" data-tooltip data-original-title=\"Liste des idées\"><i class=\"fa fa-th\"></i></a>
                            </div>
                            <div class=\"col-md-10 center\">
                                <h2 class=\"shorter\">";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "titre", array()), "html", null, true);
        echo "</h2>
                            </div>

                        </div>
                    </div>

                    <hr class=\"tall\">

                    <div class=\"row\">
                        <div class=\"col-md-4\">


                            <div>
                                <div class=\"thumbnail\">
                                    <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("image_idee", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
        echo "\">
                                </div>
                            </div>



                        </div>

                        <div class=\"col-md-8\">

                            <div class=\"portfolio-info\">
                                <div class=\"row\">
                                    <div class=\"col-md-12 center\">
                                        <ul>
                                            <li>
                                                <a href=\"#\" data-tooltip data-original-title=\"Like\"><i class=\"fa fa-heart\"></i>14</a>
                                            </li>
                                            <li>
                                                <i class=\"fa fa-calendar\"></i> 21 November 2013
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <h4><strong>Description </strong> de l'idée</h4>
                            <p class=\"taller\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "description", array()), "html", null, true);
        echo "</p>
                            <ul class=\"portfolio-details\">
                                <li>

                                    <p><strong>Thême:</strong></p>

                                    <ul class=\"list list-skills icons list-unstyled list-inline\">


                                        <li><i class=\"fa fa-check-circle\"> </i>  <li>";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "theme", array()), "html", null, true);
        echo "</li> <br>

                                    </ul>
                                </li>
                            </ul>


 
                            ";
        // line 98
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "user", array()), "id", array()))) {
            // line 99
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_idee", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Modifier</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
                                <a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer_idee", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Supprimer</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
                            ";
        }
        // line 102
        echo "                            <ul class=\"portfolio-details\">
                                <div class=\"form-group\">






                                    <div class=\"col-md-6\">
                                        <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Commentaire</label><br>
                                        <div class=\"col-md-6\">
                                             <form  action=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("addCommentaireIdee", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
        echo "\">
                                            <textarea class=\"form-control\" rows=\"3\" data-plugin-maxlength maxlength=\"5000 \" name=\"CIdee\">
                                                    
                                            </textarea>
                                           
                                                <input type=\"submit\" /> <br>
                                            </form>

                                        </div>

                                    </div>




                                </div>  

                                ";
        // line 130
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CrowdRiseBundle:Commentaire:showCommentaireByIdee", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))));
        echo " 


                            </ul>

                        </div>
                    </div>

                    <hr class=\"tall\" />


                </div>

            </div>



            ";
        // line 147
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig", 147)->display($context);
        // line 148
        echo "        </div>
        ";
        // line 149
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig", 149)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 149,  211 => 148,  209 => 147,  189 => 130,  169 => 113,  156 => 102,  151 => 100,  146 => 99,  144 => 98,  133 => 90,  121 => 81,  91 => 54,  74 => 40,  68 => 37,  48 => 20,  44 => 19,  30 => 8,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %} */
/*         <div class="body">*/
/* */
/* */
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="{{path('crowd_rise_homepage')}}">Home</a></li>*/
/*                                     <li class="active"><a href="{{path('list_idee')}}">Ideé</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Idée</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <div class="container">*/
/* */
/*                     <div class="portfolio-title">*/
/*                         <div class="row">*/
/*                             <div class="portfolio-nav-all col-md-1">*/
/*                                 <a href="{{path('list_idee')}}" data-tooltip data-original-title="Liste des idées"><i class="fa fa-th"></i></a>*/
/*                             </div>*/
/*                             <div class="col-md-10 center">*/
/*                                 <h2 class="shorter">{{m.titre}}</h2>*/
/*                             </div>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall">*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-4">*/
/* */
/* */
/*                             <div>*/
/*                                 <div class="thumbnail">*/
/*                                     <img alt="" class="img-responsive" src="{{path('image_idee',{'id':m.id})}}">*/
/*                                 </div>*/
/*                             </div>*/
/* */
/* */
/* */
/*                         </div>*/
/* */
/*                         <div class="col-md-8">*/
/* */
/*                             <div class="portfolio-info">*/
/*                                 <div class="row">*/
/*                                     <div class="col-md-12 center">*/
/*                                         <ul>*/
/*                                             <li>*/
/*                                                 <a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i>14</a>*/
/*                                             </li>*/
/*                                             <li>*/
/*                                                 <i class="fa fa-calendar"></i> 21 November 2013*/
/*                                             </li>*/
/* */
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/*                             <h4><strong>Description </strong> de l'idée</h4>*/
/*                             <p class="taller">{{m.description}}</p>*/
/*                             <ul class="portfolio-details">*/
/*                                 <li>*/
/* */
/*                                     <p><strong>Thême:</strong></p>*/
/* */
/*                                     <ul class="list list-skills icons list-unstyled list-inline">*/
/* */
/* */
/*                                         <li><i class="fa fa-check-circle"> </i>  <li>{{m.theme}}</li> <br>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/* */
/*  */
/*                             {% if app.user.id == m.user.id %}*/
/*                                 <a href="{{path('modifier_idee', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Modifier</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*                                 <a href="{{path('supprimer_idee', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Supprimer</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*                             {% endif %}*/
/*                             <ul class="portfolio-details">*/
/*                                 <div class="form-group">*/
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     <div class="col-md-6">*/
/*                                         <label class="col-md-3 control-label" for="textareaDefault">Commentaire</label><br>*/
/*                                         <div class="col-md-6">*/
/*                                              <form  action="{{path('addCommentaireIdee',{'id':m.id})}}">*/
/*                                             <textarea class="form-control" rows="3" data-plugin-maxlength maxlength="5000 " name="CIdee">*/
/*                                                     */
/*                                             </textarea>*/
/*                                            */
/*                                                 <input type="submit" /> <br>*/
/*                                             </form>*/
/* */
/*                                         </div>*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/* */
/*                                 </div>  */
/* */
/*                                 {{ render(controller('CrowdRiseBundle:Commentaire:showCommentaireByIdee',{'id':m.id})) }} */
/* */
/* */
/*                             </ul>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall" />*/
/* */
/* */
/*                 </div>*/
/* */
/*             </div>*/
/* */
/* */
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
