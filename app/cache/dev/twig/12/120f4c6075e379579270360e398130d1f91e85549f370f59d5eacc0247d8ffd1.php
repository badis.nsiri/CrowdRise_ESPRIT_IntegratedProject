<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:erreurLogin.html.twig */
class __TwigTemplate_6c4847908e9dce633dcf11c26c067ed607a1cd1b62ee7093ab220c56d5d79b10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice2.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:erreurLogin.html.twig", 2);
        $this->blocks = array(
            'erreur_login' => array($this, 'block_erreur_login'),
            'fos_user_login' => array($this, 'block_fos_user_login'),
            'UserNameOrEmail' => array($this, 'block_UserNameOrEmail'),
            'Password' => array($this, 'block_Password'),
            'RememberMe' => array($this, 'block_RememberMe'),
            'loginButton' => array($this, 'block_loginButton'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_erreur_login($context, array $blocks = array())
    {
        // line 5
        echo "   dfdddddddd
";
    }

    // line 8
    public function block_fos_user_login($context, array $blocks = array())
    {
        // line 9
        echo "

    <form action=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
        ";
        // line 13
        $this->displayBlock('UserNameOrEmail', $context, $blocks);
        // line 23
        echo "        ";
        $this->displayBlock('Password', $context, $blocks);
        // line 35
        echo "

        ";
        // line 37
        $this->displayBlock('RememberMe', $context, $blocks);
        // line 52
        echo "    </form>
";
    }

    // line 13
    public function block_UserNameOrEmail($context, array $blocks = array())
    {
        // line 14
        echo "            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        <label for=\"username\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adresse e-mail ou Nom d'utilisateur", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                        <input type=\"text\" class=\"form-control input-lg\" id=\"username\" name=\"_username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
                    </div>
                </div>
            </div>
        ";
    }

    // line 23
    public function block_Password($context, array $blocks = array())
    {
        // line 24
        echo "            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        <a class=\"pull-right\" id=\"headerRecover\" href=\"#\">(Lost Password?)</a>
                        <label for=\"password\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe :", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                        <input type=\"password\" class=\"form-control input-lg\" id=\"password\" name=\"_password\" required=\"required\" />
                    </div>
                </div>
            </div>

        ";
    }

    // line 37
    public function block_RememberMe($context, array $blocks = array())
    {
        // line 38
        echo "            <div class=\"row\">
                <div class=\"col-md-6\">
                    <span class=\"remember-box checkbox\">
                        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                        <label for=\"remember_me\">";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Garder ma session active", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                    </span>
                </div>
                ";
        // line 45
        $this->displayBlock('loginButton', $context, $blocks);
        // line 50
        echo "            </div>   
        ";
    }

    // line 45
    public function block_loginButton($context, array $blocks = array())
    {
        // line 46
        echo "                    <div class=\"col-md-6\">
                        <input type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("connexion", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                    </div>
                ";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:erreurLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 47,  138 => 46,  135 => 45,  130 => 50,  128 => 45,  122 => 42,  116 => 38,  113 => 37,  102 => 28,  96 => 24,  93 => 23,  84 => 18,  80 => 17,  75 => 14,  72 => 13,  67 => 52,  65 => 37,  61 => 35,  58 => 23,  56 => 13,  52 => 12,  48 => 11,  44 => 9,  41 => 8,  36 => 5,  33 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice2.html.twig" %}*/
/* */
/* {% block erreur_login %}*/
/*    dfdddddddd*/
/* {% endblock  %}*/
/* */
/* {% block fos_user_login %}*/
/* */
/* */
/*     <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*         <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*         {% block UserNameOrEmail %}*/
/*             <div class="row">*/
/*                 <div class="form-group">*/
/*                     <div class="col-md-12">*/
/*                         <label for="username">{{ "Adresse e-mail ou Nom d'utilisateur"|trans({}, 'FOSUserBundle') }}</label>*/
/*                         <input type="text" class="form-control input-lg" id="username" name="_username" value="{{ last_username }}" required="required" />*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         {% endblock %}*/
/*         {% block Password %}*/
/*             <div class="row">*/
/*                 <div class="form-group">*/
/*                     <div class="col-md-12">*/
/*                         <a class="pull-right" id="headerRecover" href="#">(Lost Password?)</a>*/
/*                         <label for="password">{{ 'Mot de passe :'|trans({}, 'FOSUserBundle') }}</label>*/
/*                         <input type="password" class="form-control input-lg" id="password" name="_password" required="required" />*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         {% endblock %}*/
/* */
/* */
/*         {% block RememberMe %}*/
/*             <div class="row">*/
/*                 <div class="col-md-6">*/
/*                     <span class="remember-box checkbox">*/
/*                         <input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/*                         <label for="remember_me">{{ 'Garder ma session active'|trans({}, 'FOSUserBundle') }}</label>*/
/*                     </span>*/
/*                 </div>*/
/*                 {% block loginButton %}*/
/*                     <div class="col-md-6">*/
/*                         <input type="submit" id="_submit" name="_submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading..." value="{{ 'connexion'|trans({}, 'FOSUserBundle') }}" />*/
/*                     </div>*/
/*                 {% endblock %}*/
/*             </div>   */
/*         {% endblock %}*/
/*     </form>*/
/* {% endblock %}*/
