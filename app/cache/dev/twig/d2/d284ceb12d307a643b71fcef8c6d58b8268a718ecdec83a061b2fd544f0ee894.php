<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig */
class __TwigTemplate_c68ec2d7ad49f4fde1782ffd5ae2cc2b250f628a79a9d66efa669eb2ac02168b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
\t<head>
              ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig", 3)->display($context);
        // line 4
        echo "\t</head>
        <body>
            
                                                                        
                 ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig", 8)->display($context);
        echo "                                                               
         
                                <section class=\"page-top\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>
\t\t\t\t\t\t\t\t\t<li class=\"active\">Probleme</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<h1>Formulaire du Probleme</h1>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
                              <div class=\"container\">
\t\t\t\t
                                        
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Veuillez saisir les champs</h2>
\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t
                                                                    
                                                                      <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" >
                                                                             
                                                                                <div class=\"form-group\">

                                                                                   <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                                                                   <div class=\"col-md-6\">
                                                                                   
                                                                                       ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "titre", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                                                                   \t
\t\t\t\t\t\t\t\t\t\t\t</div>
                                                                                        


                                                                                </div>
                                                                                        
                                                                                <div class=\"form-group\">
                                                                                  <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                                                                   <div class=\"col-md-6\">
                                                                                  ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                                                                  </div>

                                                                                </div>

                                                                                  <div class=\"form-group\">
                                                                                  <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Thème</label> 
                                                                                   <div class=\"col-md-6\">
                                                                                  ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                                                                  </div>

                                                                                </div>
                                                                            

                                                                              ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                                                                              <input type=\"submit\" value=\"Modifier\" />

                                                                           </form>
\t\t\t\t\t\t\t\t</div>
                                                            
\t\t\t\t\t\t\t</section>
                                        
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                   </div>
                       ";
        // line 87
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig", 87)->display($context);
        echo "                                                         
                     ";
        // line 88
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig", 88)->display($context);
        echo "  
        </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 88,  125 => 87,  111 => 76,  102 => 70,  91 => 62,  77 => 51,  31 => 8,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/* 	<head>*/
/*               {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/* 	</head>*/
/*         <body>*/
/*             */
/*                                                                         */
/*                  {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}                                                               */
/*          */
/*                                 <section class="page-top">*/
/* 					<div class="container">*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<ul class="breadcrumb">*/
/* 									<li><a href="#">Home</a></li>*/
/* 									<li class="active">Probleme</li>*/
/* 								</ul>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<h1>Formulaire du Probleme</h1>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</section>*/
/*                               <div class="container">*/
/* 				*/
/*                                         */
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<section class="panel">*/
/* 								<header class="panel-heading">*/
/* 									<div class="panel-actions">*/
/* 										<a href="#" class="fa fa-caret-down"></a>*/
/* 										<a href="#" class="fa fa-times"></a>*/
/* 									</div>*/
/* 					*/
/* 									<h2 class="panel-title">Veuillez saisir les champs</h2>*/
/* 								</header>*/
/* 								<div class="panel-body">*/
/* 								*/
/*                                                                     */
/*                                                                       <form method="POST" class="form-horizontal form-bordered form-bordered" >*/
/*                                                                              */
/*                                                                                 <div class="form-group">*/
/* */
/*                                                                                    <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                                                                    <div class="col-md-6">*/
/*                                                                                    */
/*                                                                                        {{ form_widget(f.titre,{"attr": {'class': "form-control"}} )}}*/
/*                                                                                    	*/
/* 											</div>*/
/*                                                                                         */
/* */
/* */
/*                                                                                 </div>*/
/*                                                                                         */
/*                                                                                 <div class="form-group">*/
/*                                                                                   <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                                                                    <div class="col-md-6">*/
/*                                                                                   {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                                                                   </div>*/
/* */
/*                                                                                 </div>*/
/* */
/*                                                                                   <div class="form-group">*/
/*                                                                                   <label class="col-md-3 control-label" for="textareaDefault">Thème</label> */
/*                                                                                    <div class="col-md-6">*/
/*                                                                                   {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                                                                   </div>*/
/* */
/*                                                                                 </div>*/
/*                                                                             */
/* */
/*                                                                               {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                                                                               <input type="submit" value="Modifier" />*/
/* */
/*                                                                            </form>*/
/* 								</div>*/
/*                                                             */
/* 							</section>*/
/*                                         */
/* 						</div>*/
/* 					</div>*/
/*                    </div>*/
/*                        {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}                                                         */
/*                      {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}  */
/*         </body>*/
/* </html>*/
