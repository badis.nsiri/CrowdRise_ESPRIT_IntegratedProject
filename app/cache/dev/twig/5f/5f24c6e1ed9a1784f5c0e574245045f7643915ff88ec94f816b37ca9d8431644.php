<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_b9f92a24862b3e82b0d440c3aa7d7b0eeb3cdb812d8d0d5dbb9ff4f03e006be9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_inscrit' => array($this, 'block_fos_user_inscrit'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('fos_user_inscrit', $context, $blocks);
    }

    public function block_fos_user_inscrit($context, array $blocks = array())
    {
        // line 2
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 2)->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  20 => 1,);
    }
}
/* {% block fos_user_inscrit %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock %}*/
