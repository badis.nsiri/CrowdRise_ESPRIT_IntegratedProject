<?php

/* EasyAdminBundle:default:label_undefined.html.twig */
class __TwigTemplate_4e1ef55c9190fbe024a8eb58be1965b4c9c5cff33d21d0b6f72a8fdf44ab7e8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_undefined.html.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }
}
/* {# this template is rendered when an error or exception occurs while*/
/*    trying to get the value of the field (by default nothing is displayed) #}*/
/* */
