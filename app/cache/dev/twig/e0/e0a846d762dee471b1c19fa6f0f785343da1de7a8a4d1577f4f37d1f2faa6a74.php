<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig */
class __TwigTemplate_5c9a18c3a83bf8662d0e33815c524297e359667805fb57ecf359ee2bbf0b071a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
\t<head>
            ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig", 5)->display($context);
        // line 6
        echo "\t</head>
\t<body>

\t\t<div class=\"body\">
\t\t    ";
        // line 10
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig", 10)->display($context);
        // line 11
        echo "
\t\t\t<div role=\"main\" class=\"main\">
\t\t\t
                            <section class=\"page-top\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>
\t\t\t\t\t\t\t\t\t<li class=\"active\">Portfolio</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<h1>Single Project</h1>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>

\t\t\t\t<div class=\"container\">

\t\t\t\t\t<div class=\"portfolio-title\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"portfolio-nav-all col-md-1\">
\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" data-tooltip data-original-title=\"Back to list\"><i class=\"fa fa-th\"></i></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-10 center\">
\t\t\t\t\t\t\t\t<h2 class=\"shorter\">Lorem Ipsum Dolor</h2>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"portfolio-nav col-md-1\">
\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" class=\"portfolio-nav-prev\" data-tooltip data-original-title=\"Previous\"><i class=\"fa fa-chevron-left\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" class=\"portfolio-nav-next\" data-tooltip data-original-title=\"Next\"><i class=\"fa fa-chevron-right\"></i></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<hr class=\"tall\">

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-4\">

\t\t\t\t\t\t\t<div class=\"owl-carousel\" data-plugin-options='{\"items\": 1}'>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<div class=\"thumbnail\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<div class=\"thumbnail\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-1.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t<div class=\"thumbnail\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-2.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-md-8\">

\t\t\t\t\t\t\t<div class=\"portfolio-info\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 center\">
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-tooltip data-original-title=\"Like\"><i class=\"fa fa-heart\"></i>14</a>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i> 21 November 2013
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-tags\"></i> <a href=\"#\">Brand</a>, <a href=\"#\">Design</a>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<h4>Project <strong>Description</strong></h4>
\t\t\t\t\t\t\t<p class=\"taller\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti. Proin quis eros odio, dapibus dictum mauris. Donec nisi libero, adipiscing id pretium eget, consectetur sit amet leo. Nam at eros quis mi egestas fringilla non nec purus.</p>

\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Live Preview</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>

\t\t\t\t\t\t\t<ul class=\"portfolio-details\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<p><strong>Skills:</strong></p>

\t\t\t\t\t\t\t\t\t<ul class=\"list list-skills icons list-unstyled list-inline\">
\t\t\t\t\t\t\t\t\t\t<li><i class=\"fa fa-check-circle\"></i> Design</li>
\t\t\t\t\t\t\t\t\t\t<li><i class=\"fa fa-check-circle\"></i> HTML/CSS</li>
\t\t\t\t\t\t\t\t\t\t<li><i class=\"fa fa-check-circle\"></i> Javascript</li>
\t\t\t\t\t\t\t\t\t\t<li><i class=\"fa fa-check-circle\"></i> Backend</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<p><strong>Client:</strong></p>
\t\t\t\t\t\t\t\t\t<p>Okler Themes</p>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<hr class=\"tall\" />

\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<h3>Related <strong>Work</strong></h3>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<ul class=\"portfolio-list\">
\t\t\t\t\t\t\t<li class=\"col-md-3 col-xs-6\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">SEO</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Website</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-xs-6\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-1.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">Okler</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Brand</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-xs-6\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-2.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">The Fly</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Logo</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-xs-6\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"portfolio-single-project.html\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-3.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">The Code</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Website</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>

\t\t\t\t\t</div>

\t\t\t\t</div>

\t\t\t</div>

\t\t\t<section class=\"call-to-action featured footer\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"center\">
\t\t\t\t\t\t\t<h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987\" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</section>

                          ";
        // line 202
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig", 202)->display($context);
        // line 203
        echo "\t\t</div>

\t\t";
        // line 205
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig", 205)->display($context);
        // line 206
        echo "\t</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 206,  254 => 205,  250 => 203,  248 => 202,  216 => 173,  199 => 159,  182 => 145,  165 => 131,  98 => 67,  90 => 62,  82 => 57,  34 => 11,  32 => 10,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/* 	<head>*/
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/* 	</head>*/
/* 	<body>*/
/* */
/* 		<div class="body">*/
/* 		    {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/* */
/* 			<div role="main" class="main">*/
/* 			*/
/*                             <section class="page-top">*/
/* 					<div class="container">*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<ul class="breadcrumb">*/
/* 									<li><a href="#">Home</a></li>*/
/* 									<li class="active">Portfolio</li>*/
/* 								</ul>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<h1>Single Project</h1>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</section>*/
/* */
/* 				<div class="container">*/
/* */
/* 					<div class="portfolio-title">*/
/* 						<div class="row">*/
/* 							<div class="portfolio-nav-all col-md-1">*/
/* 								<a href="portfolio-single-project.html" data-tooltip data-original-title="Back to list"><i class="fa fa-th"></i></a>*/
/* 							</div>*/
/* 							<div class="col-md-10 center">*/
/* 								<h2 class="shorter">Lorem Ipsum Dolor</h2>*/
/* 							</div>*/
/* 							<div class="portfolio-nav col-md-1">*/
/* 								<a href="portfolio-single-project.html" class="portfolio-nav-prev" data-tooltip data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>*/
/* 								<a href="portfolio-single-project.html" class="portfolio-nav-next" data-tooltip data-original-title="Next"><i class="fa fa-chevron-right"></i></a>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 					<hr class="tall">*/
/* */
/* 					<div class="row">*/
/* 						<div class="col-md-4">*/
/* */
/* 							<div class="owl-carousel" data-plugin-options='{"items": 1}'>*/
/* 								<div>*/
/* 									<div class="thumbnail">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/* 									</div>*/
/* 								</div>*/
/* 								<div>*/
/* 									<div class="thumbnail">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-1.jpg')}}">*/
/* 									</div>*/
/* 								</div>*/
/* 								<div>*/
/* 									<div class="thumbnail">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-2.jpg')}}">*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* */
/* 						</div>*/
/* */
/* 						<div class="col-md-8">*/
/* */
/* 							<div class="portfolio-info">*/
/* 								<div class="row">*/
/* 									<div class="col-md-12 center">*/
/* 										<ul>*/
/* 											<li>*/
/* 												<a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i>14</a>*/
/* 											</li>*/
/* 											<li>*/
/* 												<i class="fa fa-calendar"></i> 21 November 2013*/
/* 											</li>*/
/* 											<li>*/
/* 												<i class="fa fa-tags"></i> <a href="#">Brand</a>, <a href="#">Design</a>*/
/* 											</li>*/
/* 										</ul>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* */
/* 							<h4>Project <strong>Description</strong></h4>*/
/* 							<p class="taller">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti. Proin quis eros odio, dapibus dictum mauris. Donec nisi libero, adipiscing id pretium eget, consectetur sit amet leo. Nam at eros quis mi egestas fringilla non nec purus.</p>*/
/* */
/* 							<a href="#" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Live Preview</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/* */
/* 							<ul class="portfolio-details">*/
/* 								<li>*/
/* 									<p><strong>Skills:</strong></p>*/
/* */
/* 									<ul class="list list-skills icons list-unstyled list-inline">*/
/* 										<li><i class="fa fa-check-circle"></i> Design</li>*/
/* 										<li><i class="fa fa-check-circle"></i> HTML/CSS</li>*/
/* 										<li><i class="fa fa-check-circle"></i> Javascript</li>*/
/* 										<li><i class="fa fa-check-circle"></i> Backend</li>*/
/* 									</ul>*/
/* 								</li>*/
/* 								<li>*/
/* 									<p><strong>Client:</strong></p>*/
/* 									<p>Okler Themes</p>*/
/* 								</li>*/
/* 							</ul>*/
/* */
/* 						</div>*/
/* 					</div>*/
/* */
/* 					<hr class="tall" />*/
/* */
/* 					<div class="row">*/
/* */
/* 						<div class="col-md-12">*/
/* 							<h3>Related <strong>Work</strong></h3>*/
/* 						</div>*/
/* */
/* 						<ul class="portfolio-list">*/
/* 							<li class="col-md-3 col-xs-6">*/
/* 								<div class="portfolio-item thumbnail">*/
/* 									<a href="portfolio-single-project.html" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">SEO</span>*/
/* 											<span class="thumb-info-type">Website</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-xs-6">*/
/* 								<div class="portfolio-item thumbnail">*/
/* 									<a href="portfolio-single-project.html" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-1.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">Okler</span>*/
/* 											<span class="thumb-info-type">Brand</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-xs-6">*/
/* 								<div class="portfolio-item thumbnail">*/
/* 									<a href="portfolio-single-project.html" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-2.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">The Fly</span>*/
/* 											<span class="thumb-info-type">Logo</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-xs-6">*/
/* 								<div class="portfolio-item thumbnail">*/
/* 									<a href="portfolio-single-project.html" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-3.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">The Code</span>*/
/* 											<span class="thumb-info-type">Website</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 						</ul>*/
/* */
/* 					</div>*/
/* */
/* 				</div>*/
/* */
/* 			</div>*/
/* */
/* 			<section class="call-to-action featured footer">*/
/* 				<div class="container">*/
/* 					<div class="row">*/
/* 						<div class="center">*/
/* 							<h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</section>*/
/* */
/*                           {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/* 		</div>*/
/* */
/* 		{% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}*/
/* 	</body>*/
/* </html>*/
/* */
