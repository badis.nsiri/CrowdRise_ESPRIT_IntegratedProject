<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig */
class __TwigTemplate_a05181b466752bb7c374733fb51708e61349dfb4fab43742093ab33ce2a857c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
\t<head>
             ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig", 3)->display($context);
        // line 4
        echo "\t</head>
        <body>
            
                     ";
        // line 7
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig", 7)->display($context);
        echo "                                                    
                                                                              
        
                                <section class=\"page-top\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>
\t\t\t\t\t\t\t\t\t<li class=\"active\">Probleme</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<h1>Formulaire du Probleme</h1>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
                              <div class=\"container\">
\t\t\t\t
                                        
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t 
\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Veuillez saisir les champs</h2>
\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t
                                                                    
                                                                      <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" action=\"#\">
                                                                             
                                                                              <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "titre", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Thème</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>
                                                                            
                                        
                                                                              ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                                                                              <a class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i><input type=\"submit\" value=\"Ajouter\" /></a><span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span> 
                                                                              <a class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Modifier</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
 
                                                                              
                                                                              
                                                                              
                                                                           </form>
\t\t\t\t\t\t\t\t</div>
                                                            
\t\t\t\t\t\t\t</section>
                                        
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                   </div>
                          ";
        // line 88
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig", 88)->display($context);
        echo "   
                          
                          ";
        // line 90
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig", 90)->display($context);
        // line 91
        echo "                        
        </body>
</html>
        
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 91,  131 => 90,  126 => 88,  108 => 73,  99 => 67,  88 => 59,  74 => 48,  30 => 7,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/* 	<head>*/
/*              {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/* 	</head>*/
/*         <body>*/
/*             */
/*                      {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}                                                    */
/*                                                                               */
/*         */
/*                                 <section class="page-top">*/
/* 					<div class="container">*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<ul class="breadcrumb">*/
/* 									<li><a href="#">Home</a></li>*/
/* 									<li class="active">Probleme</li>*/
/* 								</ul>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<h1>Formulaire du Probleme</h1>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</section>*/
/*                               <div class="container">*/
/* 				*/
/*                                         */
/* 					<div class="row">*/
/* 						<div class="col-lg-12">*/
/* 							<section class="panel">*/
/* 								<header class="panel-heading">*/
/* 									 */
/* 					*/
/* 									<h2 class="panel-title">Veuillez saisir les champs</h2>*/
/* 								</header>*/
/* 								<div class="panel-body">*/
/* 								*/
/*                                                                     */
/*                                                                       <form method="POST" class="form-horizontal form-bordered form-bordered" action="#">*/
/*                                                                              */
/*                                                                               <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.titre,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Thème</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/*                                                                             */
/*                                         */
/*                                                                               {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                                                                               <a class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i><input type="submit" value="Ajouter" /></a><span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span> */
/*                                                                               <a class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Modifier</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*  */
/*                                                                               */
/*                                                                               */
/*                                                                               */
/*                                                                            </form>*/
/* 								</div>*/
/*                                                             */
/* 							</section>*/
/*                                         */
/* 						</div>*/
/* 					</div>*/
/*                    </div>*/
/*                           {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}   */
/*                           */
/*                           {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}*/
/*                         */
/*         </body>*/
/* </html>*/
/*         */
/* */
