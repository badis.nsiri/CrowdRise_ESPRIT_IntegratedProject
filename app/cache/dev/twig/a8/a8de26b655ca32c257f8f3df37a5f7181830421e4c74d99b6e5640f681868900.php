<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig */
class __TwigTemplate_1529834481a9a7dda4720657eb8a718c7f1c0e275e617b383d4c52489b8f859b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig", 8)->display($context);
        echo " 
        <div class=\"body\">


            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Projet</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Projet</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class=\"container\">

                    <div class=\"portfolio-title\">
                        <div class=\"row\">
                            <div class=\"portfolio-nav-all col-md-1\">


                                <a href=\"\" data-tooltip data-original-title=\"Back to list\"><i class=\"fa fa-th\"></i></a>
                            </div>
                            <div class=\"col-md-10 center\">
                                <h2 class=\"shorter\">";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "titre", array()), "html", null, true);
        echo "</h2>
                            </div>


                        </div>
                    </div>

                    <hr class=\"tall\">

                    <div class=\"row\">
                        <div class=\"col-md-4\">

                            <div class=\"owl-carousel\" data-plugin-options='{\"items\": 1}'>
                                <div>
                                    <div class=\"thumbnail\">
                                        <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
        echo "\">
                                    </div>
                                </div>
                                <div>
                                    <div class=\"thumbnail\">
                                        <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-1.jpg"), "html", null, true);
        echo "\">
                                    </div>
                                </div>
                                <div>
                                    <div class=\"thumbnail\">
                                        <img alt=\"\" class=\"img-responsive\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-2.jpg"), "html", null, true);
        echo "\">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class=\"col-md-8\">

                            <div class=\"portfolio-info\">
                                <div class=\"row\">
                                    <div class=\"col-md-12 center\">
                                        <ul>
                                            <li>
                                                <a href=\"#\" data-tooltip data-original-title=\"Like\"><i class=\"fa fa-heart\"></i>14</a>
                                            </li>
                                            <li>
                                                <i class=\"fa fa-calendar\"></i> 21 November 2013
                                            </li>
                                            <li>
                                                <i class=\"fa fa-tags\"></i> <a href=\"#\">Brand</a>, <a href=\"#\">Design</a>
                                            </li>
                                            <div class=\"col-md-12\">

                                                <hr class=\"visible-sm visible-xs tall\" />

                                                <br/><br/>

                                                <div class=\"center\">
                                                    <div class=\"circular-bar\">
                                                        <div class=\"circular-bar-chart\" data-percent=\"";
        // line 97
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "sommeTemporaire", array()) / $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "montant", array())) * 100), "html", null, true);
        echo "\" data-plugin-options='{\"barColor\": \"#0088CC\"}'>
                                                            <strong> Montant </strong>
                                                            <label>";
        // line 99
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "sommeTemporaire", array()) / $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "montant", array())) * 100), "html", null, true);
        echo " % </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </ul>

                                    </div>

                                </div>

                            </div>
                            <br/><br/>
                            <p><h4>Type d'investissement:</h4></p>
                            ";
        // line 115
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            echo " 
                                <ul class=\"list list-skills icons list-unstyled list-inline\">
                                    <li><i class=\"fa fa-check-circle\"></i><li>  <form action=\"";
            // line 117
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Don", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()), "iduser" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\" method=\"POST\"> <input type=\"submit\" class=\"btn btn-primary btn-xs\" value=\"Don\" /> </form></li>  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "don", array()), "html", null, true);
            echo " Dt  </li><br>
                                    <li><i class=\"fa fa-check-circle\"></i><li>  <form action=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Pret", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()), "iduser" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\" method=\"POST\"> <input type=\"submit\" class=\"btn btn-primary btn-xs\" value=\"Prêt\" /> </form></li> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "pret", array()), "html", null, true);
            echo " Dt </li> <br>
                                    <li><i class=\"fa fa-check-circle\"></i><li>  <form action=\"";
            // line 119
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Recompense", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()), "iduser" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\" method=\"POST\"> <input type=\"submit\" class=\"btn btn-primary btn-xs\" value=\"Récompense\" /> </form></li> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "recompense", array()), "html", null, true);
            echo " Dt  </li> <br>
                                    <li><i class=\"fa fa-check-circle\"></i><li>  <form action=\"";
            // line 120
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Investissement", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()), "iduser" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\" method=\"POST\"> <input type=\"submit\" class=\"btn btn-primary btn-xs\" value=\"Investissement\" /> </form></li> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "investissement", array()), "html", null, true);
            echo " Dt  </li> <br>

                                </ul>

                            ";
        }
        // line 124
        echo " 

                            <h4>Description du projet : </h4>
                            <p class=\"taller\">";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "description", array()), "html", null, true);
        echo "</p>


                            <ul class=\"portfolio-details\">
                                <li>

                                    <p><strong>Thême:</strong></p>

                                    <ul class=\"list list-skills icons list-unstyled list-inline\">


                                        <li><i class=\"fa fa-check-circle\"> </i>  <li>";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "theme", array()), "html", null, true);
        echo "</li> <br>

                                    </ul>
                                </li>








                                <ul class=\"portfolio-details\">

                                    <div class=\"form-group\">




                                        <div class=\"col-md-6\">
                                            <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Commentaire</label>
                                            <div class=\"col-md-6\">
                                                <form  action=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("addCommentaireProjet", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
        echo "\">
                                                    <textarea class=\"form-control\" rows=\"3\" data-plugin-maxlength maxlength=\"5000 \" name=\"CIdee\">
                                                    
                                                    </textarea>

                                                    <input type=\"submit\" /> <br>
                                                </form>


                                            </div>

                                        </div>




                                    </div>  

                                    ";
        // line 178
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CrowdRiseBundle:Commentaire:showCommentaireByProjet", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))));
        echo " 


                                    ";
        // line 181
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "user", array()), "id", array()))) {
            echo "            
                                        <a href=\"";
            // line 182
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_projet", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Modifier</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
                                        <a href=\"";
            // line 183
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer_projet", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Supprimer</a> <span class=\"arrow hlb\" data-appear-animation=\"rotateInUpLeft\" data-appear-animation-delay=\"800\"></span>
                                        </span>
                                        <br/><br/><br/><br/>
                                        <p class=\"col-md-12\">";
            // line 186
            echo $this->env->getExtension('nomaya_social_bar')->getSocialButtons();
            echo "</p>
                                    ";
        }
        // line 188
        echo "                                </ul>

                        </div>
                    </div>

                    <hr class=\"tall\" />

                    <div class=\"row\">



                    </div>

                </div>

            </div>

            <section class=\"call-to-action featured footer\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"center\">
                            <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987
                                                                                                                                \" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
                        </div>
                    </div>
                </div>
            </section>

            ";
        // line 216
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig", 216)->display($context);
        // line 217
        echo "        </div>
        ";
        // line 218
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig", 218)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  316 => 218,  313 => 217,  311 => 216,  281 => 188,  276 => 186,  270 => 183,  266 => 182,  262 => 181,  256 => 178,  235 => 160,  210 => 138,  196 => 127,  191 => 124,  181 => 120,  175 => 119,  169 => 118,  163 => 117,  158 => 115,  139 => 99,  134 => 97,  101 => 67,  93 => 62,  85 => 57,  67 => 42,  30 => 8,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %} */
/*         <div class="body">*/
/* */
/* */
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Projet</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Projet</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <div class="container">*/
/* */
/*                     <div class="portfolio-title">*/
/*                         <div class="row">*/
/*                             <div class="portfolio-nav-all col-md-1">*/
/* */
/* */
/*                                 <a href="" data-tooltip data-original-title="Back to list"><i class="fa fa-th"></i></a>*/
/*                             </div>*/
/*                             <div class="col-md-10 center">*/
/*                                 <h2 class="shorter">{{m.titre}}</h2>*/
/*                             </div>*/
/* */
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall">*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-4">*/
/* */
/*                             <div class="owl-carousel" data-plugin-options='{"items": 1}'>*/
/*                                 <div>*/
/*                                     <div class="thumbnail">*/
/*                                         <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div>*/
/*                                     <div class="thumbnail">*/
/*                                         <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-1.jpg')}}">*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div>*/
/*                                     <div class="thumbnail">*/
/*                                         <img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-2.jpg')}}">*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/*                         </div>*/
/* */
/*                         <div class="col-md-8">*/
/* */
/*                             <div class="portfolio-info">*/
/*                                 <div class="row">*/
/*                                     <div class="col-md-12 center">*/
/*                                         <ul>*/
/*                                             <li>*/
/*                                                 <a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i>14</a>*/
/*                                             </li>*/
/*                                             <li>*/
/*                                                 <i class="fa fa-calendar"></i> 21 November 2013*/
/*                                             </li>*/
/*                                             <li>*/
/*                                                 <i class="fa fa-tags"></i> <a href="#">Brand</a>, <a href="#">Design</a>*/
/*                                             </li>*/
/*                                             <div class="col-md-12">*/
/* */
/*                                                 <hr class="visible-sm visible-xs tall" />*/
/* */
/*                                                 <br/><br/>*/
/* */
/*                                                 <div class="center">*/
/*                                                     <div class="circular-bar">*/
/*                                                         <div class="circular-bar-chart" data-percent="{{((m.sommeTemporaire)/(m.montant))*100}}" data-plugin-options='{"barColor": "#0088CC"}'>*/
/*                                                             <strong> Montant </strong>*/
/*                                                             <label>{{((m.sommeTemporaire)/(m.montant))*100}} % </label>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </div>*/
/* */
/*                                             </div>*/
/* */
/*                                         </ul>*/
/* */
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                             </div>*/
/*                             <br/><br/>*/
/*                             <p><h4>Type d'investissement:</h4></p>*/
/*                             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %} */
/*                                 <ul class="list list-skills icons list-unstyled list-inline">*/
/*                                     <li><i class="fa fa-check-circle"></i><li>  <form action="{{path('Don', {'id': m.id,'iduser': app.user.id})}}" method="POST"> <input type="submit" class="btn btn-primary btn-xs" value="Don" /> </form></li>  {{m.don}} Dt  </li><br>*/
/*                                     <li><i class="fa fa-check-circle"></i><li>  <form action="{{path('Pret', {'id': m.id,'iduser': app.user.id})}}" method="POST"> <input type="submit" class="btn btn-primary btn-xs" value="Prêt" /> </form></li> {{m.pret}} Dt </li> <br>*/
/*                                     <li><i class="fa fa-check-circle"></i><li>  <form action="{{path('Recompense', {'id': m.id,'iduser': app.user.id})}}" method="POST"> <input type="submit" class="btn btn-primary btn-xs" value="Récompense" /> </form></li> {{m.recompense}} Dt  </li> <br>*/
/*                                     <li><i class="fa fa-check-circle"></i><li>  <form action="{{path('Investissement', {'id': m.id,'iduser': app.user.id})}}" method="POST"> <input type="submit" class="btn btn-primary btn-xs" value="Investissement" /> </form></li> {{m.investissement}} Dt  </li> <br>*/
/* */
/*                                 </ul>*/
/* */
/*                             {% endif %} */
/* */
/*                             <h4>Description du projet : </h4>*/
/*                             <p class="taller">{{m.description}}</p>*/
/* */
/* */
/*                             <ul class="portfolio-details">*/
/*                                 <li>*/
/* */
/*                                     <p><strong>Thême:</strong></p>*/
/* */
/*                                     <ul class="list list-skills icons list-unstyled list-inline">*/
/* */
/* */
/*                                         <li><i class="fa fa-check-circle"> </i>  <li>{{m.theme}}</li> <br>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                 <ul class="portfolio-details">*/
/* */
/*                                     <div class="form-group">*/
/* */
/* */
/* */
/* */
/*                                         <div class="col-md-6">*/
/*                                             <label class="col-md-3 control-label" for="textareaDefault">Commentaire</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 <form  action="{{path('addCommentaireProjet',{'id':m.id})}}">*/
/*                                                     <textarea class="form-control" rows="3" data-plugin-maxlength maxlength="5000 " name="CIdee">*/
/*                                                     */
/*                                                     </textarea>*/
/* */
/*                                                     <input type="submit" /> <br>*/
/*                                                 </form>*/
/* */
/* */
/*                                             </div>*/
/* */
/*                                         </div>*/
/* */
/* */
/* */
/* */
/*                                     </div>  */
/* */
/*                                     {{ render(controller('CrowdRiseBundle:Commentaire:showCommentaireByProjet',{'id':m.id})) }} */
/* */
/* */
/*                                     {% if app.user.id == m.user.id %}            */
/*                                         <a href="{{path('modifier_projet', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Modifier</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*                                         <a href="{{path('supprimer_projet', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Supprimer</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>*/
/*                                         </span>*/
/*                                         <br/><br/><br/><br/>*/
/*                                         <p class="col-md-12">{{socialButtons()}}</p>*/
/*                                     {% endif %}*/
/*                                 </ul>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall" />*/
/* */
/*                     <div class="row">*/
/* */
/* */
/* */
/*                     </div>*/
/* */
/*                 </div>*/
/* */
/*             </div>*/
/* */
/*             <section class="call-to-action featured footer">*/
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="center">*/
/*                             <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987*/
/*                                                                                                                                 " target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
