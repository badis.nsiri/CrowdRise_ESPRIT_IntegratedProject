<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig */
class __TwigTemplate_0c58d6aa8791d87928368914e24a7fedaab84c47c39f02bf684d8e98bfad0b7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 11
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig", 11)->display($context);
        // line 12
        echo "            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Problème</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Problème</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class=\"container\">

                    <div class=\"portfolio-title\">
                        <div class=\"row\">
                            <div class=\"portfolio-nav-all col-md-1\">
                                <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("list_probleme");
        echo "\" data-tooltip data-original-title=\"Back to list\"><i class=\"fa fa-th\"></i></a>
                            </div>
                            <div class=\"col-md-10 center\">
                                <h2 class=\"shorter\">";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "titre", array()), "html", null, true);
        echo "</h2>
                            </div>




                        </div>
                    </div>

                    <hr class=\"tall\">

                    <div class=\"row\">
                        <div class=\"col-md-7\">

                            <h4><strong>Description du problème </strong></h4>      
                            <p class=\"taller\">";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "description", array()), "html", null, true);
        echo "</p>

                            <ul class=\"portfolio-details\">
                                <li>

                                    <p><strong>Thême:</strong></p>

                                    <ul class=\"list list-skills icons list-unstyled list-inline\">


                                        <li><i class=\"fa fa-check-circle\"> </i>  <li>";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "theme", array()), "html", null, true);
        echo "</li> <br>

                                    </ul>
                                </li>
                            </ul>

                            <div class=\"form-group\">






                                <div class=\"col-md-6\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Commentaire</label><br>
                                    <div class=\"col-md-6\">
                                        <form  action=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("addCommentaireProbleme", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
        echo "\">
                                            <textarea class=\"form-control\" rows=\"3\" data-plugin-maxlength maxlength=\"5000 \" name=\"CIdee\">
                                                    
                                            </textarea>

                                            <input type=\"submit\" /> <br>
                                        </form>
                                    </div>

                                </div>




                            </div>  

                            ";
        // line 97
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CrowdRiseBundle:Commentaire:showCommentaireByProbleme", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))));
        echo " 








                        </div>
                        <div class=\"col-md-5\">

                            <div class=\"portfolio-info\">
                                <div class=\"row\">
                                    <div class=\"col-md-12 center\">
                                        <ul>
                                            <li>
                                                <a href=\"#\" data-tooltip data-original-title=\"Like\"><i class=\"fa fa-heart\"></i>14</a>
                                            </li>

                                            <li>
                                                <i class=\"fa fa-tags\"></i>  ";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "etat", array()), "html", null, true);
        echo "
                                            </li>
                                            <br>
                                            <br>


                                            ";
        // line 124
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "user", array()), "id", array()))) {
            // line 125
            echo "                                                <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("problemeRes", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\">
                                                    ";
            // line 126
            if (($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "etat", array()) == "Resolu")) {
                echo "Non Resolu";
            }
            // line 127
            echo "                                                    ";
            if (($this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "etat", array()) == "Non Resolu")) {
                echo "Resolu";
            }
            echo " </a> <span ></span><br><br>

                                                <a href=\"";
            // line 129
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_probleme", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Modifier</a> <span  ></span>
                                                <a href=\"";
            // line 130
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supprimer_probleme", array("id" => $this->getAttribute((isset($context["m"]) ? $context["m"] : $this->getContext($context, "m")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-icon\"><i class=\"fa fa-external-link\"></i>Supprimer</a> <span  ></span>
                                            ";
        }
        // line 132
        echo "                                        </ul>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                    <hr class=\"tall\" />




                </div>

            </div>



            ";
        // line 155
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig", 155)->display($context);
        // line 156
        echo "        </div>
        ";
        // line 157
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig", 157)->display($context);
        echo "                                                                       
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 157,  225 => 156,  223 => 155,  198 => 132,  193 => 130,  189 => 129,  181 => 127,  177 => 126,  172 => 125,  170 => 124,  161 => 118,  137 => 97,  118 => 81,  99 => 65,  86 => 55,  68 => 40,  62 => 37,  35 => 12,  33 => 11,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Problème</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Problème</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <div class="container">*/
/* */
/*                     <div class="portfolio-title">*/
/*                         <div class="row">*/
/*                             <div class="portfolio-nav-all col-md-1">*/
/*                                 <a href="{{path('list_probleme')}}" data-tooltip data-original-title="Back to list"><i class="fa fa-th"></i></a>*/
/*                             </div>*/
/*                             <div class="col-md-10 center">*/
/*                                 <h2 class="shorter">{{m.titre}}</h2>*/
/*                             </div>*/
/* */
/* */
/* */
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall">*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-7">*/
/* */
/*                             <h4><strong>Description du problème </strong></h4>      */
/*                             <p class="taller">{{m.description}}</p>*/
/* */
/*                             <ul class="portfolio-details">*/
/*                                 <li>*/
/* */
/*                                     <p><strong>Thême:</strong></p>*/
/* */
/*                                     <ul class="list list-skills icons list-unstyled list-inline">*/
/* */
/* */
/*                                         <li><i class="fa fa-check-circle"> </i>  <li>{{m.theme}}</li> <br>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                             <div class="form-group">*/
/* */
/* */
/* */
/* */
/* */
/* */
/*                                 <div class="col-md-6">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Commentaire</label><br>*/
/*                                     <div class="col-md-6">*/
/*                                         <form  action="{{path('addCommentaireProbleme',{'id':m.id})}}">*/
/*                                             <textarea class="form-control" rows="3" data-plugin-maxlength maxlength="5000 " name="CIdee">*/
/*                                                     */
/*                                             </textarea>*/
/* */
/*                                             <input type="submit" /> <br>*/
/*                                         </form>*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/* */
/* */
/* */
/*                             </div>  */
/* */
/*                             {{ render(controller('CrowdRiseBundle:Commentaire:showCommentaireByProbleme',{'id':m.id})) }} */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                         </div>*/
/*                         <div class="col-md-5">*/
/* */
/*                             <div class="portfolio-info">*/
/*                                 <div class="row">*/
/*                                     <div class="col-md-12 center">*/
/*                                         <ul>*/
/*                                             <li>*/
/*                                                 <a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i>14</a>*/
/*                                             </li>*/
/* */
/*                                             <li>*/
/*                                                 <i class="fa fa-tags"></i>  {{m.etat}}*/
/*                                             </li>*/
/*                                             <br>*/
/*                                             <br>*/
/* */
/* */
/*                                             {% if app.user.id == m.user.id %}*/
/*                                                 <a href="{{path('problemeRes', {'id': m.id})}}" class="btn btn-primary btn-icon">*/
/*                                                     {% if m.etat=='Resolu' %}Non Resolu{% endif %}*/
/*                                                     {% if m.etat=='Non Resolu' %}Resolu{% endif %} </a> <span ></span><br><br>*/
/* */
/*                                                 <a href="{{path('modifier_probleme', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Modifier</a> <span  ></span>*/
/*                                                 <a href="{{path('supprimer_probleme', {'id': m.id})}}" class="btn btn-primary btn-icon"><i class="fa fa-external-link"></i>Supprimer</a> <span  ></span>*/
/*                                             {% endif %}*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/* */
/* */
/* */
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <hr class="tall" />*/
/* */
/* */
/* */
/* */
/*                 </div>*/
/* */
/*             </div>*/
/* */
/* */
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}                                                                       */
/*     </body>*/
/* </html>*/
