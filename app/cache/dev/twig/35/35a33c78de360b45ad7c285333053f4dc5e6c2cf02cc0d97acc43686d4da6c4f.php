<?php

/* CrowdRiseBundle:Commentaire:show.html.twig */
class __TwigTemplate_bf7fdd86377982a1f992325fb9c2cadcbfbc1395fc2c8bd359ad0d7529c2c173 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["coms"]) ? $context["coms"] : $this->getContext($context, "coms")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 3
            echo "    ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["i"], "projet", array())) != 0)) {
                // line 4
                echo "        Username (Projet )  : ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "author", array()), "username", array()), "html", null, true);
                echo "<br>
        ";
                // line 5
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "body", array()), "html", null, true);
                echo "    <br>
        ";
                // line 6
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["i"], "author", array()), "id", array()))) {
                    // line 7
                    echo "            
            <a href=\"";
                    // line 8
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("updateCommentaireProjet", array("id" => $this->getAttribute($context["i"], "id", array()), "id2" => $this->getAttribute($this->getAttribute($context["i"], "projet", array()), "id", array()))), "html", null, true);
                    echo "\">Modifier</a>    
            <a href=\"";
                    // line 9
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("deleteCommentaireProjet", array("id" => $this->getAttribute($context["i"], "id", array()), "id2" => $this->getAttribute($this->getAttribute($context["i"], "projet", array()), "id", array()))), "html", null, true);
                    echo "\">Supprimer</a>   <br>
        ";
                }
                // line 11
                echo "    ";
            }
            // line 12
            echo "    ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["i"], "idee", array())) != 0)) {
                // line 13
                echo "        Username (Idee ): ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "author", array()), "username", array()), "html", null, true);
                echo "<br>
        ";
                // line 14
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "body", array()), "html", null, true);
                echo " <br>
        ";
                // line 15
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["i"], "author", array()), "id", array()))) {
                    // line 16
                    echo "
            <a href=\"";
                    // line 17
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("updateCommentaireIdee", array("id" => $this->getAttribute($context["i"], "id", array()), "id2" => $this->getAttribute($this->getAttribute($context["i"], "idee", array()), "id", array()))), "html", null, true);
                    echo "\">Modifier</a>   
            <a href=\"";
                    // line 18
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("deleteCommentaireIdee", array("id" => $this->getAttribute($context["i"], "id", array()), "id2" => $this->getAttribute($this->getAttribute($context["i"], "idee", array()), "id", array()))), "html", null, true);
                    echo "\">Supprimer</a>   <br>
        ";
                }
                // line 20
                echo "
    ";
            }
            // line 22
            echo "    ";
            if ((twig_length_filter($this->env, $this->getAttribute($context["i"], "probleme", array())) != 0)) {
                // line 23
                echo "        Username (Probleme ) : ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "author", array()), "username", array()), "html", null, true);
                echo "<br>
        ";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "body", array()), "html", null, true);
                echo " <br>
        ";
                // line 25
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["i"], "author", array()), "id", array()))) {
                    // line 26
                    echo "
            <a href=\"";
                    // line 27
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("updateCommentaireProbleme", array("id" => $this->getAttribute($context["i"], "id", array()), "id2" => $this->getAttribute($this->getAttribute($context["i"], "probleme", array()), "id", array()))), "html", null, true);
                    echo "\">Modifier</a>    
            <a href=\"";
                    // line 28
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("deleteCommentaireProbleme", array("id" => $this->getAttribute($context["i"], "id", array()), "id2" => $this->getAttribute($this->getAttribute($context["i"], "probleme", array()), "id", array()))), "html", null, true);
                    echo "\">Supprimer</a>   <br>
        ";
                }
                // line 30
                echo "
    ";
            }
            // line 31
            echo " 
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Commentaire:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 31,  111 => 30,  106 => 28,  102 => 27,  99 => 26,  97 => 25,  93 => 24,  88 => 23,  85 => 22,  81 => 20,  76 => 18,  72 => 17,  69 => 16,  67 => 15,  63 => 14,  58 => 13,  55 => 12,  52 => 11,  47 => 9,  43 => 8,  40 => 7,  38 => 6,  34 => 5,  29 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* */
/* {% for i in coms %}*/
/*     {% if i.projet |length != 0 %}*/
/*         Username (Projet )  : {{i.author.username}}<br>*/
/*         {{ i.body}}    <br>*/
/*         {% if app.user.id == i.author.id %}*/
/*             */
/*             <a href="{{path('updateCommentaireProjet',{'id':i.id,'id2':i.projet.id} )}}">Modifier</a>    */
/*             <a href="{{path('deleteCommentaireProjet',{'id':i.id,'id2':i.projet.id} )}}">Supprimer</a>   <br>*/
/*         {% endif %}*/
/*     {% endif %}*/
/*     {% if i.idee |length != 0 %}*/
/*         Username (Idee ): {{i.author.username}}<br>*/
/*         {{ i.body}} <br>*/
/*         {% if app.user.id == i.author.id %}*/
/* */
/*             <a href="{{path('updateCommentaireIdee',{'id':i.id,'id2':i.idee.id} )}}">Modifier</a>   */
/*             <a href="{{path('deleteCommentaireIdee',{'id':i.id,'id2':i.idee.id} )}}">Supprimer</a>   <br>*/
/*         {% endif %}*/
/* */
/*     {% endif %}*/
/*     {% if i.probleme |length != 0 %}*/
/*         Username (Probleme ) : {{i.author.username}}<br>*/
/*         {{ i.body}} <br>*/
/*         {% if app.user.id == i.author.id %}*/
/* */
/*             <a href="{{path('updateCommentaireProbleme',{'id':i.id,'id2':i.probleme.id} )}}">Modifier</a>    */
/*             <a href="{{path('deleteCommentaireProbleme',{'id':i.id,'id2':i.probleme.id} )}}">Supprimer</a>   <br>*/
/*         {% endif %}*/
/* */
/*     {% endif %} */
/* {% endfor %}*/
