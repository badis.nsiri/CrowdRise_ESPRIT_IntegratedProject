<?php

/* CrowdRiseBundle:Commentaire:update.html.twig */
class __TwigTemplate_59b7ce749b3447f81f936f0c8e34183f9300ff79a9bcc694e972e85dc0eafc2b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:Commentaire:update.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:Commentaire:update.html.twig", 8)->display($context);
        echo " 
        <div class=\"body\">
            <div role=\"main\" class=\"main\">
                ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'form');
        echo "
            </div>  
        </div>
        ";
        // line 14
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:Commentaire:update.html.twig", 14)->display($context);
        // line 15
        echo "
        ";
        // line 16
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:Commentaire:update.html.twig", 16)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Commentaire:update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 16,  44 => 15,  42 => 14,  36 => 11,  30 => 8,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %} */
/*         <div class="body">*/
/*             <div role="main" class="main">*/
/*                 {{ form(f)}}*/
/*             </div>  */
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
