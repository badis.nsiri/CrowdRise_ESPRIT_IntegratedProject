<?php

/* CrowdRiseBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_801e41175973bcce047dd4d8fec3f940275aa3b148983456cf79947182b41b7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " 
<p>
    
";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")))), "html", null, true);
        echo "
</p>
 
";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }
}
/*  */
/* <p>*/
/*     */
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/*  */
/* */
