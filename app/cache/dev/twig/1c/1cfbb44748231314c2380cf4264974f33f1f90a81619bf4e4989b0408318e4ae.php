<?php

/* CrowdRiseBundle:Includes/List:listProblemes.html.twig */
class __TwigTemplate_fd28d4cdba2cdfc420df759aa92d8a263d9ead24c045fa24c328a8332bb52056 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "                    <div class=\"container\">
\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<hr class=\"tall\" />
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t</div>
                        <section class=\"page-top\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>
\t\t\t\t\t\t\t\t\t<li class=\"active\">Problèmes</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<h1>Découvrir</h1>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                        </section>
\t\t\t\t\t<h2>Problèmes</h2>

\t\t\t\t\t<ul class=\"nav nav-pills sort-source\" data-sort-id=\"portfolio\" data-option-key=\"filter\">
\t\t\t\t\t\t<li data-option-value=\"*\" class=\"active\"><a href=\"#\">Tous</a></li>
\t\t\t\t\t\t<li data-option-value=\".websites\"><a href=\"#\">Personels</a></li>
\t\t\t\t\t\t<li data-option-value=\".logos\"><a href=\"#\">Taches</a></li>
\t\t\t\t\t\t<li data-option-value=\".brands\"><a href=\"#\">Communitaires</a></li>
\t\t\t\t\t</ul>

\t\t\t\t\t<hr />

                                        <div class=\"row\">

\t\t\t\t\t\t<ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">SEO</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Website</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item brands\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-1.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">Okler</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Brand</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item logos\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 69
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-2.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">The Fly</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Logo</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 83
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-3.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">The Code</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Website</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-3.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">The Code</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Website</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 111
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">SEO</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Website</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item brands\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-1.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">Okler</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Brand</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item logos\">
\t\t\t\t\t\t\t\t<div class=\"portfolio-item img-thumbnail\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 139
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\" class=\"thumb-info\">
\t\t\t\t\t\t\t\t\t\t<img alt=\"\" class=\"img-responsive\" src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/projects/project-2.jpg"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-title\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-inner\">The Fly</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-type\">Logo</span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"thumb-info-action\">
\t\t\t\t\t\t\t\t\t\t\t<span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Includes/List:listProblemes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 140,  201 => 139,  185 => 126,  181 => 125,  165 => 112,  161 => 111,  145 => 98,  141 => 97,  125 => 84,  121 => 83,  105 => 70,  101 => 69,  85 => 56,  81 => 55,  65 => 42,  61 => 41,  19 => 1,);
    }
}
/*                     <div class="container">*/
/* 				*/
/* 					<div class="row">*/
/* 						<hr class="tall" />*/
/* 					</div>*/
/* 				*/
/* 				</div>*/
/*                         <section class="page-top">*/
/* 					<div class="container">*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<ul class="breadcrumb">*/
/* 									<li><a href="#">Home</a></li>*/
/* 									<li class="active">Problèmes</li>*/
/* 								</ul>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="row">*/
/* 							<div class="col-md-12">*/
/* 								<h1>Découvrir</h1>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/*                         </section>*/
/* 					<h2>Problèmes</h2>*/
/* */
/* 					<ul class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter">*/
/* 						<li data-option-value="*" class="active"><a href="#">Tous</a></li>*/
/* 						<li data-option-value=".websites"><a href="#">Personels</a></li>*/
/* 						<li data-option-value=".logos"><a href="#">Taches</a></li>*/
/* 						<li data-option-value=".brands"><a href="#">Communitaires</a></li>*/
/* 					</ul>*/
/* */
/* 					<hr />*/
/* */
/*                                         <div class="row">*/
/* */
/* 						<ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">SEO</span>*/
/* 											<span class="thumb-info-type">Website</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item brands">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-1.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">Okler</span>*/
/* 											<span class="thumb-info-type">Brand</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item logos">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-2.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">The Fly</span>*/
/* 											<span class="thumb-info-type">Logo</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-3.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">The Code</span>*/
/* 											<span class="thumb-info-type">Website</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-3.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">The Code</span>*/
/* 											<span class="thumb-info-type">Website</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">SEO</span>*/
/* 											<span class="thumb-info-type">Website</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item brands">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-1.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">Okler</span>*/
/* 											<span class="thumb-info-type">Brand</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 							<li class="col-md-3 col-sm-6 col-xs-12 isotope-item logos">*/
/* 								<div class="portfolio-item img-thumbnail">*/
/* 									<a href="{{ path('projet')}}" class="thumb-info">*/
/* 										<img alt="" class="img-responsive" src="{{asset('FontOffice/img/projects/project-2.jpg')}}">*/
/* 										<span class="thumb-info-title">*/
/* 											<span class="thumb-info-inner">The Fly</span>*/
/* 											<span class="thumb-info-type">Logo</span>*/
/* 										</span>*/
/* 										<span class="thumb-info-action">*/
/* 											<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/* 										</span>*/
/* 									</a>*/
/* 								</div>*/
/* 							</li>*/
/* 						</ul>*/
/* 					</div>*/
