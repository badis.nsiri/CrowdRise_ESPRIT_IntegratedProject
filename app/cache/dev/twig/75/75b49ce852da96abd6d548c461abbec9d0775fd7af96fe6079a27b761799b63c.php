<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig */
class __TwigTemplate_801cbcda7528ce29a027820312959b16069a691097600591049eef4aacbfa2eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig", 3)->display($context);
        // line 4
        echo "    </head>
    <body>

        ";
        // line 7
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig", 7)->display($context);
        echo "   


        <section class=\"page-top\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <ul class=\"breadcrumb\">
                            <li><a href=\"#\">Home</a></li>
                            <li class=\"active\">Evenement</li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h1>Modifier votre evenement</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <section class=\"panel\">
                        <header class=\"panel-heading\">


                            <h2 class=\"panel-title\">Veuillez saisir les champs</h2>
                        </header>
                        <div class=\"panel-body\">


                            <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" >

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Theme</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Lieu:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "lieu", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>    

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Date du debut :</label>
                                    <div class=\"col-md-6\">
                                        ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "dateDebut", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Date de fin :</label>
                                    <div class=\"col-md-6\">
                                        ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "dateFin", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <div class=\"form-group\">
                                        <label class=\"col-md-3 control-label\" for=\"inputDefault\">Heure de debut :</label>
                                        <div class=\"col-md-6\">
                                            ";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "heureDebut", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                        </div> 
                                    </div> 

                                    <div class=\"form-group\">
                                        <label class=\"col-md-3 control-label\" for=\"inputDefault\">Heure de fin :</label>
                                        <div class=\"col-md-6\">
                                            ";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "heureFin", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                        </div>
                                    </div>

                                    <div class=\"form-group\">
                                        <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Image</label> 
                                        <div class=\"col-md-6\">
                                            ";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "file", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "    
                                        </div>  
                                    </div>      




                                    ";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                                    <input type=\"submit\" value=\"Modifier\" />

                            </form>
                        </div>

                    </section>

                </div>
            </div>
        </div>
        ";
        // line 127
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig", 127)->display($context);
        // line 128
        echo "
        ";
        // line 129
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig", 129)->display($context);
        echo "           

    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 129,  185 => 128,  183 => 127,  169 => 116,  159 => 109,  149 => 102,  139 => 95,  128 => 87,  118 => 80,  108 => 73,  96 => 64,  86 => 57,  72 => 46,  30 => 7,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}   */
/* */
/* */
/*         <section class="page-top">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <ul class="breadcrumb">*/
/*                             <li><a href="#">Home</a></li>*/
/*                             <li class="active">Evenement</li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h1>Modifier votre evenement</h1>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <section class="panel">*/
/*                         <header class="panel-heading">*/
/* */
/* */
/*                             <h2 class="panel-title">Veuillez saisir les champs</h2>*/
/*                         </header>*/
/*                         <div class="panel-body">*/
/* */
/* */
/*                             <form method="POST" class="form-horizontal form-bordered form-bordered" >*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.nom,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Theme</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Lieu:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.lieu,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>    */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="inputDefault">Date du debut :</label>*/
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.dateDebut,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="inputDefault">Date de fin :</label>*/
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.dateFin,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <div class="form-group">*/
/*                                         <label class="col-md-3 control-label" for="inputDefault">Heure de debut :</label>*/
/*                                         <div class="col-md-6">*/
/*                                             {{ form_widget(f.heureDebut,{"attr": {'class': "form-control"}} )}}*/
/*                                         </div> */
/*                                     </div> */
/* */
/*                                     <div class="form-group">*/
/*                                         <label class="col-md-3 control-label" for="inputDefault">Heure de fin :</label>*/
/*                                         <div class="col-md-6">*/
/*                                             {{ form_widget(f.heureFin,{"attr": {'class': "form-control"}} )}}*/
/*                                         </div>*/
/*                                     </div>*/
/* */
/*                                     <div class="form-group">*/
/*                                         <label class="col-md-3 control-label" for="textareaDefault">Image</label> */
/*                                         <div class="col-md-6">*/
/*                                             {{ form_widget(f.file,{"attr": {'class': "form-control"}})}}    */
/*                                         </div>  */
/*                                     </div>      */
/* */
/* */
/* */
/* */
/*                                     {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                                     <input type="submit" value="Modifier" />*/
/* */
/*                             </form>*/
/*                         </div>*/
/* */
/*                     </section>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}           */
/* */
/*     </body>*/
/* </html>*/
