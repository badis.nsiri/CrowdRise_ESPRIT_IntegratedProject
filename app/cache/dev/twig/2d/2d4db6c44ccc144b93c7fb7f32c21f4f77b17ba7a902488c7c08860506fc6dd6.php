<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig */
class __TwigTemplate_9f20f1daf61b094c96182c64cf67fd94bf92491837fc6c0bf0ae315e4697d7d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig", 3)->display($context);
        // line 4
        echo "    </head>
    <body>

        ";
        // line 7
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig", 7)->display($context);
        echo "    

        
        <section class=\"page-top\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <ul class=\"breadcrumb\">
                            <li><a href=\"#\">Home</a></li>
                            <li class=\"active\">Probleme</li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h1>Formulaire du projet</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class=\"container\">


            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <section class=\"panel\">
                        <header class=\"panel-heading\">
                            <div class=\"panel-actions\">
                                <a href=\"#\" class=\"fa fa-caret-down\"></a>
                                <a href=\"#\" class=\"fa fa-times\"></a>
                            </div>

                            <h2 class=\"panel-title\">Veuillez saisir les champs</h2>
                        </header>
                        <div class=\"panel-body\">


                            <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" >

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "titre", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Theme</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Type:  </label>
                                    <div class=\"col-md-6\">
                                      

                                        ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "type", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>
                    </div>


                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Taches</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "tache", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>  

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Montant total demandé</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "montant", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Don</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "don", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Pret</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "pret", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Récompense</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "recompense", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Financement participatif en capital</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "investissement", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>    


                                

                                 <!-- pour completer le travail de bouton submit  -->
                                <input type=\"submit\" value=\"Ajouter\" />

                            </form>
                        </div>

                    </section>

                </div>
            </div>
        </div>

                ";
        // line 148
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig", 148)->display($context);
        echo "                                                         
                     ";
        // line 149
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig", 149)->display($context);
        echo "  
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 149,  204 => 148,  181 => 128,  170 => 120,  159 => 112,  148 => 104,  137 => 96,  126 => 88,  115 => 80,  103 => 71,  90 => 61,  77 => 51,  30 => 7,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}    */
/* */
/*         */
/*         <section class="page-top">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <ul class="breadcrumb">*/
/*                             <li><a href="#">Home</a></li>*/
/*                             <li class="active">Probleme</li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h1>Formulaire du projet</h1>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <div class="container">*/
/* */
/* */
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <section class="panel">*/
/*                         <header class="panel-heading">*/
/*                             <div class="panel-actions">*/
/*                                 <a href="#" class="fa fa-caret-down"></a>*/
/*                                 <a href="#" class="fa fa-times"></a>*/
/*                             </div>*/
/* */
/*                             <h2 class="panel-title">Veuillez saisir les champs</h2>*/
/*                         </header>*/
/*                         <div class="panel-body">*/
/* */
/* */
/*                             <form method="POST" class="form-horizontal form-bordered form-bordered" >*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.titre,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Theme</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Type:  </label>*/
/*                                     <div class="col-md-6">*/
/*                                       */
/* */
/*                                         {{ form_widget(f.type,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/*                     </div>*/
/* */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Taches</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.tache,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>  */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Montant total demandé</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.montant,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Don</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.don,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Pret</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.pret,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Récompense</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.recompense,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Financement participatif en capital</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.investissement,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>    */
/* */
/* */
/*                                 */
/* */
/*                                  <!-- pour completer le travail de bouton submit  -->*/
/*                                 <input type="submit" value="Ajouter" />*/
/* */
/*                             </form>*/
/*                         </div>*/
/* */
/*                     </section>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*                 {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}                                                         */
/*                      {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}  */
/*     </body>*/
/* </html>*/
