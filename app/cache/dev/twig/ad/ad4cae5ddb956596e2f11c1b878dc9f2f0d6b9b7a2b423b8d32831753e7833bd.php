<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProjet.html.twig */
class __TwigTemplate_fa5619ff5f4d22c96aa27efcecf041554835091fa3c4448470a569ba25221143 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>

        <!-- Basic -->
        <meta charset=\"utf-8\">
        <title>CrowdRise Tunisie</title>\t\t
        <meta name=\"keywords\" content=\"HTML5 Template\" />
        <meta name=\"description\" content=\"Porto - Responsive HTML5 Template\">
        <meta name=\"author\" content=\"okler.net\">

        <!-- Mobile Metas -->
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

        <!-- Web Fonts  -->
        <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light
\" rel=\"stylesheet\" type=\"text/css\">

        <!-- Vendor CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/bootstrap/bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/fontawesome/css/font-awesome.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/owlcarousel/owl.carousel.min.css"), "html", null, true);
        echo "\" media=\"screen\">
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/owlcarousel/owl.theme.default.min.css"), "html", null, true);
        echo "\" media=\"screen\">
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/magnific-popup/magnific-popup.css"), "html", null, true);
        echo "\" media=\"screen\">

        <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/bootstrap/bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/fontawesome/css/font-awesome.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/owlcarousel/owl.carousel.min.css"), "html", null, true);
        echo "\" media=\"screen\">
        <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/owlcarousel/owl.theme.default.min.css"), "html", null, true);
        echo "\" media=\"screen\">
        <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/magnific-popup/magnific-popup.css"), "html", null, true);
        echo "\" media=\"screen\">


        <!-- Theme CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/theme.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/theme-elements.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/theme-blog.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/theme-shop.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/theme-animate.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/theme.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/theme-elements.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/theme-blog.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/theme-shop.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/theme-animate.css"), "html", null, true);
        echo "\">

        <!-- Current Page CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/rs-plugin/css/settings.css"), "html", null, true);
        echo "\" media=\"screen\">
        <link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/circle-flip-slideshow/css/component.css"), "html", null, true);
        echo "\" media=\"screen\">

        <!-- Skin CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/skins/default.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/skins/default.css"), "html", null, true);
        echo "\">

        <!-- Theme Custom CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/css/custom.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/custom.css"), "html", null, true);
        echo "\">

        <!-- Head Libs -->
        <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/vendor/modernizr/modernizr.js"), "html", null, true);
        echo "\"></script>

        <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/modernizr/modernizr.js"), "html", null, true);
        echo "\"></script>

        <!--[if IE]>
                <link rel=\"stylesheet\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/ie.css"), "html", null, true);
        echo "\">
                <link rel=\"stylesheet\" href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/ie.css"), "html", null, true);
        echo "\">

                
        <![endif]-->

        <!--[if lte IE 8]>
                <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/respond/respond.js"), "html", null, true);
        echo "\"></script>
                <script src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/excanvas/excanvas.js"), "html", null, true);
        echo "\"></script>

                <script src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/respond/respond.js"), "html", null, true);
        echo "\"></script>
                <script src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("vendor/excanvas/excanvas.js"), "html", null, true);
        echo "\"></script>
        <![endif]-->

    </head>
    <body>



        <header id=\"header\">
            <div class=\"container\">
                <div class=\"logo\">
                    <a href=\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("crowd_rise_frontOffice");
        echo "\">
                        <img alt=\"Porto\" width=\"111\" height=\"54\" data-sticky-width=\"82\" data-sticky-height=\"40\" src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/logo.png"), "html", null, true);
        echo "\">
                    </a>
                </div>
                <div class=\"search\">
                    <form id=\"searchForm\" action=\"page-search-results.html\" method=\"get\">
                        <div class=\"input-group\">
                            <input type=\"text\" class=\"form-control search\" name=\"q\" id=\"q\" placeholder=\"Search...\" required>
                            <span class=\"input-group-btn\">
                                <button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <ul class=\"social-icons\">
                    <li class=\"facebook\"><a href=\"http://www.facebook.com/\" target=\"_blank\" title=\"Facebook\">Facebook</a></li>
                    <li class=\"twitter\"><a href=\"http://www.twitter.com/
\" target=\"_blank\" title=\"Twitter\">Twitter</a></li>
                    <li class=\"linkedin\"><a href=\"http://www.linkedin.com/
\" target=\"_blank\" title=\"Linkedin\">Linkedin</a></li>
                </ul>
                <nav>
                    <ul class=\"nav nav-pills nav-top\">
                        <li>
                            <a href=\"about-us.html\"><i class=\"fa fa-angle-right\"></i>About Us</a>
                        </li>
                        <li>
                            <a href=\"contact-us.html\"><i class=\"fa fa-angle-right\"></i>Contact Us</a>
                        </li>
                        <li class=\"phone\">
                            <span><i class=\"fa fa-phone\"></i>(92) 707-030</span>
                        </li>
                    </ul>
                </nav>
                <button class=\"btn btn-responsive-nav btn-inverse\" data-toggle=\"collapse\" data-target=\".nav-main-collapse\">
                    <i class=\"fa fa-bars\"></i>
                </button>
            </div>
            <div class=\"navbar-collapse nav-main-collapse collapse\">
                <div class=\"container\">
                    <nav class=\"nav-main mega-menu\">
                        <ul class=\"nav nav-pills nav-main\" id=\"mainMenu\">
                            <li >
                                <a  href=\"";
        // line 130
        echo $this->env->getExtension('routing')->getPath("crowd_rise_frontOffice");
        echo "\" >Home</a>
                                <ul class=\"dropdown-menu\">

                                </ul>
                            </li>
                            <li class=\"dropdown active\">
                                <a class=\"dropdown-toggle\" href=\"\">
                                    Publier
                                    <i class=\"fa fa-angle-down\"></i>
                                </a>
                                <ul class=\"dropdown-menu\">

                                    <li><a href=\"";
        // line 142
        echo $this->env->getExtension('routing')->getPath("formulaireProjet");
        echo "\">Un Projet <span class=\"tip\">réalise le maintenant!</span></a></li>
                                    <li><a href=\"";
        // line 143
        echo $this->env->getExtension('routing')->getPath("formulaireProbleme");
        echo "\">Un Problème</a></li>
                                    <li><a href=\"index-3.html\">Chercher des solvors experts</a></li>

                                </ul>
                            </li>
                            <li >
                                <a href=\"#\"> About Us</a>

                            </li>





                            <li >
                                <a  href=\"#\">
                                    Contact Us

                                </a>

                            </li>
                            <li class=\"dropdown mega-menu-item mega-menu-signin signin\" id=\"headerAccount\">
                                <a class=\"dropdown-toggle\" href=\"page-login.html\">
                                    <i class=\"fa fa-user\"></i> Sign In
                                    <i class=\"fa fa-angle-down\"></i>
                                </a>
                                <ul class=\"dropdown-menu\">
                                    <li>
                                        <div class=\"mega-menu-content\">
                                            <div class=\"row\">
                                                <div class=\"col-md-12\">

                                                    <div class=\"signin-form\">

                                                        <span class=\"mega-menu-sub-title\">Sign In</span>

                                                        <form action=\"\" id=\"\" method=\"post\">
                                                            <div class=\"row\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-md-12\">
                                                                        <label>Username or E-mail Address</label>
                                                                        <input type=\"text\" value=\"\" class=\"form-control input-lg\">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-md-12\">
                                                                        <a class=\"pull-right\" id=\"headerRecover\" href=\"#\">(Lost Password?)</a>
                                                                        <label>Password</label>
                                                                        <input type=\"password\" value=\"\" class=\"form-control input-lg\">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"col-md-6\">
                                                                    <span class=\"remember-box checkbox\">
                                                                        <label for=\"rememberme\">
                                                                            <input type=\"checkbox\" id=\"rememberme\" name=\"rememberme\">Remember Me
                                                                        </label>
                                                                    </span>
                                                                </div>
                                                                <div class=\"col-md-6\">
                                                                    <input type=\"submit\" value=\"Login\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\">
                                                                </div>
                                                            </div>
                                                        </form>

                                                        <p class=\"sign-up-info\">Don't have an account yet? <a href=\"#\" id=\"headerSignUp\">Sign Up</a></p>

                                                    </div>

                                                    <div class=\"signup-form\">
                                                        <span class=\"mega-menu-sub-title\">Create Account</span>

                                                        <form action=\"\" id=\"\" method=\"post\">
                                                            <div class=\"row\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-md-12\">
                                                                        <label>E-mail Address</label>
                                                                        <input type=\"text\" value=\"\" class=\"form-control input-lg\">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-md-6\">
                                                                        <label>Password</label>
                                                                        <input type=\"password\" value=\"\" class=\"form-control input-lg\">
                                                                    </div>
                                                                    <div class=\"col-md-6\">
                                                                        <label>Re-enter Password</label>
                                                                        <input type=\"password\" value=\"\" class=\"form-control input-lg\">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"col-md-12\">
                                                                    <input type=\"submit\" value=\"Create Account\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\">
                                                                </div>
                                                            </div>
                                                        </form>
                                                        
                                <p class=\"log-in-info\">Already have an account? <a href=\"#\" id=\"headerSignIn\">Log In</a></p>
                                                    </div>

                                                    <div class=\"recover-form\">
                                                        <span class=\"mega-menu-sub-title\">Reset My Password</span>
                                                        <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>

                                                        <form action=\"\" id=\"\" method=\"post\">
                                                            <div class=\"row\">
                                                                <div class=\"form-group\">
                                                                    <div class=\"col-md-12\">
                                                                        <label>E-mail Address</label>
                                                                        <input type=\"text\" value=\"\" class=\"form-control input-lg\">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"col-md-12\">
                                                                    <input type=\"submit\" value=\"Submit\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\">
                                                                </div>
                                                            </div>
                                                        </form>

                                                        <p class=\"log-in-info\">Already have an account? <a href=\"#\" id=\"headerRecoverCancel\">Log In</a></p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <section class=\"page-top\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <ul class=\"breadcrumb\">
                            <li><a href=\"#\">Home</a></li>
                            <li class=\"active\">Probleme</li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h1>Formulaire du projet</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class=\"container\">


            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <section class=\"panel\">
                        <header class=\"panel-heading\">
                            <div class=\"panel-actions\">
                                <a href=\"#\" class=\"fa fa-caret-down\"></a>
                                <a href=\"#\" class=\"fa fa-times\"></a>
                            </div>

                            <h2 class=\"panel-title\">Veuillez saisir les champs</h2>
                        </header>
                        <div class=\"panel-body\">


                            <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" >

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 324
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "titre", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Theme</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 334
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">

                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">  Type:  </label>
                                    <div class=\"col-md-6\">
                                      

                                        ";
        // line 344
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "type", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "

                                    </div>



                                </div>


                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 356
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Taches</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 364
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "tache", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>  

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Montant total demandé</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 372
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "montant", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Don</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 380
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "don", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Pret</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 388
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "pret", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Récompense</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 396
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "recompense", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div> 

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Financement participatif en capital</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 404
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "investissement", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>

                                </div>    





                                ";
        // line 413
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                                <input type=\"submit\" value=\"Modifier\" />

                            </form>
                        </div>

                    </section>

                </div>
            </div>
        </div>

        <footer id=\"footer\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"footer-ribbon\">
                        <span>Get in Touch</span>
                    </div>
                    <div class=\"col-md-3\">
                        <div class=\"newsletter\">
                            <h4>Newsletter</h4>
                            <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

                            <div class=\"alert alert-success hidden\" id=\"newsletterSuccess\">
                                <strong>Success!</strong> You've been added to our email list.
                            </div>

                            <div class=\"alert alert-danger hidden\" id=\"newsletterError\"></div>

                            <form id=\"newsletterForm\" action=\"php/newsletter-subscribe.php\" method=\"POST\">
                                <div class=\"input-group\">
                                    <input class=\"form-control\" placeholder=\"Email Address\" name=\"newsletterEmail\" id=\"newsletterEmail\" type=\"text\">
                                    <span class=\"input-group-btn\">
                                        <button class=\"btn btn-default\" type=\"submit\">Go!</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=\"col-md-3\">
                        <h4>Latest Tweets</h4>
                        <div id=\"tweet\" class=\"twitter\" data-plugin-tweets data-plugin-options='{\"username\": \"\", \"count\": 2}'>
                            <p>Please wait...</p>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"contact-details\">
                            <h4>Contact Us</h4>
                            <ul class=\"contact\">
                                <li><p><i class=\"fa fa-map-marker\"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
                                <li><p><i class=\"fa fa-phone\"></i> <strong>Phone:</strong> (123) 456-7890</p></li>
                                <li><p><i class=\"fa fa-envelope\"></i> <strong>Email:</strong> <a href=\"mailto:mail@example.com\">mail@example.com</a></p></li>
                            </ul>
                        </div>
                    </div>
                    <div class=\"col-md-2\">
                        <h4>Follow Us</h4>
                        <div class=\"social-icons\">
                            <ul class=\"social-icons\">
                                <li class=\"facebook\"><a href=\"https://www.facebook.com/groups/186467038374829/\" target=\"_blank\" data-placement=\"bottom\" data-tooltip title=\"Facebook\">Facebook</a></li>
                                <li class=\"twitter\"><a href=\"http://www.twitter.com/
\" target=\"_blank\" data-placement=\"bottom\" data-tooltip title=\"Twitter\">Twitter</a></li>
                                <li class=\"linkedin\"><a href=\"http://www.linkedin.com/
\" target=\"_blank\" data-placement=\"bottom\" data-tooltip title=\"Linkedin\">Linkedin</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>



            <div class=\"footer-copyright\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-1\">
                            <a href=\"index.html\" class=\"logo\">
                                <img alt=\"Porto Website Template\" class=\"img-responsive\" src=\"img/logo-footer.png\">
                            </a>
                        </div>
                        <div class=\"col-md-7\">
                            <p>© Copyright 2015. All Rights Reserved.</p>
                        </div>
                        <div class=\"col-md-4\">
                            <nav id=\"sub-menu\">
                                <ul>
                                    <li><a href=\"page-faq.html\">FAQ's</a></li>
                                    <li><a href=\"sitemap.html\">Sitemap</a></li>
                                    <li><a href=\"contact-us.html\">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProjet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  580 => 413,  568 => 404,  557 => 396,  546 => 388,  535 => 380,  524 => 372,  513 => 364,  502 => 356,  487 => 344,  474 => 334,  461 => 324,  277 => 143,  273 => 142,  258 => 130,  213 => 88,  209 => 87,  195 => 76,  191 => 75,  186 => 73,  182 => 72,  173 => 66,  169 => 65,  163 => 62,  158 => 60,  152 => 57,  147 => 55,  141 => 52,  136 => 50,  130 => 47,  126 => 46,  120 => 43,  116 => 42,  112 => 41,  108 => 40,  104 => 39,  99 => 37,  95 => 36,  91 => 35,  87 => 34,  83 => 33,  76 => 29,  72 => 28,  68 => 27,  64 => 26,  60 => 25,  55 => 23,  51 => 22,  47 => 21,  43 => 20,  39 => 19,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/* */
/*         <!-- Basic -->*/
/*         <meta charset="utf-8">*/
/*         <title>CrowdRise Tunisie</title>		*/
/*         <meta name="keywords" content="HTML5 Template" />*/
/*         <meta name="description" content="Porto - Responsive HTML5 Template">*/
/*         <meta name="author" content="okler.net">*/
/* */
/*         <!-- Mobile Metas -->*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*         <!-- Web Fonts  -->*/
/*         <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light*/
/* " rel="stylesheet" type="text/css">*/
/* */
/*         <!-- Vendor CSS -->*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/bootstrap/bootstrap.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/fontawesome/css/font-awesome.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/owlcarousel/owl.carousel.min.css')}}" media="screen">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/owlcarousel/owl.theme.default.min.css')}}" media="screen">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/magnific-popup/magnific-popup.css')}}" media="screen">*/
/* */
/*         <link rel="stylesheet" href="{{asset('vendor/bootstrap/bootstrap.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('vendor/fontawesome/css/font-awesome.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('vendor/owlcarousel/owl.carousel.min.css')}}" media="screen">*/
/*         <link rel="stylesheet" href="{{asset('vendor/owlcarousel/owl.theme.default.min.css')}}" media="screen">*/
/*         <link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" media="screen">*/
/* */
/* */
/*         <!-- Theme CSS -->*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/theme.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/theme-elements.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/theme-blog.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/theme-shop.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/theme-animate.css')}}">*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/theme.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/theme-elements.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/theme-blog.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/theme-shop.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/theme-animate.css')}}">*/
/* */
/*         <!-- Current Page CSS -->*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/rs-plugin/css/settings.css')}}" media="screen">*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/vendor/circle-flip-slideshow/css/component.css')}}" media="screen">*/
/* */
/*         <!-- Skin CSS -->*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/skins/default.css')}}">*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/skins/default.css')}}">*/
/* */
/*         <!-- Theme Custom CSS -->*/
/*         <link rel="stylesheet" href="{{asset('FontOffice/css/custom.css')}}">*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/custom.css')}}">*/
/* */
/*         <!-- Head Libs -->*/
/*         <script src="{{asset('FontOffice/vendor/modernizr/modernizr.js')}}"></script>*/
/* */
/*         <script src="{{asset('vendor/modernizr/modernizr.js')}}"></script>*/
/* */
/*         <!--[if IE]>*/
/*                 <link rel="stylesheet" href="{{asset('css/ie.css')}}">*/
/*                 <link rel="stylesheet" href="{{asset('css/ie.css')}}">*/
/* */
/*                 */
/*         <![endif]-->*/
/* */
/*         <!--[if lte IE 8]>*/
/*                 <script src="{{asset('vendor/respond/respond.js')}}"></script>*/
/*                 <script src="{{asset('vendor/excanvas/excanvas.js')}}"></script>*/
/* */
/*                 <script src="{{asset('vendor/respond/respond.js')}}"></script>*/
/*                 <script src="{{asset('vendor/excanvas/excanvas.js')}}"></script>*/
/*         <![endif]-->*/
/* */
/*     </head>*/
/*     <body>*/
/* */
/* */
/* */
/*         <header id="header">*/
/*             <div class="container">*/
/*                 <div class="logo">*/
/*                     <a href="{{ path('crowd_rise_frontOffice') }}">*/
/*                         <img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="{{asset('FontOffice/img/logo.png')}}">*/
/*                     </a>*/
/*                 </div>*/
/*                 <div class="search">*/
/*                     <form id="searchForm" action="page-search-results.html" method="get">*/
/*                         <div class="input-group">*/
/*                             <input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>*/
/*                             <span class="input-group-btn">*/
/*                                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>*/
/*                             </span>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*                 <ul class="social-icons">*/
/*                     <li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>*/
/*                     <li class="twitter"><a href="http://www.twitter.com/*/
/* " target="_blank" title="Twitter">Twitter</a></li>*/
/*                     <li class="linkedin"><a href="http://www.linkedin.com/*/
/* " target="_blank" title="Linkedin">Linkedin</a></li>*/
/*                 </ul>*/
/*                 <nav>*/
/*                     <ul class="nav nav-pills nav-top">*/
/*                         <li>*/
/*                             <a href="about-us.html"><i class="fa fa-angle-right"></i>About Us</a>*/
/*                         </li>*/
/*                         <li>*/
/*                             <a href="contact-us.html"><i class="fa fa-angle-right"></i>Contact Us</a>*/
/*                         </li>*/
/*                         <li class="phone">*/
/*                             <span><i class="fa fa-phone"></i>(92) 707-030</span>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </nav>*/
/*                 <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">*/
/*                     <i class="fa fa-bars"></i>*/
/*                 </button>*/
/*             </div>*/
/*             <div class="navbar-collapse nav-main-collapse collapse">*/
/*                 <div class="container">*/
/*                     <nav class="nav-main mega-menu">*/
/*                         <ul class="nav nav-pills nav-main" id="mainMenu">*/
/*                             <li >*/
/*                                 <a  href="{{ path('crowd_rise_frontOffice') }}" >Home</a>*/
/*                                 <ul class="dropdown-menu">*/
/* */
/*                                 </ul>*/
/*                             </li>*/
/*                             <li class="dropdown active">*/
/*                                 <a class="dropdown-toggle" href="">*/
/*                                     Publier*/
/*                                     <i class="fa fa-angle-down"></i>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu">*/
/* */
/*                                     <li><a href="{{ path('formulaireProjet') }}">Un Projet <span class="tip">réalise le maintenant!</span></a></li>*/
/*                                     <li><a href="{{ path('formulaireProbleme') }}">Un Problème</a></li>*/
/*                                     <li><a href="index-3.html">Chercher des solvors experts</a></li>*/
/* */
/*                                 </ul>*/
/*                             </li>*/
/*                             <li >*/
/*                                 <a href="#"> About Us</a>*/
/* */
/*                             </li>*/
/* */
/* */
/* */
/* */
/* */
/*                             <li >*/
/*                                 <a  href="#">*/
/*                                     Contact Us*/
/* */
/*                                 </a>*/
/* */
/*                             </li>*/
/*                             <li class="dropdown mega-menu-item mega-menu-signin signin" id="headerAccount">*/
/*                                 <a class="dropdown-toggle" href="page-login.html">*/
/*                                     <i class="fa fa-user"></i> Sign In*/
/*                                     <i class="fa fa-angle-down"></i>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     <li>*/
/*                                         <div class="mega-menu-content">*/
/*                                             <div class="row">*/
/*                                                 <div class="col-md-12">*/
/* */
/*                                                     <div class="signin-form">*/
/* */
/*                                                         <span class="mega-menu-sub-title">Sign In</span>*/
/* */
/*                                                         <form action="" id="" method="post">*/
/*                                                             <div class="row">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-md-12">*/
/*                                                                         <label>Username or E-mail Address</label>*/
/*                                                                         <input type="text" value="" class="form-control input-lg">*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                             <div class="row">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-md-12">*/
/*                                                                         <a class="pull-right" id="headerRecover" href="#">(Lost Password?)</a>*/
/*                                                                         <label>Password</label>*/
/*                                                                         <input type="password" value="" class="form-control input-lg">*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                             <div class="row">*/
/*                                                                 <div class="col-md-6">*/
/*                                                                     <span class="remember-box checkbox">*/
/*                                                                         <label for="rememberme">*/
/*                                                                             <input type="checkbox" id="rememberme" name="rememberme">Remember Me*/
/*                                                                         </label>*/
/*                                                                     </span>*/
/*                                                                 </div>*/
/*                                                                 <div class="col-md-6">*/
/*                                                                     <input type="submit" value="Login" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                         </form>*/
/* */
/*                                                         <p class="sign-up-info">Don't have an account yet? <a href="#" id="headerSignUp">Sign Up</a></p>*/
/* */
/*                                                     </div>*/
/* */
/*                                                     <div class="signup-form">*/
/*                                                         <span class="mega-menu-sub-title">Create Account</span>*/
/* */
/*                                                         <form action="" id="" method="post">*/
/*                                                             <div class="row">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-md-12">*/
/*                                                                         <label>E-mail Address</label>*/
/*                                                                         <input type="text" value="" class="form-control input-lg">*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                             <div class="row">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-md-6">*/
/*                                                                         <label>Password</label>*/
/*                                                                         <input type="password" value="" class="form-control input-lg">*/
/*                                                                     </div>*/
/*                                                                     <div class="col-md-6">*/
/*                                                                         <label>Re-enter Password</label>*/
/*                                                                         <input type="password" value="" class="form-control input-lg">*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                             <div class="row">*/
/*                                                                 <div class="col-md-12">*/
/*                                                                     <input type="submit" value="Create Account" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                         </form>*/
/*                                                         */
/*                                 <p class="log-in-info">Already have an account? <a href="#" id="headerSignIn">Log In</a></p>*/
/*                                                     </div>*/
/* */
/*                                                     <div class="recover-form">*/
/*                                                         <span class="mega-menu-sub-title">Reset My Password</span>*/
/*                                                         <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>*/
/* */
/*                                                         <form action="" id="" method="post">*/
/*                                                             <div class="row">*/
/*                                                                 <div class="form-group">*/
/*                                                                     <div class="col-md-12">*/
/*                                                                         <label>E-mail Address</label>*/
/*                                                                         <input type="text" value="" class="form-control input-lg">*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                             <div class="row">*/
/*                                                                 <div class="col-md-12">*/
/*                                                                     <input type="submit" value="Submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">*/
/*                                                                 </div>*/
/*                                                             </div>*/
/*                                                         </form>*/
/* */
/*                                                         <p class="log-in-info">Already have an account? <a href="#" id="headerRecoverCancel">Log In</a></p>*/
/*                                                     </div>*/
/* */
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </nav>*/
/*                 </div>*/
/*             </div>*/
/*         </header>*/
/*         <section class="page-top">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <ul class="breadcrumb">*/
/*                             <li><a href="#">Home</a></li>*/
/*                             <li class="active">Probleme</li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h1>Formulaire du projet</h1>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <div class="container">*/
/* */
/* */
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <section class="panel">*/
/*                         <header class="panel-heading">*/
/*                             <div class="panel-actions">*/
/*                                 <a href="#" class="fa fa-caret-down"></a>*/
/*                                 <a href="#" class="fa fa-times"></a>*/
/*                             </div>*/
/* */
/*                             <h2 class="panel-title">Veuillez saisir les champs</h2>*/
/*                         </header>*/
/*                         <div class="panel-body">*/
/* */
/* */
/*                             <form method="POST" class="form-horizontal form-bordered form-bordered" >*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.titre,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Theme</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/* */
/*                                     <label class="col-md-3 control-label" for="textareaDefault">  Type:  </label>*/
/*                                     <div class="col-md-6">*/
/*                                       */
/* */
/*                                         {{ form_widget(f.type,{"attr": {'class': "form-control"}} )}}*/
/* */
/*                                     </div>*/
/* */
/* */
/* */
/*                                 </div>*/
/* */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Taches</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.tache,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>  */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Montant total demandé</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.montant,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Don</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.don,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Pret</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.pret,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Récompense</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.recompense,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div> */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Financement participatif en capital</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.investissement,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/* */
/*                                 </div>    */
/* */
/* */
/* */
/* */
/* */
/*                                 {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                                 <input type="submit" value="Modifier" />*/
/* */
/*                             </form>*/
/*                         </div>*/
/* */
/*                     </section>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <footer id="footer">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="footer-ribbon">*/
/*                         <span>Get in Touch</span>*/
/*                     </div>*/
/*                     <div class="col-md-3">*/
/*                         <div class="newsletter">*/
/*                             <h4>Newsletter</h4>*/
/*                             <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>*/
/* */
/*                             <div class="alert alert-success hidden" id="newsletterSuccess">*/
/*                                 <strong>Success!</strong> You've been added to our email list.*/
/*                             </div>*/
/* */
/*                             <div class="alert alert-danger hidden" id="newsletterError"></div>*/
/* */
/*                             <form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">*/
/*                                 <div class="input-group">*/
/*                                     <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">*/
/*                                     <span class="input-group-btn">*/
/*                                         <button class="btn btn-default" type="submit">Go!</button>*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </form>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-3">*/
/*                         <h4>Latest Tweets</h4>*/
/*                         <div id="tweet" class="twitter" data-plugin-tweets data-plugin-options='{"username": "", "count": 2}'>*/
/*                             <p>Please wait...</p>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="contact-details">*/
/*                             <h4>Contact Us</h4>*/
/*                             <ul class="contact">*/
/*                                 <li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>*/
/*                                 <li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</p></li>*/
/*                                 <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>*/
/*                             </ul>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-2">*/
/*                         <h4>Follow Us</h4>*/
/*                         <div class="social-icons">*/
/*                             <ul class="social-icons">*/
/*                                 <li class="facebook"><a href="https://www.facebook.com/groups/186467038374829/" target="_blank" data-placement="bottom" data-tooltip title="Facebook">Facebook</a></li>*/
/*                                 <li class="twitter"><a href="http://www.twitter.com/*/
/* " target="_blank" data-placement="bottom" data-tooltip title="Twitter">Twitter</a></li>*/
/*                                 <li class="linkedin"><a href="http://www.linkedin.com/*/
/* " target="_blank" data-placement="bottom" data-tooltip title="Linkedin">Linkedin</a></li>*/
/*                             </ul>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/* */
/* */
/*             <div class="footer-copyright">*/
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="col-md-1">*/
/*                             <a href="index.html" class="logo">*/
/*                                 <img alt="Porto Website Template" class="img-responsive" src="img/logo-footer.png">*/
/*                             </a>*/
/*                         </div>*/
/*                         <div class="col-md-7">*/
/*                             <p>© Copyright 2015. All Rights Reserved.</p>*/
/*                         </div>*/
/*                         <div class="col-md-4">*/
/*                             <nav id="sub-menu">*/
/*                                 <ul>*/
/*                                     <li><a href="page-faq.html">FAQ's</a></li>*/
/*                                     <li><a href="sitemap.html">Sitemap</a></li>*/
/*                                     <li><a href="contact-us.html">Contact</a></li>*/
/*                                 </ul>*/
/*                             </nav>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </footer>*/
/*     </body>*/
/* </html>*/
