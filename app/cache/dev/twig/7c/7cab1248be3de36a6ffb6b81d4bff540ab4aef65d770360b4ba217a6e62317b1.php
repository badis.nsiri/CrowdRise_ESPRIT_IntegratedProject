<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig */
class __TwigTemplate_e5db59ee2096945ade93dd1a37c72957776a3c8b33d067fc3154487b8e9076e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig", 3)->display($context);
        // line 4
        echo "    </head>
    <body>


        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig", 8)->display($context);
        echo "   

        <section class=\"page-top\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <ul class=\"breadcrumb\">
                            <li><a href=\"#\">Home</a></li>
                            <li class=\"active\">Evenement</li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h1>Formulaire de l'evenement</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class=\"container\">


            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <section class=\"panel\">
                        <header class=\"panel-heading\">
                            <div class=\"panel-actions\">
                                <a href=\"#\" class=\"fa fa-caret-down\"></a>
                                <a href=\"#\" class=\"fa fa-times\"></a>
                            </div>

                            <h2 class=\"panel-title\">Veuillez saisir les champs</h2>
                        </header>
                        <div class=\"panel-body\">


                            <form method=\"POST\" class=\"form-horizontal form-bordered form-bordered\" ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'enctype');
        echo " >

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Titre:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "nom", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Thème:</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "theme", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Description:</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>  
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Lieu:  </label>
                                    <div class=\"col-md-6\">

                                        ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "lieu", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>    

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Date de debut :</label>
                                    <div class=\"col-md-6\">
                                        ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "dateDebut", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Date de fin :</label>
                                    <div class=\"col-md-6\">
                                        ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "dateFin", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Heure de debut :</label>
                                    <div class=\"col-md-6\">
                                        ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "heureDebut", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"inputDefault\">Heure de fin :</label>
                                    <div class=\"col-md-6\">
                                        ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "heureFin", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <label class=\"col-md-3 control-label\" for=\"textareaDefault\">Image</label> 
                                    <div class=\"col-md-6\">
                                        ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "file", array()), 'widget');
        echo "    
                                    </div>  
                                </div>   

                                    ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), "_token", array()), 'widget');
        echo "

                                ";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["f"]) ? $context["f"] : $this->getContext($context, "f")), 'rest');
        echo " <!-- pour completer le travail de bouton submit  -->
                                <input type=\"submit\" value=\"Ajouter\" />








                            </form>
                        </div>

                    </section>

                </div>
            </div>
        </div>

        ";
        // line 132
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig", 132)->display($context);
        // line 133
        echo "
        ";
        // line 134
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig", 134)->display($context);
        echo "    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 134,  196 => 133,  194 => 132,  172 => 113,  167 => 111,  160 => 107,  150 => 100,  140 => 93,  130 => 86,  120 => 79,  110 => 72,  99 => 64,  89 => 57,  79 => 50,  70 => 44,  31 => 8,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}   */
/* */
/*         <section class="page-top">*/
/*             <div class="container">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <ul class="breadcrumb">*/
/*                             <li><a href="#">Home</a></li>*/
/*                             <li class="active">Evenement</li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h1>Formulaire de l'evenement</h1>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <div class="container">*/
/* */
/* */
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <section class="panel">*/
/*                         <header class="panel-heading">*/
/*                             <div class="panel-actions">*/
/*                                 <a href="#" class="fa fa-caret-down"></a>*/
/*                                 <a href="#" class="fa fa-times"></a>*/
/*                             </div>*/
/* */
/*                             <h2 class="panel-title">Veuillez saisir les champs</h2>*/
/*                         </header>*/
/*                         <div class="panel-body">*/
/* */
/* */
/*                             <form method="POST" class="form-horizontal form-bordered form-bordered" {{ form_enctype(f) }} >*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Titre:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.nom,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Thème:</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.theme,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Description:</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.description,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>  */
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Lieu:  </label>*/
/*                                     <div class="col-md-6">*/
/* */
/*                                         {{ form_widget(f.lieu,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>    */
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="inputDefault">Date de debut :</label>*/
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.dateDebut,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="inputDefault">Date de fin :</label>*/
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.dateFin,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="inputDefault">Heure de debut :</label>*/
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.heureDebut,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="inputDefault">Heure de fin :</label>*/
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.heureFin,{"attr": {'class': "form-control"}} )}}*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                                 <div class="form-group">*/
/*                                     <label class="col-md-3 control-label" for="textareaDefault">Image</label> */
/*                                     <div class="col-md-6">*/
/*                                         {{ form_widget(f.file)}}    */
/*                                     </div>  */
/*                                 </div>   */
/* */
/*                                     {{ form_widget(f._token) }}*/
/* */
/*                                 {{ form_rest(f)}} <!-- pour completer le travail de bouton submit  -->*/
/*                                 <input type="submit" value="Ajouter" />*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                             </form>*/
/*                         </div>*/
/* */
/*                     </section>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/* */
/*         {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*     </body>*/
/* </html>*/
