<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:test.html.twig */
class __TwigTemplate_e2eaf0dc954ec3b8486c12f983c8ba6cea1306325ffed6614cb1a3358ffb37cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " 
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo " <br>
";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["iduser"]) ? $context["iduser"] : $this->getContext($context, "iduser")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
/*  */
/* {{ id }} <br>*/
/* {{ iduser }}*/
