<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig */
class __TwigTemplate_7e8a15bebfa2abe8779507f2620a9e4d88042a7600a3ecf5f71774dc1bf05738 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        ";
        // line 8
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig", 8)->display($context);
        echo " 
        <div class=\"body\">


            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Problème</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Problème</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <div class=\"row\">

                    <ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">


                        <div role=\"main\" class=\"main shop\">

                            <div class=\"container\">



                                <div class=\"row\">
                                    <div class=\"col-md-12\">

                                        <div class=\"row featured-boxes\">
                                            <div class=\"col-md-12\">
                                                <div class=\"featured-box featured-box-secundary featured-box-cart\">
                                                    <div class=\"box-content\">
                                                        <form method=\"post\" action=\"\">
                                                            <table cellspacing=\"0\" class=\"shop_table cart\">
                                                                <thead>
                                                                    <tr>
                                                                        <th class=\"product-remove\">
                                                                            &nbsp;
                                                                        </th>

                                                                        <th class=\"product-name\">
                                                                            Problème
                                                                        </th>
                                                                        <th class=\"product-price\">
                                                                            Thême
                                                                        </th>
                                                                        <th class=\"product-quantity\">

                                                                        </th>
                                                                        <th class=\"product-subtotal\">
                                                                            Ajouté par
                                                                        </th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                    ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["problemes"]) ? $context["problemes"] : $this->getContext($context, "problemes")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "  

                                                                        <tr class=\"cart_table_item\">
                                                                            <td class=\"product-remove\">
                                                                                <span class=\"onsale\">";
            // line 79
            if (($this->getAttribute($context["i"], "etat", array()) == "Resolu")) {
                echo "                                        
            

                                                                                        <ul class=\"list list-skills icons list-unstyled list-inline\">
                                                                                            <li><i class=\"fa fa-check-circle\"> </i>    </li> <br>

                                                                                        </ul>

                                                                                   
                                                                                    </div>";
            }
            // line 88
            echo "</span>

                                                                            </td>

                                                                            <td class=\"product-name\">
                                                                                <a href=\"";
            // line 93
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("probleme", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "titre", array()), "html", null, true);
            echo "</a>
                                                                            </td>
                                                                            <td class=\"product-price\">
                                                                                <span class=\"amount\">";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "theme", array()), "html", null, true);
            echo "</span>
                                                                            </td>
                                                                            <td class=\"product-quantity\">

                                                                            </td>
                                                                            <td class=\"product-subtotal\">
                                                                                <span class=\"amount\">";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "user", array()), "username", array()), "html", null, true);
            echo "</span>
                                                                            </td>
                                                                        </tr>


                                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "
                                                                </tbody>
                                                            </table>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        </ul>

























                                    </div>


                                    <section class=\"call-to-action featured footer\">
                                        <div class=\"container\">
                                            <div class=\"row\">
                                                <div class=\"center\">
                                                    <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href=\"http://themeforest.net/item/porto-responsive-html5-template/4106987\" target=\"_blank\" class=\"btn btn-lg btn-primary\" data-appear-animation=\"bounceIn\">Buy Now!</a> <span class=\"arrow hlb hidden-xs hidden-sm hidden-md\" data-appear-animation=\"rotateInUpLeft\" style=\"top: -22px;\"></span></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    ";
        // line 157
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig", 157)->display($context);
        // line 158
        echo "                                </div>
                                ";
        // line 159
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig", 159)->display($context);
        echo "    
                                </body>
                                </html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 159,  211 => 158,  209 => 157,  158 => 108,  146 => 102,  137 => 96,  129 => 93,  122 => 88,  109 => 79,  100 => 75,  30 => 8,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %} */
/*         <div class="body">*/
/* */
/* */
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Problème</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Problème</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/*                 <div class="row">*/
/* */
/*                     <ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/* */
/* */
/*                         <div role="main" class="main shop">*/
/* */
/*                             <div class="container">*/
/* */
/* */
/* */
/*                                 <div class="row">*/
/*                                     <div class="col-md-12">*/
/* */
/*                                         <div class="row featured-boxes">*/
/*                                             <div class="col-md-12">*/
/*                                                 <div class="featured-box featured-box-secundary featured-box-cart">*/
/*                                                     <div class="box-content">*/
/*                                                         <form method="post" action="">*/
/*                                                             <table cellspacing="0" class="shop_table cart">*/
/*                                                                 <thead>*/
/*                                                                     <tr>*/
/*                                                                         <th class="product-remove">*/
/*                                                                             &nbsp;*/
/*                                                                         </th>*/
/* */
/*                                                                         <th class="product-name">*/
/*                                                                             Problème*/
/*                                                                         </th>*/
/*                                                                         <th class="product-price">*/
/*                                                                             Thême*/
/*                                                                         </th>*/
/*                                                                         <th class="product-quantity">*/
/* */
/*                                                                         </th>*/
/*                                                                         <th class="product-subtotal">*/
/*                                                                             Ajouté par*/
/*                                                                         </th>*/
/*                                                                         */
/*                                                                     </tr>*/
/*                                                                 </thead>*/
/*                                                                 <tbody>*/
/* */
/*                                                                     {% for i in problemes %}  */
/* */
/*                                                                         <tr class="cart_table_item">*/
/*                                                                             <td class="product-remove">*/
/*                                                                                 <span class="onsale">{% if i.etat=='Resolu' %}                                        */
/*             */
/* */
/*                                                                                         <ul class="list list-skills icons list-unstyled list-inline">*/
/*                                                                                             <li><i class="fa fa-check-circle"> </i>    </li> <br>*/
/* */
/*                                                                                         </ul>*/
/* */
/*                                                                                    */
/*                                                                                     </div>{% endif %}</span>*/
/* */
/*                                                                             </td>*/
/* */
/*                                                                             <td class="product-name">*/
/*                                                                                 <a href="{{ path('probleme',{'id':i.id})}}">{{i.titre}}</a>*/
/*                                                                             </td>*/
/*                                                                             <td class="product-price">*/
/*                                                                                 <span class="amount">{{i.theme}}</span>*/
/*                                                                             </td>*/
/*                                                                             <td class="product-quantity">*/
/* */
/*                                                                             </td>*/
/*                                                                             <td class="product-subtotal">*/
/*                                                                                 <span class="amount">{{i.user.username}}</span>*/
/*                                                                             </td>*/
/*                                                                         </tr>*/
/* */
/* */
/*                                                                     {% endfor %}*/
/* */
/*                                                                 </tbody>*/
/*                                                             </table>*/
/*                                                         </form>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/*                                         </ul>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     </div>*/
/* */
/* */
/*                                     <section class="call-to-action featured footer">*/
/*                                         <div class="container">*/
/*                                             <div class="row">*/
/*                                                 <div class="center">*/
/*                                                     <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website! <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">Buy Now!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </section>*/
/* */
/*                                     {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*                                 </div>*/
/*                                 {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}    */
/*                                 </body>*/
/*                                 </html>*/
