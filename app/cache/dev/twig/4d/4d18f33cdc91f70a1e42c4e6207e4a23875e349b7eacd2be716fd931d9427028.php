<?php

/* CrowdRiseBundle:Includes:Index/indexHeader.html.twig */
class __TwigTemplate_ce438279b5342f7e6656fca57cf27453c1985e96c2ff250352bc470b90861414 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'erreur_login' => array($this, 'block_erreur_login'),
            'fosUserGen' => array($this, 'block_fosUserGen'),
            'fos_user_login' => array($this, 'block_fos_user_login'),
            'fos_user_inscrit' => array($this, 'block_fos_user_inscrit'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"header\">
                <div class=\"container\">
                    <div class=\"logo\">
                        <a href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("crowd_rise_frontOffice");
        echo "\">
                            <img alt=\"Porto\" width=\"111\" height=\"54\" data-sticky-width=\"82\" data-sticky-height=\"40\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("FontOffice/img/logo.png"), "html", null, true);
        echo "\">
                        </a>
                    </div>
                    <div class=\"search\">
                        <form id=\"searchForm\" action=\"page-search-results.html\" method=\"get\">
                            <div class=\"input-group\">
                                <input type=\"text\" class=\"form-control search\" name=\"q\" id=\"q\" placeholder=\"Search...\" required>
                                <span class=\"input-group-btn\">
                                    <button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <ul class=\"social-icons\">
                        <li class=\"facebook\"><a href=\"http://www.facebook.com/\" target=\"_blank\" title=\"Facebook\">Facebook</a></li>
                        <li class=\"twitter\"><a href=\"http://www.twitter.com/\" target=\"_blank\" title=\"Twitter\">Twitter</a></li>
                        <li class=\"linkedin\"><a href=\"http://www.linkedin.com/\" target=\"_blank\" title=\"Linkedin\">Linkedin</a></li>
                    </ul>
                    <nav>
                        <ul class=\"nav nav-pills nav-top\">
                            <li>
                                <a href=\"about-us.html\"><i class=\"fa fa-angle-right\"></i>About Us</a>
                            </li>
                            <li>
                                <a href=\"contact-us.html\"><i class=\"fa fa-angle-right\"></i>Contact Us</a>
                            </li>
                            <li class=\"phone\">
                                <span><i class=\"fa fa-phone\"></i>(92) 707-030</span>
                            </li>
                        </ul>
                    </nav>
                    <button class=\"btn btn-responsive-nav btn-inverse\" data-toggle=\"collapse\" data-target=\".nav-main-collapse\">
                        <i class=\"fa fa-bars\"></i>
                    </button>
                </div>
                <div class=\"navbar-collapse nav-main-collapse collapse\">
                    <div class=\"container\">
                        <nav class=\"nav-main mega-menu\">
                            <ul class=\"nav nav-pills nav-main\" id=\"mainMenu\">
                                <li >
                                    <a  href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("crowd_rise_frontOffice");
        echo "\" >Home</a>
                                    <ul class=\"dropdown-menu\">

                                    </ul>
                                </li>
                                <li class=\"dropdown active\">
                                    <a class=\"dropdown-toggle\" href=\"\">
                                        Publier
                                        <i class=\"fa fa-angle-down\"></i>
                                    </a>
                                    <ul class=\"dropdown-menu\">

                                        <li><a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("formulaireProjet");
        echo "\">Un Projet <span class=\"tip\">réalise le maintenant!</span></a></li>
                                        <li><a href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("formulaireProbleme");
        echo "\">Un Problème</a></li>
                                        <li><a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("formulaireIdee");
        echo "\">Une Idée</a></li>
                                        <li><a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("EvenementFormulaire");
        echo "\">Un évenement</a></li>
                                        <li><a href=\"index-3.html\">Chercher des solvors experts</a></li>

                                    </ul>
                                </li>
                                <li >
                                    <a href=\"#\"> About Us</a>

                                </li>





                                <li >
                                    <a  href=\"#\">
                                        Contact Us

                                    </a>

                                </li>
                                ";
        // line 81
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 82
            echo "
                                    <li class=\"dropdown mega-menu-item mega-menu-signin signin logged\" id=\"headerAccount\">



                                        <a class=\"dropdown-toggle disabled\" href=\"#\">

                                            <i class=\"fa fa-user\"></i> ";
            // line 89
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("%username%", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " 
                                            <i class=\"fa fa-angle-down\"></i>

                                            ";
            // line 93
            echo "


                                        </a>

                                        <ul class=\"dropdown-menu\">
                                            <li>
                                                <div class=\"mega-menu-content\">
                                                    <div class=\"row\">
                                                        <div class=\"col-md-8\">
                                                            <div class=\"user-avatar\">
                                                                <div class=\"img-thumbnail\">
                                                                    ";
            // line 105
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) != null)) {
                // line 106
                echo "                                                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("image_user", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
                echo "\" alt=\"\">
                                                                    ";
            }
            // line 108
            echo "                                                                </div>
                                                                <p><strong> ";
            // line 109
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("%username%", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "  </strong><span>CrowdRise Tunisie ^^</span></p>
                                                            </div>
                                                        </div>
                                                        <div class=\"col-md-9\">
                                                            <ul class=\"list-account-options\">


                                                                <li><a href=\"";
            // line 116
            echo $this->env->getExtension('routing')->getPath("changePassword");
            echo " \">modifier votre de passe</a></li>
                                                                <li><a href=\"";
            // line 117
            echo $this->env->getExtension('routing')->getPath("editProfil");
            echo "\">Modifier votre profile</a></li>
                                                                <li><a href=\"";
            // line 118
            echo $this->env->getExtension('routing')->getPath("showProfil");
            echo "\">Consulter votre profile</a></li>
                                                                <li><a href=\"";
            // line 119
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("deleteMyCompte", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\">Supprimer votre compte</a></li>
                                                                <li>
                                                                    <a  class=\" profile  showopacity\" href=\"";
            // line 121
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">
                                                                        ";
            // line 122
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Déconnexion", array(), "FOSUserBundle"), "html", null, true);
            echo " 

                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>









                                    </li>
                                ";
        } else {
            // line 143
            echo "                                    <li class=\"dropdown mega-menu-item mega-menu-signin signin\" id=\"headerAccount\">
                                        <a class=\"dropdown-toggle\" href=\"";
            // line 144
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">
                                            ";
            // line 145
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("connexion", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                        </a>
                                        <ul class=\"dropdown-menu\">
                                            <li>
                                                <div class=\"mega-menu-content\">
                                                    <div class=\"row\">
                                                        <div class=\"col-md-12\">

                                                            <div class=\"signin-form\">

                                                                <span class=\"mega-menu-sub-title\">Sign In      ";
            // line 155
            $this->displayBlock('erreur_login', $context, $blocks);
            // line 157
            echo "      </span>
                                                                        ";
            // line 158
            $this->displayBlock('fosUserGen', $context, $blocks);
            // line 165
            echo "
                                                                    <p class=\"sign-up-info\">Vous n'avez pas encore un compte? <a href=\"#\" id=\"headerSignUp\">Inscription</a></p>

                                                                </div>

                                                                <div class=\"signup-form\">
                                                                    <span class=\"mega-menu-sub-title\">Créer votre compte</span>

                                                                    ";
            // line 173
            $this->displayBlock('fos_user_inscrit', $context, $blocks);
            // line 176
            echo "
                                                                    <p class=\"log-in-info\">Vous avez déjà un compte?<a href=\"#\" id=\"headerSignIn\">connexion</a></p>
                                                                </div>

                                                                <div class=\"recover-form\">
                                                                    <span class=\"mega-menu-sub-title\">Reset My Password</span>
                                                                    <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>

                                                                    <form action=\"\" id=\"\" method=\"post\">
                                                                        <div class=\"row\">
                                                                            <div class=\"form-group\">
                                                                                <div class=\"col-md-12\">
                                                                                    <label>E-mail Address</label>
                                                                                    <input type=\"text\" value=\"\" class=\"form-control input-lg\">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-12\">
                                                                                <input type=\"submit\" value=\"Submit\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\">
                                                                            </div>
                                                                        </div>
                                                                    </form>

                                                                    <p class=\"log-in-info\">Already have an account? <a href=\"#\" id=\"headerRecoverCancel\">Log In</a></p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </li>
                                        ";
        }
        // line 210
        echo " 
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </header>
";
    }

    // line 155
    public function block_erreur_login($context, array $blocks = array())
    {
        echo "  

                                                                    ";
    }

    // line 158
    public function block_fosUserGen($context, array $blocks = array())
    {
        // line 159
        echo "                                                                            ";
        $this->displayBlock('fos_user_login', $context, $blocks);
        // line 164
        echo "                                                                    ";
    }

    // line 159
    public function block_fos_user_login($context, array $blocks = array())
    {
        // line 160
        echo "
                                                                                ";
        // line 161
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CrowdRiseBundle:CrowdRiseLogin:login"));
        echo "

                                                                        ";
    }

    // line 173
    public function block_fos_user_inscrit($context, array $blocks = array())
    {
        // line 174
        echo "                                                                        ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FOSUserBundle:Registration:register"));
        echo ".
                                                                    ";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Includes:Index/indexHeader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 174,  340 => 173,  333 => 161,  330 => 160,  327 => 159,  323 => 164,  320 => 159,  317 => 158,  309 => 155,  299 => 210,  262 => 176,  260 => 173,  250 => 165,  248 => 158,  245 => 157,  243 => 155,  230 => 145,  226 => 144,  223 => 143,  199 => 122,  195 => 121,  190 => 119,  186 => 118,  182 => 117,  178 => 116,  168 => 109,  165 => 108,  159 => 106,  157 => 105,  143 => 93,  137 => 89,  128 => 82,  126 => 81,  102 => 60,  98 => 59,  94 => 58,  90 => 57,  75 => 45,  32 => 5,  28 => 4,  23 => 1,);
    }
}
/* <header id="header">*/
/*                 <div class="container">*/
/*                     <div class="logo">*/
/*                         <a href="{{ path('crowd_rise_frontOffice') }}">*/
/*                             <img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="{{asset('FontOffice/img/logo.png')}}">*/
/*                         </a>*/
/*                     </div>*/
/*                     <div class="search">*/
/*                         <form id="searchForm" action="page-search-results.html" method="get">*/
/*                             <div class="input-group">*/
/*                                 <input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>*/
/*                                 <span class="input-group-btn">*/
/*                                     <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>*/
/*                                 </span>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <ul class="social-icons">*/
/*                         <li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>*/
/*                         <li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>*/
/*                         <li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin">Linkedin</a></li>*/
/*                     </ul>*/
/*                     <nav>*/
/*                         <ul class="nav nav-pills nav-top">*/
/*                             <li>*/
/*                                 <a href="about-us.html"><i class="fa fa-angle-right"></i>About Us</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="contact-us.html"><i class="fa fa-angle-right"></i>Contact Us</a>*/
/*                             </li>*/
/*                             <li class="phone">*/
/*                                 <span><i class="fa fa-phone"></i>(92) 707-030</span>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </nav>*/
/*                     <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">*/
/*                         <i class="fa fa-bars"></i>*/
/*                     </button>*/
/*                 </div>*/
/*                 <div class="navbar-collapse nav-main-collapse collapse">*/
/*                     <div class="container">*/
/*                         <nav class="nav-main mega-menu">*/
/*                             <ul class="nav nav-pills nav-main" id="mainMenu">*/
/*                                 <li >*/
/*                                     <a  href="{{ path('crowd_rise_frontOffice') }}" >Home</a>*/
/*                                     <ul class="dropdown-menu">*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li class="dropdown active">*/
/*                                     <a class="dropdown-toggle" href="">*/
/*                                         Publier*/
/*                                         <i class="fa fa-angle-down"></i>*/
/*                                     </a>*/
/*                                     <ul class="dropdown-menu">*/
/* */
/*                                         <li><a href="{{ path('formulaireProjet') }}">Un Projet <span class="tip">réalise le maintenant!</span></a></li>*/
/*                                         <li><a href="{{ path('formulaireProbleme') }}">Un Problème</a></li>*/
/*                                         <li><a href="{{ path('formulaireIdee') }}">Une Idée</a></li>*/
/*                                         <li><a href="{{ path('EvenementFormulaire') }}">Un évenement</a></li>*/
/*                                         <li><a href="index-3.html">Chercher des solvors experts</a></li>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li >*/
/*                                     <a href="#"> About Us</a>*/
/* */
/*                                 </li>*/
/* */
/* */
/* */
/* */
/* */
/*                                 <li >*/
/*                                     <a  href="#">*/
/*                                         Contact Us*/
/* */
/*                                     </a>*/
/* */
/*                                 </li>*/
/*                                 {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/* */
/*                                     <li class="dropdown mega-menu-item mega-menu-signin signin logged" id="headerAccount">*/
/* */
/* */
/* */
/*                                         <a class="dropdown-toggle disabled" href="#">*/
/* */
/*                                             <i class="fa fa-user"></i> {{ '%username%'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} */
/*                                             <i class="fa fa-angle-down"></i>*/
/* */
/*                                             {# <img class="img-thumbnail img-thumbnail-small "  src="{{asset('FontOffice/img/imgProfil.png')}}" />  #}*/
/* */
/* */
/* */
/*                                         </a>*/
/* */
/*                                         <ul class="dropdown-menu">*/
/*                                             <li>*/
/*                                                 <div class="mega-menu-content">*/
/*                                                     <div class="row">*/
/*                                                         <div class="col-md-8">*/
/*                                                             <div class="user-avatar">*/
/*                                                                 <div class="img-thumbnail">*/
/*                                                                     {% if app.user != null %}*/
/*                                                                     <img src="{{path('image_user',{'id':app.user.id})}}" alt="">*/
/*                                                                     {% endif %}*/
/*                                                                 </div>*/
/*                                                                 <p><strong> {{ '%username%'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}  </strong><span>CrowdRise Tunisie ^^</span></p>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                         <div class="col-md-9">*/
/*                                                             <ul class="list-account-options">*/
/* */
/* */
/*                                                                 <li><a href="{{path('changePassword')}} ">modifier votre de passe</a></li>*/
/*                                                                 <li><a href="{{path('editProfil')}}">Modifier votre profile</a></li>*/
/*                                                                 <li><a href="{{path('showProfil')}}">Consulter votre profile</a></li>*/
/*                                                                 <li><a href="{{path('deleteMyCompte',{'id':app.user.id})}}">Supprimer votre compte</a></li>*/
/*                                                                 <li>*/
/*                                                                     <a  class=" profile  showopacity" href="{{ path('fos_user_security_logout') }}">*/
/*                                                                         {{ 'Déconnexion'|trans({}, 'FOSUserBundle') }} */
/* */
/*                                                                     </a>*/
/*                                                                 </li>*/
/*                                                             </ul>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </li>*/
/*                                         </ul>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                     </li>*/
/*                                 {% else %}*/
/*                                     <li class="dropdown mega-menu-item mega-menu-signin signin" id="headerAccount">*/
/*                                         <a class="dropdown-toggle" href="{{ path('fos_user_security_login') }}">*/
/*                                             {{ 'connexion'|trans({}, 'FOSUserBundle') }}*/
/*                                         </a>*/
/*                                         <ul class="dropdown-menu">*/
/*                                             <li>*/
/*                                                 <div class="mega-menu-content">*/
/*                                                     <div class="row">*/
/*                                                         <div class="col-md-12">*/
/* */
/*                                                             <div class="signin-form">*/
/* */
/*                                                                 <span class="mega-menu-sub-title">Sign In      {% block erreur_login %}  */
/* */
/*                                                                     {% endblock  %}      </span>*/
/*                                                                         {% block fosUserGen %}*/
/*                                                                             {% block fos_user_login %}*/
/* */
/*                                                                                 {{ render(controller('CrowdRiseBundle:CrowdRiseLogin:login')) }}*/
/* */
/*                                                                         {% endblock %}*/
/*                                                                     {% endblock %}*/
/* */
/*                                                                     <p class="sign-up-info">Vous n'avez pas encore un compte? <a href="#" id="headerSignUp">Inscription</a></p>*/
/* */
/*                                                                 </div>*/
/* */
/*                                                                 <div class="signup-form">*/
/*                                                                     <span class="mega-menu-sub-title">Créer votre compte</span>*/
/* */
/*                                                                     {% block fos_user_inscrit %}*/
/*                                                                         {{ render(controller('FOSUserBundle:Registration:register')) }}.*/
/*                                                                     {% endblock %}*/
/* */
/*                                                                     <p class="log-in-info">Vous avez déjà un compte?<a href="#" id="headerSignIn">connexion</a></p>*/
/*                                                                 </div>*/
/* */
/*                                                                 <div class="recover-form">*/
/*                                                                     <span class="mega-menu-sub-title">Reset My Password</span>*/
/*                                                                     <p>Complete the form below to receive an email with the authorization code needed to reset your password.</p>*/
/* */
/*                                                                     <form action="" id="" method="post">*/
/*                                                                         <div class="row">*/
/*                                                                             <div class="form-group">*/
/*                                                                                 <div class="col-md-12">*/
/*                                                                                     <label>E-mail Address</label>*/
/*                                                                                     <input type="text" value="" class="form-control input-lg">*/
/*                                                                                 </div>*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/*                                                                         <div class="row">*/
/*                                                                             <div class="col-md-12">*/
/*                                                                                 <input type="submit" value="Submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading...">*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/*                                                                     </form>*/
/* */
/*                                                                     <p class="log-in-info">Already have an account? <a href="#" id="headerRecoverCancel">Log In</a></p>*/
/*                                                                 </div>*/
/* */
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                             </ul>*/
/* */
/*                                         </li>*/
/*                                         {% endif %} */
/*                                         </ul>*/
/*                                     </nav>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </header>*/
/* */
