<?php

/* CrowdRiseBundle:Security:loginAcceuil.html.twig */
class __TwigTemplate_9dbce9fca9f9bf1f37032f430c346fb49e8a4380eaaf90424505be7083d2e95b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'erreur_login' => array($this, 'block_erreur_login'),
            'fos_user_login' => array($this, 'block_fos_user_login'),
            'UserNameOrEmail' => array($this, 'block_UserNameOrEmail'),
            'Password' => array($this, 'block_Password'),
            'RememberMe' => array($this, 'block_RememberMe'),
            'loginButton' => array($this, 'block_loginButton'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 14
        $this->displayBlock('erreur_login', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('fos_user_login', $context, $blocks);
    }

    // line 14
    public function block_erreur_login($context, array $blocks = array())
    {
        echo "  
   ";
        // line 16
        echo "    
";
    }

    // line 19
    public function block_fos_user_login($context, array $blocks = array())
    {
        // line 20
        echo "

    <form action=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
        ";
        // line 24
        $this->displayBlock('UserNameOrEmail', $context, $blocks);
        // line 34
        echo "        ";
        $this->displayBlock('Password', $context, $blocks);
        // line 46
        echo "

        ";
        // line 48
        $this->displayBlock('RememberMe', $context, $blocks);
        // line 63
        echo "    </form>
";
    }

    // line 24
    public function block_UserNameOrEmail($context, array $blocks = array())
    {
        // line 25
        echo "            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        <label for=\"username\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adresse e-mail ou Nom d'utilisateur", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                        <input type=\"text\" class=\"form-control input-lg\" id=\"username\" name=\"_username\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
                    </div>
                </div>
            </div>
        ";
    }

    // line 34
    public function block_Password($context, array $blocks = array())
    {
        // line 35
        echo "            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        <a class=\"pull-right\" id=\"headerRecover\" href=\"#\">(Lost Password?)</a>
                        <label for=\"password\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe :", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                        <input type=\"password\" class=\"form-control input-lg\" id=\"password\" name=\"_password\" required=\"required\" />
                    </div>
                </div>
            </div>

        ";
    }

    // line 48
    public function block_RememberMe($context, array $blocks = array())
    {
        // line 49
        echo "            <div class=\"row\">
                <div class=\"col-md-6\">
                    <span class=\"remember-box checkbox\">
                        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                        <label for=\"remember_me\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Garder ma session active", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                    </span>
                </div>
                ";
        // line 56
        $this->displayBlock('loginButton', $context, $blocks);
        // line 61
        echo "            </div>   
        ";
    }

    // line 56
    public function block_loginButton($context, array $blocks = array())
    {
        // line 57
        echo "                    <div class=\"col-md-6\">
                        <input type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-primary pull-right push-bottom\" data-loading-text=\"Loading...\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("connexion", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                    </div>
                ";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:Security:loginAcceuil.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  144 => 58,  141 => 57,  138 => 56,  133 => 61,  131 => 56,  125 => 53,  119 => 49,  116 => 48,  105 => 39,  99 => 35,  96 => 34,  87 => 29,  83 => 28,  78 => 25,  75 => 24,  70 => 63,  68 => 48,  64 => 46,  61 => 34,  59 => 24,  55 => 23,  51 => 22,  47 => 20,  44 => 19,  39 => 16,  34 => 14,  30 => 19,  27 => 18,  25 => 14,);
    }
}
/* {#*/
/* {% if error %}*/
/*     {% block erreur_login %}  */
/*         {% include "CrowdRiseBundle:Security:loginAcceuil.html.twig" %}*/
/*             errr*/
/*     {% endblock  %}*/
/*    */
/* */
/*  */
/* */
/* */
/* {% else %}*/
/* #}*/
/* {% block erreur_login %}  */
/*    {# {% include "CrowdRiseBundle:Security:loginAcceuil.html.twig" %} #}*/
/*     */
/* {% endblock  %}*/
/* */
/* {% block fos_user_login %}*/
/* */
/* */
/*     <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*         <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*         {% block UserNameOrEmail %}*/
/*             <div class="row">*/
/*                 <div class="form-group">*/
/*                     <div class="col-md-12">*/
/*                         <label for="username">{{ "Adresse e-mail ou Nom d'utilisateur"|trans({}, 'FOSUserBundle') }}</label>*/
/*                         <input type="text" class="form-control input-lg" id="username" name="_username" value="{{ last_username }}" required="required" />*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         {% endblock %}*/
/*         {% block Password %}*/
/*             <div class="row">*/
/*                 <div class="form-group">*/
/*                     <div class="col-md-12">*/
/*                         <a class="pull-right" id="headerRecover" href="#">(Lost Password?)</a>*/
/*                         <label for="password">{{ 'Mot de passe :'|trans({}, 'FOSUserBundle') }}</label>*/
/*                         <input type="password" class="form-control input-lg" id="password" name="_password" required="required" />*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         {% endblock %}*/
/* */
/* */
/*         {% block RememberMe %}*/
/*             <div class="row">*/
/*                 <div class="col-md-6">*/
/*                     <span class="remember-box checkbox">*/
/*                         <input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/*                         <label for="remember_me">{{ 'Garder ma session active'|trans({}, 'FOSUserBundle') }}</label>*/
/*                     </span>*/
/*                 </div>*/
/*                 {% block loginButton %}*/
/*                     <div class="col-md-6">*/
/*                         <input type="submit" id="_submit" name="_submit" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading..." value="{{ 'connexion'|trans({}, 'FOSUserBundle') }}" />*/
/*                     </div>*/
/*                 {% endblock %}*/
/*             </div>   */
/*         {% endblock %}*/
/*     </form>*/
/* {% endblock %}*/
/* */
