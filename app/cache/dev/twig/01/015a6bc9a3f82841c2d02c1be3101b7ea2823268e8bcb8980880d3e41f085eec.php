<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:indexProfile.html.twig */
class __TwigTemplate_0bbb05a8514df6e4b13954e8bb774d102dcf9932a864005343fc1f73e35e1e08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProfile.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>

        <div class=\"body\">

            ";
        // line 11
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexHeader.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProfile.html.twig", 11)->display($context);
        // line 12
        echo "            <div role=\"main\" class=\"main\">

                <section class=\"page-top\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <ul class=\"breadcrumb\">
                                    <li><a href=\"#\">Home</a></li>
                                    <li class=\"active\">Features</li>
                                </ul>
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                <h1>Forms Basic</h1>
                            </div>
                        </div>
                    </div>
                </section>


                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <section class=\"panel\">
                                <header class=\"panel-heading\">


                                    <h2 class=\"panel-title\">Form Elements</h2>
                                </header>
                                <div class=\"panel-body\">
                                    <form class=\"form-horizontal form-bordered\" method=\"get\">
                                        
                                       
                       <div class=\"form-group\">
                                            <label class=\"control-label col-md-3\">Mon Profile</label>
                                            <div class=\"col-md-6\">
                                                <section class=\"form-group-vertical\">
                                                    <div class=\"input-group input-group-icon\">
                                                        <span class=\"input-group-addon\">
                                                            <span class=\"icon\"><i class=\"fa fa-user\"></i></span>
                                                        </span>
                                                        <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "\" disabled>
                                                    </div>
                                                    <div class=\"input-group input-group-icon\">
                                                        <span class=\"input-group-addon\">
                                                            <span class=\"icon\"><i class=\"fa fa-envelope\"></i></span>
                                                        </span>
                                                        <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "email", array()), "html", null, true);
        echo "\" disabled>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>



                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\">File Upload</label>
                                            <div class=\"col-md-6\">
                                                <div class=\"fileupload fileupload-new\" data-provides=\"fileupload\">
                                                    <div class=\"input-append\">
                                                        <div class=\"uneditable-input\">
                                                            <i class=\"fa fa-file fileupload-exists\"></i>
                                                            <span class=\"fileupload-preview\"></span>
                                                        </div>
                                                        <span class=\"btn btn-default btn-file\">
                                                            <span class=\"fileupload-exists\">Change</span>
                                                            <span class=\"fileupload-new\">Select file</span>
                                                            <input type=\"file\" />
                                                        </span>
                                                        <a href=\"#\" class=\"btn btn-default fileupload-exists\" data-dismiss=\"fileupload\">Remove</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                 


















                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\">Nom & Prenom :</label>
                                            <div class=\"col-md-6\">
                                                <div class=\"input-group mb-md\">
                                                    <span class=\"input-group-addon\">
                                                        <i class=\"fa fa-user\"></i>
                                                    </span>
                                                    <input type=\"text\" class=\"form-control\" placeholder=\"Nom\">
                                                </div>
                                                <div class=\"input-group mb-md\">
                                                    <span class=\"input-group-addon\">
                                                        <i class=\"fa fa-user\"></i>
                                                    </span>
                                                    <input type=\"text\" class=\"form-control\" placeholder=\"Prenom\">
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\">Sexe :</label>
                                            <div class=\"col-md-6\">
                                                 

                                                <div class=\"input-group mb-md\">
                                                    <span class=\"input-group-addon\">
                                                        <input type=\"radio\">
                                                    </span>
                                                    <input type=\"text\" class=\"form-control\" disabled value=\"Homme\">
                                                </div>
                                                <div class=\"input-group mb-md\">
                                                    <span class=\"input-group-addon\">
                                                        <input type=\"radio\">
                                                    </span>
                                                    <input type=\"text\" class=\"form-control\" disabled value=\"Femme\">
                                                </div>

                                            </div>
                                        </div>



                                    </form>

                            </section>

                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-lg-12\">


                        </div>

                        ";
        // line 162
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexFooter.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProfile.html.twig", 162)->display($context);
        // line 163
        echo "                    </div>

                    ";
        // line 165
        $this->loadTemplate("CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig", "CrowdRiseBundle:CrowdRiseFrontOffice:indexProfile.html.twig", 165)->display($context);
        // line 166
        echo "                    </body>
                    </html>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:indexProfile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 166,  199 => 165,  195 => 163,  193 => 162,  88 => 60,  79 => 54,  35 => 12,  33 => 11,  26 => 6,  24 => 5,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         {% include 'CrowdRiseBundle:Includes:Index/indexHeadAssets.html.twig' %}*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div class="body">*/
/* */
/*             {% include 'CrowdRiseBundle:Includes:Index/indexHeader.html.twig' %}*/
/*             <div role="main" class="main">*/
/* */
/*                 <section class="page-top">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <ul class="breadcrumb">*/
/*                                     <li><a href="#">Home</a></li>*/
/*                                     <li class="active">Features</li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Forms Basic</h1>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/* */
/* */
/*                 <div class="container">*/
/*                     <div class="row">*/
/*                         <div class="col-lg-12">*/
/*                             <section class="panel">*/
/*                                 <header class="panel-heading">*/
/* */
/* */
/*                                     <h2 class="panel-title">Form Elements</h2>*/
/*                                 </header>*/
/*                                 <div class="panel-body">*/
/*                                     <form class="form-horizontal form-bordered" method="get">*/
/*                                         */
/*                                        */
/*                        <div class="form-group">*/
/*                                             <label class="control-label col-md-3">Mon Profile</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 <section class="form-group-vertical">*/
/*                                                     <div class="input-group input-group-icon">*/
/*                                                         <span class="input-group-addon">*/
/*                                                             <span class="icon"><i class="fa fa-user"></i></span>*/
/*                                                         </span>*/
/*                                                         <input class="form-control" type="text" placeholder="{{ app.user.username }}" disabled>*/
/*                                                     </div>*/
/*                                                     <div class="input-group input-group-icon">*/
/*                                                         <span class="input-group-addon">*/
/*                                                             <span class="icon"><i class="fa fa-envelope"></i></span>*/
/*                                                         </span>*/
/*                                                         <input class="form-control" type="text" placeholder="{{ app.user.email  }}" disabled>*/
/*                                                     </div>*/
/*                                                 </section>*/
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/* */
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label">File Upload</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 <div class="fileupload fileupload-new" data-provides="fileupload">*/
/*                                                     <div class="input-append">*/
/*                                                         <div class="uneditable-input">*/
/*                                                             <i class="fa fa-file fileupload-exists"></i>*/
/*                                                             <span class="fileupload-preview"></span>*/
/*                                                         </div>*/
/*                                                         <span class="btn btn-default btn-file">*/
/*                                                             <span class="fileupload-exists">Change</span>*/
/*                                                             <span class="fileupload-new">Select file</span>*/
/*                                                             <input type="file" />*/
/*                                                         </span>*/
/*                                                         <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/* */
/*                  */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label">Nom & Prenom :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                 <div class="input-group mb-md">*/
/*                                                     <span class="input-group-addon">*/
/*                                                         <i class="fa fa-user"></i>*/
/*                                                     </span>*/
/*                                                     <input type="text" class="form-control" placeholder="Nom">*/
/*                                                 </div>*/
/*                                                 <div class="input-group mb-md">*/
/*                                                     <span class="input-group-addon">*/
/*                                                         <i class="fa fa-user"></i>*/
/*                                                     </span>*/
/*                                                     <input type="text" class="form-control" placeholder="Prenom">*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="form-group">*/
/*                                             <label class="col-md-3 control-label">Sexe :</label>*/
/*                                             <div class="col-md-6">*/
/*                                                  */
/* */
/*                                                 <div class="input-group mb-md">*/
/*                                                     <span class="input-group-addon">*/
/*                                                         <input type="radio">*/
/*                                                     </span>*/
/*                                                     <input type="text" class="form-control" disabled value="Homme">*/
/*                                                 </div>*/
/*                                                 <div class="input-group mb-md">*/
/*                                                     <span class="input-group-addon">*/
/*                                                         <input type="radio">*/
/*                                                     </span>*/
/*                                                     <input type="text" class="form-control" disabled value="Femme">*/
/*                                                 </div>*/
/* */
/*                                             </div>*/
/*                                         </div>*/
/* */
/* */
/* */
/*                                     </form>*/
/* */
/*                             </section>*/
/* */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-lg-12">*/
/* */
/* */
/*                         </div>*/
/* */
/*                         {% include 'CrowdRiseBundle:Includes:Index/indexFooter.html.twig' %}*/
/*                     </div>*/
/* */
/*                     {% include 'CrowdRiseBundle:Includes:Index/indexBodyAssets.html.twig' %}*/
/*                     </body>*/
/*                     </html>*/
