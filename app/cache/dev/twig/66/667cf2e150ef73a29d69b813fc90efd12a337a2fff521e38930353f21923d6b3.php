<?php

/* CrowdRiseBundle:CrowdRiseFrontOffice:showAllIdee.html.twig */
class __TwigTemplate_c4ed5198c992ed6b35451b1dd9dea46aa84f9eef43ee4eb896cf3db0dcdc7851 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2><a href=\"";
        echo $this->env->getExtension('routing')->getPath("list_idee");
        echo "\">Idée</a></h2>
<div class=\"search\">
    <form id=\"searchForm\" action=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("SearchIdee");
        echo "\" method=\"POST\">
        <div class=\"input-group\">
            <input type=\"text\" class=\"form-control search\" name=\"q\" id=\"q\" placeholder=\"Search...\" required>
            <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
            </span>
        </div>
    </form>
</div>
<hr />

<div class=\"row\">

    <ul class=\"portfolio-list sort-destination\" data-sort-id=\"portfolio\">
        ";
        // line 17
        $context["c"] = 0;
        // line 18
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["idees"]) ? $context["idees"] : $this->getContext($context, "idees")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "  
                ";
            // line 19
            $context["c"] = ((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")) + 1);
            // line 20
            echo "                    ";
            if (((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")) <= 4)) {
                echo " 
                        <li class=\"col-md-3 col-sm-6 col-xs-12 isotope-item websites\">
                            <div class=\"portfolio-item img-thumbnail\">
                                <a href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("idee", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                echo "\" class=\"thumb-info\">
                                    <img alt=\"\" class=\"img-responsive\" src=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("image_idee", array("id" => $this->getAttribute($context["i"], "id", array()))), "html", null, true);
                echo "\">
                                    <span class=\"thumb-info-title\">
                                        <span class=\"thumb-info-inner\">";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "titre", array()), "html", null, true);
                echo "</span>
                                        <span class=\"thumb-info-type\">";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "user", array()), "username", array()), "html", null, true);
                echo "</span>
                                    </span>
                                    <span class=\"thumb-info-action\">
                                        <span title=\"Universal\" class=\"thumb-info-action-icon\"><i class=\"fa fa-link\"></i></span>
                                    </span>
                                </a>
                            </div>
                        </li>
                    ";
            }
            // line 36
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                    </ul>
                </div>";
    }

    public function getTemplateName()
    {
        return "CrowdRiseBundle:CrowdRiseFrontOffice:showAllIdee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 37,  85 => 36,  73 => 27,  69 => 26,  64 => 24,  60 => 23,  53 => 20,  51 => 19,  44 => 18,  42 => 17,  25 => 3,  19 => 1,);
    }
}
/* <h2><a href="{{path('list_idee')}}">Idée</a></h2>*/
/* <div class="search">*/
/*     <form id="searchForm" action="{{path('SearchIdee')}}" method="POST">*/
/*         <div class="input-group">*/
/*             <input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>*/
/*             <span class="input-group-btn">*/
/*                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>*/
/*             </span>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* <hr />*/
/* */
/* <div class="row">*/
/* */
/*     <ul class="portfolio-list sort-destination" data-sort-id="portfolio">*/
/*         {% set c=0   %}*/
/*             {% for i in idees %}  */
/*                 {% set c=c+1 %}*/
/*                     {% if c<=4 %} */
/*                         <li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">*/
/*                             <div class="portfolio-item img-thumbnail">*/
/*                                 <a href="{{ path('idee',{'id':i.id})}}" class="thumb-info">*/
/*                                     <img alt="" class="img-responsive" src="{{path('image_idee',{'id':i.id})}}">*/
/*                                     <span class="thumb-info-title">*/
/*                                         <span class="thumb-info-inner">{{i.titre}}</span>*/
/*                                         <span class="thumb-info-type">{{i.user.username}}</span>*/
/*                                     </span>*/
/*                                     <span class="thumb-info-action">*/
/*                                         <span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>*/
/*                                     </span>*/
/*                                 </a>*/
/*                             </div>*/
/*                         </li>*/
/*                     {%endif %}*/
/*                     {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
