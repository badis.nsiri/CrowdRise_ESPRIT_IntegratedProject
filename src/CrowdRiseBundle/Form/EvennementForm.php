<?php

namespace CrowdRiseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EvennementForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('description', 'textarea')
                ->add('theme')
                ->add('lieu')
                ->add('dateDebut', 'date')
                ->add('dateFin', 'date')
                ->add('heureDebut', 'time')
                ->add('heureFin', 'time')
                ->add('file');
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CrowdRiseBundle\Entity\Evennement'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'crowdrisebundle_evennement';
    }

}
