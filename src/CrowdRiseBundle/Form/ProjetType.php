<?php

namespace CrowdRiseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description', 'textarea')
            ->add('type', 'choice', array(
                    'choices' => array('personne' => 'Personne', 'charite' => 'Charite', 'startup' => 'startup'),
                    'preferred_choices' => array('personne'),
                ))
            ->add('theme')
            ->add('tache', 'textarea')
            ->add('montant')
            ->add('don')
            ->add('pret')
            ->add('recompense')
            ->add('investissement')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CrowdRiseBundle\Entity\Projet'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crowdrisebundle_projet';
    }
}
