<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PatientRepository
 *
 * @author Win7pro
 */
class EvennementRepository extends EntityRepository {

    public function findEvenementsByTheme($theme) {
        $qb = $this->createQuerybuilder('s');
        $qb->where("s.theme = :theme ")->setParameter('theme', $theme);

        return $qb->getQuery()->getResult();
    }
 
}