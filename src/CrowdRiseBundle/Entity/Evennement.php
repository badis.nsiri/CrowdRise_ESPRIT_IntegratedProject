<?php

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CrowdRiseBundle\Entity\EvennementRepository")
 */
class Evennement {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", nullable=false)
     */
    private $nom;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     */
    private $description;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", nullable=false)
     */
    private $lieu;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="themeEvent", type="string", nullable=false)
     */
    private $theme;

    /**
     *
     *  
     *
     * @ORM\Column(name="dateDebut", type="date", nullable=false)
     */
    protected $dateDebut;

    /**
     *
     *  
     *
     * @ORM\Column(name="dateFin", type="date", nullable=false)
     */
    protected $dateFin;

    /**
     *
     *  
     *
     * @ORM\Column(name="heureDebut", type="time", nullable=false)
     */
    protected $heureDebut;

    /**
     *
     *  
     *
     * @ORM\Column(name="heureFin", type="time", nullable=false)
     */
    protected $heureFin;

    /**
     * @ORM\Column(name="imageEventBlob", type="blob", nullable=false)
     */
    private $imageEventBlob;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getTheme() {
        return $this->theme;
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    public function getLieu() {
        return $this->lieu;
    }

    public function getDateDebut() {
        return $this->dateDebut;
    }

    public function getDateFin() {
        return $this->dateFin;
    }

    public function setLieu($lieu) {
        $this->lieu = $lieu;
    }

    public function setDateDebut($dateDebut) {
        $this->dateDebut = $dateDebut;
    }

    public function setDateFin($dateFin) {
        $this->dateFin = $dateFin;
    }

    public function getHeureDebut() {
        return $this->heureDebut;
    }

    public function getHeureFin() {
        return $this->heureFin;
    }

    public function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    public function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }
    
 

    function getFile() {
        return $this->file;
    }
    function getImageEventBlob() {
        return $this->imageEventBlob;
    }

    function setImageEventBlob($imageEventBlob) {
        $this->imageEventBlob = $imageEventBlob;
    }

     

    function setFile($file) {
        $this->file = $file;
    }

        
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="evennements")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="idee")
     */
    protected $images;

    public function __construct() {
        $this->images = new ArrayCollection();
    }

    public function getUser() {
        return $this->user;
    }

    public function getImages() {
        return $this->images;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setImages($images) {
        $this->images = $images;
    }

}
