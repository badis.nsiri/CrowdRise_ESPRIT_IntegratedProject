<?php

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\CommentBundle\Entity\Comment as BaseComment;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CrowdRiseBundle\Entity\IdeeRepository")
 */
class Idee {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", nullable=false)
     */
    private $titre;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     */
    private $description;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="theme", type="string", nullable=false)
     */
    private $theme;

    /**
     * @ORM\Column(name="imageIdeeBlob", type="blob", nullable=false)
     */
    private $imageIdeeBlob;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;

    public function getId() {
        return $this->id;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getTheme() {
        return $this->theme;
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="idees")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="idee")
     */
    protected $images;

    /**
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="idee")
     */
    protected $commentaires;

    public function __construct() {
        $this->images = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getUser() {
        return $this->user;
    }

    public function getImages() {
        return $this->images;
    }

    public function getCommentaires() {
        return $this->commentaires;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setImages($images) {
        $this->images = $images;
    }

    public function setCommentaires($commentaires) {
        $this->commentaires = $commentaires;
    }

    /**
     * Set imageIdeeBlob
     *
     * @param string $imageIdeeBlob
     * @return 
     */
    public function setImageIdeeBlob($imageIdeeBlob) {
        $this->imageIdeeBlob = $imageIdeeBlob;

        return $this;
    }

    /**
     * Get imageIdeeBlob
     *
     * @return string 
     */
    public function getImageIdeeBlob() {
        return $this->imageIdeeBlob;
    }

    function getFile() {
        return $this->file;
    }

    function setFile($file) {
        $this->file = $file;
    }

}
