<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transaction
 *
 * @author Badis
 */

namespace CrowdRiseBundle\Entity;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="transaction")
 */
// this entire entity is developped By Badis NSIRI
class Transaction {
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    
    /**
    * @ORM\Column(type="string", length=100)
    */
    protected $type;
    
    
    /**
    * @ORM\Column(type="decimal", scale=2)
    */
    protected $montant;

    public function getId() {
        return $this->id;
    }

    public function getType() {
        return $this->type;
    }

    public function getMontant() {
        return $this->montant;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setMontant($montant) {
        $this->montant = $montant;
    }

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transactions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    public function getUser() {
        return $this->user;
    }

    public function getProjet() {
        return $this->projet;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setProjet($projet) {
        $this->projet = $projet;
    }

        
     /**
     * @ORM\ManyToOne(targetEntity="Projet", inversedBy="transactions")
     * @ORM\JoinColumn(name="projet_id", referencedColumnName="id")
     */
    protected $projet;
}
