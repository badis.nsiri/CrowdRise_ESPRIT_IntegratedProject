<?php
namespace CrowdRiseBundle\Entity ;
use Doctrine\ORM\Mapping as ORM ; 
use Symfony\Component\Validator\Constraints as Assert;
/**

* 

*

* @ORM\Entity

*/

class Image

{

 /**

 * @var integer

 *

 * @ORM\Column(name="id", type="integer", nullable=false)

 * @ORM\Id

 * @ORM\GeneratedValue(strategy="IDENTITY")

 */

 private $id;

 /**

 * 

 * 

 * @ORM\Column(name="img", type="blob", nullable=false)

 */

 private $img;

 

 /**

 * @var string

 *

 * @ORM\Column(name="titre", type="string", nullable=false)

 */

 private $titre;

 

 /**

 * 

 * @Assert\File(maxSize="6000000")

 * 

 */
  private $file;
  
  
   /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="images")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
   
    
   /**
     * @ORM\ManyToOne(targetEntity="Probleme", inversedBy="images")
     * @ORM\JoinColumn(name="probleme_id", referencedColumnName="id")
     */
    protected $probleme;
   
     
   /**
     * @ORM\ManyToOne(targetEntity="Idee", inversedBy="images")
     * @ORM\JoinColumn(name="idee_id", referencedColumnName="id")
     */
    protected $idee;
    
    /**
     * @ORM\ManyToOne(targetEntity="Projet", inversedBy="images")
     * @ORM\JoinColumn(name="projet_id", referencedColumnName="id")
     */
    protected $projet;
    public function getId() {
        return $this->id;
    }

    public function getImg() {
        return $this->img;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getFile() {
        return $this->file;
    }

    public function getUser() {
        return $this->user;
    }

    public function getProbleme() {
        return $this->probleme;
    }

    public function getIdee() {
        return $this->idee;
    }

    public function getProjet() {
        return $this->projet;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setImg($img) {
        $this->img = $img;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setProbleme($probleme) {
        $this->probleme = $probleme;
    }

    public function setIdee($idee) {
        $this->idee = $idee;
    }

    public function setProjet($projet) {
        $this->projet = $projet;
    }


}