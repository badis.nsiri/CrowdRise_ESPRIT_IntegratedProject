<?php
namespace CrowdRiseBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
 


/**
 *
 *
 * @ORM\Entity
 */
class Probleme  {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", nullable=false)
     */
    private $titre;

    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     */
      private $description;
  
    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", nullable=false)
     */
    private $theme; 
      
        /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", nullable=false)
     */
    private $etat="Non Resolu";  
    
    
      public function getId() {
          return $this->id;
      }

      public function getTitre() {
          return $this->titre;
      }

      public function getDescription() {
          return $this->description;
      }

      public function setId($id) {
          $this->id = $id;
      }

      public function setTitre($titre) {
          $this->titre = $titre;
      }

      public function setDescription($description) {
          $this->description = $description;
      }

      public function getTheme() {
          return $this->theme;
      }

      public function setTheme($theme) {
          $this->theme = $theme;
      }
      
      public function getEtat() {
          return $this->etat;
      }

      public function setEtat($etat) {
          $this->etat = $etat;
      }

      
/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="problemes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
    * @ORM\OneToMany(targetEntity="Image", mappedBy="probleme")
    */
    protected $images;
    
     
    
    public function __construct()
    {
        $this->images= new ArrayCollection();
     }
    
    public function getUser() {
        return $this->user;
    }

    public function getImages() {
        return $this->images;
    }

    

    public function setUser($user) {
        $this->user = $user;
    }

    public function setImages($images) {
        $this->images = $images;
    }


} 