<?php
// src/AppBundle/Entity/User.php

namespace CrowdRiseBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */

// this entire  class is developed BY Badis NSIRI using FOS User Bundle
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

      /**
     *
     * @var string
     *
     * @ORM\Column(name="somme", type="integer", nullable=false)
     */
      
      private $somme=5000;     
      
      public function getSommeTemporaire() {
          return $this->sommeTemporaire;
      }

      public function setSommeTemporaire($sommeTemporaire) {
          $this->sommeTemporaire = $sommeTemporaire;
      }

            
   
      
    /**
     * @ORM\OneToMany(targetEntity="Projet", mappedBy="fos_user")
     */
    protected $projets;
    
    /**
    * @ORM\OneToMany(targetEntity="Probleme", mappedBy="fos_user")
    */
    protected $problemes;
    
    /**
    * @ORM\OneToMany(targetEntity="Idee", mappedBy="fos_user")
    */
    protected $idees;
    
  
    
    
    
    /**
    * @ORM\OneToMany(targetEntity="Transaction", mappedBy="fos_user")
    */
    protected $transactions;
    
    /**
    * @ORM\OneToMany(targetEntity="Mail", mappedBy="fos_user")
    */
    protected $mails;
    
    /**
    * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="fos_user")
    */
    protected $commentaires;
    
    
    
    /**
     * @ORM\Column(name="imageUserBlob", type="blob", nullable=false)
     */
    private $imageUserBlob;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;
    
    public function __construct()
    {
        parent::__construct();
        $this->projets = new ArrayCollection();
        $this->transactions= new ArrayCollection();
        $this->problemes= new ArrayCollection();
        $this->idees= new ArrayCollection();
        
        $this->mails= new ArrayCollection();
        $this->commentaires= new ArrayCollection();
    }
    public function getId() {
        return $this->id;
    }

    public function getSomme() {
        return $this->somme;
    }

    public function getProjets() {
        return $this->projets;
    }

    public function getProblemes() {
        return $this->problemes;
    }

    public function getIdees() {
        return $this->idees;
    }

    

    public function getTransactions() {
        return $this->transactions;
    }

    public function getMails() {
        return $this->mails;
    }

    public function getCommentaires() {
        return $this->commentaires;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setSomme($somme) {
        $this->somme = $somme;
    }

    public function setProjets($projets) {
        $this->projets = $projets;
    }

    public function setProblemes($problemes) {
        $this->problemes = $problemes;
    }

    public function setIdees($idees) {
        $this->idees = $idees;
    }

   

    public function setTransactions($transactions) {
        $this->transactions = $transactions;
    }

    public function setMails($mails) {
        $this->mails = $mails;
    }

    public function setCommentaires($commentaires) {
        $this->commentaires = $commentaires;
    }
    
 

    function getFile() {
        return $this->file;
    }

 

    function setFile($file) {
        $this->file = $file;
    }
    function getImageUserBlob() {
        return $this->imageUserBlob;
    }

    function setImageUserBlob($imageUserBlob) {
        $this->imageUserBlob = $imageUserBlob;
    }




}