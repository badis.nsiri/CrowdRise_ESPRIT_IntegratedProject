<?php

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrowdRiseBundle\Entity\CommentaireRepository")
 */ 
class Commentaire {

    /**

     * @ORM\Id

     * @ORM\Column(type="integer")

     * @ORM\GeneratedValue(strategy="AUTO")

     */
    protected $id;

    /**

     * @ORM\Column(type="string")

     */
    protected $body;

    /**

     * @ORM\Column(type="date")

     */
    protected $created_at;

    /**
     * Author of the comment
     *
     * @ORM\ManyToOne(targetEntity="user")
     * @var User
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="Projet", inversedBy="commentaires")
     * @ORM\JoinColumn(name="projet_id", referencedColumnName="id")
     */
    protected $projet;

    /**
     * @ORM\ManyToOne(targetEntity="Probleme", inversedBy="commentaires")
     * @ORM\JoinColumn(name="probleme_id", referencedColumnName="id")
     */
    protected $probleme;

    /**
     * @ORM\ManyToOne(targetEntity="Idee", inversedBy="commentaires")
     * @ORM\JoinColumn(name="idee_id", referencedColumnName="id")
     */
    protected $idee;

    function getId() {
        return $this->id;
    }

    function getBody() {
        return $this->body;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function getAuthor() {
        return $this->author;
    }

    function getProjet() {
        return $this->projet;
    }

    function getProbleme() {
        return $this->probleme;
    }

    function getIdee() {
        return $this->idee;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setBody($body) {
        $this->body = $body;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function setAuthor(User $author) {
        $this->author = $author;
    }

    function setProjet($projet) {
        $this->projet = $projet;
    }

    function setProbleme($probleme) {
        $this->probleme = $probleme;
    }

    function setIdee($idee) {
        $this->idee = $idee;
    }

}
