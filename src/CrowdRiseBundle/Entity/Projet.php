<?php
namespace CrowdRiseBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\CommentBundle\Entity\Comment as BaseComment;

/**
 *
 *
 * @ORM\Entity
 */
class Projet    {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", nullable=false)
     */
    private $titre;

    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     */
      private $description;
      
      /**
     *
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
      
      private $type;
      
     /**
     *
     * @var string
     *
     * @ORM\Column(name="theme", type="string", nullable=false)
     */
      
      private $theme;
      
      
     /**
     *
     * @var string
     *
     * @ORM\Column(name="tache", type="string", nullable=false)
     */
      
      private $tache;
      
      
       /**
     *
     * @var string
     *
     * @ORM\Column(name="montant", type="integer", nullable=false)
     */
      
      private $montant; 
      
      
      
       /**
     *
     * @var string
     *
     * @ORM\Column(name="don", type="integer", nullable=false)
     */
      
      private $don=0;
      
      
             /**
     *
     * @var string
     *
     * @ORM\Column(name="pret", type="integer", nullable=false)
     */
      
      private $pret=0;
      
      
      
             /**
     *
     * @var string
     *
     * @ORM\Column(name="recompense", type="integer", nullable=false)
     */
      
      private $recompense=0;
      
      
                /**
     *
     * @var string
     *
     * @ORM\Column(name="investissement", type="integer", nullable=false)
     */
      
      private $investissement=0;
      public function getSommeTemporaire() {
          return $this->sommeTemporaire;
      }

      public function setSommeTemporaire($sommeTemporaire) {
          $this->sommeTemporaire = $sommeTemporaire;
      }

           /**
     *
     * @var string
     *
     * @ORM\Column(name="sommeTemporaire", type="integer", nullable=false)
     */
      
      private $sommeTemporaire=0; 
      
      
      
      public function getType() {
          return $this->type;
      }

      public function setType($type) {
          $this->type = $type;
      }

            
      public function getId() {
          return $this->id;
      }

      public function getTitre() {
          return $this->titre;
      }

      public function getDescription() {
          return $this->description;
      }

      public function setId($id) {
          $this->id = $id;
      }

      public function setTitre($titre) {
          $this->titre = $titre;
      }

      public function setDescription($description) {
          $this->description = $description;
      }

      public function getTheme() {
          return $this->theme;
      }

      public function setTheme($theme) {
          $this->theme = $theme;
      }

      public function getTache() {
          return $this->tache;
      }

      public function getMontant() {
          return $this->montant;
      }

      public function getDon() {
          return $this->don;
      }

      public function getPret() {
          return $this->pret;
      }

      public function getRecompense() {
          return $this->recompense;
      }

      public function getInvestissement() {
          return $this->investissement;
      }

      public function setTache($tache) {
          $this->tache = $tache;
      }

      public function setMontant($montant) {
          $this->montant = $montant;
      }

      public function setDon($don) {
          $this->don = $don;
      }

      public function setPret($pret) {
          $this->pret = $pret;
      }

      public function setRecompense($recompense) {
          $this->recompense = $recompense;
      }

      public function setInvestissement($investissement) {
          $this->investissement = $investissement;
      }

 /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projets")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
    * @ORM\OneToMany(targetEntity="Transaction", mappedBy="projet")
    */
    protected $transactions;
    
    
    /**
    * @ORM\OneToMany(targetEntity="Image", mappedBy="projet")
    */
    protected $images;
    
    /**
    * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="projet")
    */
    protected $commentaires;
    
     public function __construct()
    {
        $this->transactions= new ArrayCollection();
        $this->images= new ArrayCollection();
        $this->commentaires= new ArrayCollection();
    }
    
    public function getUser() {
        return $this->user;
    }

    public function getTransactions() {
        return $this->transactions;
    }

    public function getImages() {
        return $this->images;
    }

    public function getCommentaires() {
        return $this->commentaires;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setTransactions($transactions) {
        $this->transactions = $transactions;
    }

    public function setImages($images) {
        $this->images = $images;
    }

    public function setCommentaires($commentaires) {
        $this->commentaires = $commentaires;
    }


}

