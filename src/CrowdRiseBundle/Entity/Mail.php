<?php

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 *
 *
 * @ORM\Entity
 */
class Mail {
    //put your code here

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="From", type="string", length=255, nullable=false)
     */
    private $from;

    /**
     * @var string
     *
     * @ORM\Column(name="Text", type="string", length=255, nullable=false)
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mails")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
     /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mails")
     * @ORM\JoinColumn(name="userParent_id", referencedColumnName="id")
     */
    protected $userParent;
   
    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getFrom() {
        return $this->from;
    }

    public function getText() {
        return $this->text;
    }

    public function getUser() {
        return $this->user;
    }

    public function getUserParent() {
        return $this->userParent;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setFrom($from) {
        $this->from = $from;
    }

    public function setText($text) {
        $this->text = $text;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setUserParent($userParent) {
        $this->userParent = $userParent;
    }



}