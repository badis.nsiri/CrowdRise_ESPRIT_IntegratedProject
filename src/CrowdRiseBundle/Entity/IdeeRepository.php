<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PatientRepository
 *
 * @author Win7pro
 */
class IdeeRepository extends EntityRepository {

    public function findIdeesByTheme($theme) {
        $qb = $this->createQuerybuilder('s');
        $qb->where("s.theme like :theme ")->setParameter('theme', $theme);

        return $qb->getQuery()->getResult();
    }

//     public function findAllPatient( ){
//        $qb=$this->createQuerybuilder('s');
//        $qb->orderBy('s.','DESC');
//        return $qb->getQuery()->getResult();
//    }
}