<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CrowdRiseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PatientRepository
 *
 * @author Win7pro
 */
class CommentaireRepository extends EntityRepository {

    public function findByIdProjet($id) {
        $qb = $this->createQuerybuilder('s');
        $qb->where("s.projet = :id ")->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }
  public function findByIdProbleme($id) {
        $qb = $this->createQuerybuilder('s');
        $qb->where("s.probleme = :id ")->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }
  public function findByIdIdee($id) {
        $qb = $this->createQuerybuilder('s');
        $qb->where("s.idee = :id ")->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }
//     public function findAllPatient( ){
//        $qb=$this->createQuerybuilder('s');
//        $qb->orderBy('s.','DESC');
//        return $qb->getQuery()->getResult();
//    }
}