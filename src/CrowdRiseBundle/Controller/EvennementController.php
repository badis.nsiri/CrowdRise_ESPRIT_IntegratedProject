<?php

namespace CrowdRiseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrowdRiseBundle\Form\EvennementForm;
use CrowdRiseBundle\Entity\Evennement;
use Symfony\Component\HttpFoundation\StreamedResponse;

class EvennementController extends Controller {

    public function indexEvennementAction($id) {
        $em = $this->getDoctrine()->getManager();
        $probleme = $em->getRepository("CrowdRiseBundle:Evennement")->find($id);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexEvennement.html.twig', array("m" => $probleme));
    }

    public function indexFormulaireEvennementAction() {
        $entity = new Evennement();

        $user = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(new EvennementForm(), $entity);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $stream = fopen($entity->getFile(), 'rb');
            $entity->setImageEventBlob(stream_get_contents($stream));

            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('list_Evenement'));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireEvennement.html.twig', array('f' => $form->createView()));
    }

    public function indexModifierEvennementAction($id) {

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Evennement')->find($id);
        $form = $this->createForm(new EvennementForm, $modele);
        $request = $this->get('request');

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl('list_Evenement'));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierEvennement.html.twig', array('f' => $form->createView()));
    }

    function supprimerEvenementAction($id) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->getrepository('CrowdRiseBundle:Evennement')->find($id);

        $em->remove($model);
        $em->flush();
            return $this->redirect($this->generateUrl('list_Evenement'));
     
    }

     

    public function AfficheImageEvenementAction($id) {
        $em = $this->getDoctrine()->getManager();

        $image_obj = $em->getRepository('CrowdRiseBundle:Evennement')->find($id);

        $photo = $image_obj->getImageEventBlob();

        $response = new StreamedResponse(function () use ($photo) {

            echo stream_get_contents($photo);
        });

        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;
    }
     public function rechercherEvenementAction() {
        $em = $this->getDoctrine()->getManager();

        $evenements = $em->getRepository('CrowdRiseBundle:Evennement')->findAll();
        $request = $this->get('request');
        $motcle = " ";
        if (($request->getMethod() === "POST")) {
            $motcle = $request->request->get('q');
            $evenements = $em->getRepository('CrowdRiseBundle:Evennement')->findEvenementsByTheme($motcle);
            return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig', array('evenements' => $evenements));
        }


        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenementByTheme.html.twig', array('evenements' => $evenements));
    }
    
    public function listEvenementAction() {


        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository
                        ('CrowdRiseBundle:Evennement')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:listEvenements.html.twig', array(
                    "evenements" => $evenements
        ));
    }

}
    