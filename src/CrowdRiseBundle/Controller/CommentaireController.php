<?php

namespace CrowdRiseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrowdRiseBundle\Entity\Commentaire;
use CrowdRiseBundle\Form\CommentaireType;

/**
 * Comment controller.
 *
 */
class CommentaireController extends Controller {

    public function showCommentaireByProjetAction($id) {



        $em = $this->getDoctrine()->getManager();
        $coms = $em->getRepository
                        ('CrowdRiseBundle:Commentaire')->findByIdProjet($id);

        return $this->render('CrowdRiseBundle:Commentaire:show.html.twig', array(
                    "coms" => $coms
        ));
    }

    public function showCommentaireByProblemeAction($id) {



        $em = $this->getDoctrine()->getManager();
        $coms = $em->getRepository
                        ('CrowdRiseBundle:Commentaire')->findByIdProbleme($id);

        return $this->render('CrowdRiseBundle:Commentaire:show.html.twig', array(
                    "coms" => $coms
        ));
    }

    public function showCommentaireByIdeeAction($id) {



        $em = $this->getDoctrine()->getManager();
        $coms = $em->getRepository
                        ('CrowdRiseBundle:Commentaire')->findByIdIdee($id);

        return $this->render('CrowdRiseBundle:Commentaire:show.html.twig', array(
                    "coms" => $coms
        ));
    }

    public function DeleteCommentaireByIdeeAction($id, $id2) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->geTrepository('CrowdRiseBundle:Commentaire')->find($id);

        $em->remove($model);
        $em->flush();


        $em2 = $this->getDoctrine()->getManager();
        $idee = $em2->getRepository("CrowdRiseBundle:Idee")->find($id2);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig', array("m" => $idee));


//       return $this->redirect($this->generateUrl('idee', array('m' =>$idee)));
    }

    public function DeleteCommentaireByProjetAction($id, $id2) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->geTrepository('CrowdRiseBundle:Commentaire')->find($id);

        $em->remove($model);
        $em->flush();


        $em2 = $this->getDoctrine()->getManager();
        $idee = $em2->getRepository("CrowdRiseBundle:Projet")->find($id2);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig', array("m" => $idee));


//       return $this->redirect($this->generateUrl('idee', array('m' =>$idee)));
    }

    public function DeleteCommentaireByProblemeAction($id, $id2) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->geTrepository('CrowdRiseBundle:Commentaire')->find($id);

        $em->remove($model);
        $em->flush();


        $em2 = $this->getDoctrine()->getManager();
        $idee = $em2->getRepository("CrowdRiseBundle:Probleme")->find($id2);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig', array("m" => $idee));


//       return $this->redirect($this->generateUrl('idee', array('m' =>$idee)));
    }

    public function AjouterCommentaireIdeeAction($id) {

        $em2 = $this->getDoctrine()->getManager();
        $idee = $em2->getRepository("CrowdRiseBundle:Idee")->find($id);

        $model = new Commentaire();

        $request = $this->get('request');
        $user = $this->container->get('security.context')->getToken()->getUser();
        $ky = $request->get('CIdee');
        $model->setCreated_at(new \DateTime("now"));
        $model->setAuthor($user);
        $model->setIdee($idee);
        $model->setBody($ky);
        $em = $this->getDoctrine()->getManager();
        $em->persist($model);
        $em->flush();


        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig', array("m" => $idee));
    }

    public function AjouterCommentaireProjetAction($id) {

        $em2 = $this->getDoctrine()->getManager();
        $idee = $em2->getRepository("CrowdRiseBundle:Projet")->find($id);

        $model = new Commentaire();

        $request = $this->get('request');
        $user = $this->container->get('security.context')->getToken()->getUser();
        $ky = $request->get('CIdee');
        $model->setCreated_at(new \DateTime("now"));
        $model->setAuthor($user);
        $model->setProjet($idee);
        $model->setBody($ky);
        $em = $this->getDoctrine()->getManager();
        $em->persist($model);
        $em->flush();


        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig', array("m" => $idee));
    }

    public function AjouterCommentaireProblemeAction($id) {

        $em2 = $this->getDoctrine()->getManager();
        $idee = $em2->getRepository("CrowdRiseBundle:Probleme")->find($id);

        $model = new Commentaire();

        $request = $this->get('request');
        $user = $this->container->get('security.context')->getToken()->getUser();
        $ky = $request->get('CIdee');
        $model->setCreated_at(new \DateTime("now"));
        $model->setAuthor($user);
        $model->setProbleme($idee);
        $model->setBody($ky);
        $em = $this->getDoctrine()->getManager();
        $em->persist($model);
        $em->flush();


        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig', array("m" => $idee));
    }

    public function ModifierCommentaireProjetAction($id,$id2) {

     
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Commentaire')->find($id);
        $form = $this->createForm(new CommentaireType, $modele);
        $request = $this->get('request');

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            $em2 = $this->getDoctrine()->getManager();
            $projet = $em2->getRepository("CrowdRiseBundle:Projet")->find($id2);
            return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig', array("m" => $projet));
        }
        return $this->render('CrowdRiseBundle:Commentaire:update.html.twig', array('f' => $form->createView()));
    }
    
     public function ModifierCommentaireProblemeAction($id,$id2) {

     
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Commentaire')->find($id);
        $form = $this->createForm(new CommentaireType, $modele);
        $request = $this->get('request');

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            $em2 = $this->getDoctrine()->getManager();
            $probleme = $em2->getRepository("CrowdRiseBundle:Probleme")->find($id2);
            return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig', array("m" => $probleme));
        }
        return $this->render('CrowdRiseBundle:Commentaire:update.html.twig', array('f' => $form->createView()));
    }
    
    public function ModifierCommentaireIdeeAction($id,$id2) {

     
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Commentaire')->find($id);
        $form = $this->createForm(new CommentaireType, $modele);
        $request = $this->get('request');

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            $em2 = $this->getDoctrine()->getManager();
            $idee = $em2->getRepository("CrowdRiseBundle:Idee")->find($id2);
            return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig', array("m" => $idee));
        }
        return $this->render('CrowdRiseBundle:Commentaire:update.html.twig', array('f' => $form->createView()));
    }

}
