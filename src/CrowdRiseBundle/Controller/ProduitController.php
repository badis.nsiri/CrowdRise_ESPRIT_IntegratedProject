<?php

namespace PiDev\ClientBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PiDev\ClientBundle\Entity\User;
use PiDev\ClientBundle\Form\ProduitRechercheForm;
use PiDev\ClientBundle\Form\ImageForm;
use PiDev\ClientBundle\Controller\ImageController;
use PiDev\ClientBundle\Entity\Produit;
use PiDev\ClientBundle\Entity\Image;
use PiDev\ClientBundle\Form\ProduitType;


/**
 * Produit controller.
 *
 */
class ProduitController extends Controller
{

    /**
     * Lists all Produit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PiDevClientBundle:Produit')->findAll();

        return $this->render('PiDevClientBundle:Produit:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Produit entity.
     *
     */
    public function createAction(Request $request)
    {
       $entity = new Produit();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
         $entity->upload();
         $iduser=$this->container->get('security.context')->getToken()->getUser();
        // $em=$this->getDoctrine()->getManager();
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
             $entity->setUser($iduser);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('produit_show', array('id' => $entity->getId())));
        }

        return $this->render('PiDevClientBundle:Produit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
        
    }

    /**
     * Creates a form to create a Produit entity.
     *
     * @param Produit $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Produit $entity)
    {
        $form = $this->createForm(new ProduitType(), $entity, array(
            'action' => $this->generateUrl('produit_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Produit entity.
     *
     */
    public function newAction()
    {
        $entity = new Produit();
        $image = new Image();
        $form   = $this->createCreateForm($entity);
        $form1 = $this->createForm(new ImageForm(), $image);
        
               
       
         //$this->container->get('security.context')->getToken()->getCategorie();

        return $this->render('PiDevClientBundle:Produit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'form1'   => $form1->createView(),
            
            
        ));
    }

    /**
     * Finds and displays a Produit entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:Produit:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Produit entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produit entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PiDevClientBundle:Produit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Produit entity.
    *
    * @param Produit $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Produit $entity)
    {
        $form = $this->createForm(new ProduitType(), $entity, array(
            'action' => $this->generateUrl('produit_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Produit entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiDevClientBundle:Produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('produit_edit', array('id' => $id)));
        }

        return $this->render('PiDevClientBundle:Produit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Produit entity.
     *
     */
    public function deleteAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $modeles=$em->getRepository('PiDevClientBundle:Produit')->find($id);
        $em->remove($modeles);
        $em->flush();

        return $this->redirect($this->generateUrl('produit'));
    }

    /**
     * Creates a form to delete a Produit entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produit_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    function listProdAction(){
        
       
           $em=$this->getDoctrine()->getManager();
        /*getRespository  prendre le nom de l'entité */
        $modeles=$em->getRepository('PiDevClientBundle:Produit')->findAll();
      
        return $this->render('PiDevClientBundle:Produit:list.html.twig',array('m'=>$modeles));
    }
    function rechercherProdAction()
    {
             $em = $this->getDoctrine()->getManager();
$modeles = $em->getRepository
        ('PiDevClientBundle:Produit')->findAll();
        $request = $this->get('request');
        if ($request->getMethod()=="POST")
        {
$search=$request->get('search');
$modeles=$em->getRepository
        ('PiDevClientBundle:Produit')->
                findBy(array("nom"=>$search));  
        
    }
    return $this->render
('PiDevClientBundle:Produit:recherche.html.twig'
                ,array("mod"=>$modeles));
    }
    
    function listProduserAction(){
    $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PiDevClientBundle:Produit')->findByuser($user);
        return $this->render('PiDevClientBundle:Produit:listuser.html.twig',array(
            'entities'    => $entities,
    ));
}


public function listerAction()
{
	$form = $this->container->get('form.factory')->create(new ProduitRechercheForm());
	
	return $this->container->get('templating')->renderResponse('PiDevClientBundle:Produit:lister.html.twig', array(
		'produit' => $produit,
		'form' => $form->createView()
	));
}

public function rechercherAction()
{        
    $request = $this->container->get('request');
 
    if($request->isXmlHttpRequest())
    {
        $motcle = '';
        $motcle = $request->request->get('motcle');
 
        $em = $this->container->get('doctrine')->getEntityManager();
 
        if($motcle != '')
        {
               $qb = $em->createQueryBuilder();
 
               $qb->select('t')
                  ->from('PiDevClientBundle:Produit', 't')
                  ->where("t.nom LIKE :motcle")
                  ->orderBy('t.nom', 'ASC')
                  ->setParameter('motcle', '%'.$motcle.'%');
 
               $query = $qb->getQuery();              
               $produits = $query->getResult();
        }
        else {
            $produits = $em->getRepository('PiDevClientBundle:Produit')->findAll();
        }
 
        return $this->container->get('templating')->renderResponse('PiDevClientBundle:Produit:lister.html.twig', array(
            'produits' => $produits
            ));
    }
    else {
        return $this->listerAction();
    }
}
}

    
