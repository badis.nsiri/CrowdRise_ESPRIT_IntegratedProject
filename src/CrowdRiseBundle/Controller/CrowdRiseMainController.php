<?php

namespace CrowdRiseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrowdRiseBundle\Form\ProblemeType;
use CrowdRiseBundle\Entity\Probleme;
use CrowdRiseBundle\Form\IdeeForm;
use CrowdRiseBundle\Entity\Idee;
use CrowdRiseBundle\Form\ProjetType;
use CrowdRiseBundle\Entity\Projet;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CrowdRiseMainController extends Controller {

    public function indexFrontOfficeAction() {
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig');
    }

    public function indexProblemeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $probleme = $em->getRepository("CrowdRiseBundle:Probleme")->find($id);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig', array("m" => $probleme));
    }

    public function indexFormulaireProblemeAction() {
        $model = new Probleme();
        $modelForm = new ProblemeType ();
        $form = $this->createForm($modelForm, $model);
        $request = $this->get('request');
        $user = $this->get('security.context')->getToken()->getUser();

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $model->setUser($user);
            $em->persist($model);
            $em->flush();
            return $this->redirect($this->generateUrl('list_probleme'));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProbleme.html.twig', array('f' => $form->createView()));
    }

    public function indexModifierProblemAction($id) {

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Probleme')->find($id);
        $form = $this->createForm(new ProblemeType, $modele);
        $request = $this->get('request');

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl('list_probleme'));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProblem.html.twig', array('f' => $form->createView()));
    }

    function supprimerProblemeAction($id) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->geTrepository('CrowdRiseBundle:Probleme')->find($id);

        $em->remove($model);
        $em->flush();
        return $this->redirect($this->generateUrl('list_probleme'));
    }


    public function showAllIdeeAction() {


        $em = $this->getDoctrine()->getManager();
        $idees = $em->getRepository
                        ('CrowdRiseBundle:Idee')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllIdee.html.twig', array(
                    "idees" => $idees
        ));
    }

    public function listIdeesAction() {


        $em = $this->getDoctrine()->getManager();
        $idees = $em->getRepository
                        ('CrowdRiseBundle:Idee')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:listIdees.html.twig', array(
                    "idees" => $idees
        ));
    }

    public function listProblemesAction() {


        $em = $this->getDoctrine()->getManager();
        $problemes = $em->getRepository
                        ('CrowdRiseBundle:Probleme')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:listProblemes.html.twig', array(
                    "problemes" => $problemes
        ));
    }

    public function showAllProjetAction() {


        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository
                        ('CrowdRiseBundle:Projet')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllProjet.html.twig', array(
                    "projets" => $projets
        ));
    }

    public function showAllEvenementAction() {


        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository
                        ('CrowdRiseBundle:Evennement')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllEvenement.html.twig', array(
                    "evenements" => $evenements
        ));
    }

    public function showAllProblemeAction() {


        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository
                        ('CrowdRiseBundle:probleme')->findAll();

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllProbleme.html.twig', array(
                    "problemes" => $evenements
        ));
    }

    public function indexFormulaireIdeeAction() {
        $entity = new Idee();
        $user = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(new IdeeForm(), $entity);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $stream = fopen($entity->getFile(), 'rb');
            $entity->setImageIdeeBlob(stream_get_contents($stream));

            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('list_idee'));
        }

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireIdee.html.twig', array(
                    'f' => $form->createView(),
        ));
    }    
    
    public function indexModifierIdeeAction($id) {

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Idee')->find($id);
        $form = $this->createForm(new IdeeForm, $modele);
        
        $request = $this->get('request_stack')->getCurrentRequest();

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $stream = fopen($modele->getFile(), 'rb');
            $modele->setImageIdeeBlob(stream_get_contents($stream));
            
            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl('list_idee'));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierIdee.html.twig', array('f' => $form->createView()));
    }

    function supprimerIdeeAction($id) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->geTrepository('CrowdRiseBundle:Idee')->find($id);

        $em->remove($model);
        $em->flush();
            return $this->redirect($this->generateUrl('list_idee'));
    }

    public function indexIdeeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $probleme = $em->getRepository("CrowdRiseBundle:Idee")->find($id);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexIdee.html.twig', array("m" => $probleme));
    }

    public function indexFormulaireProjetAction() {
        $model = new Projet();
        $modelForm = new ProjetType ();
        $form = $this->createForm($modelForm, $model);

        $request = $this->get('request');
        $form->handleRequest($request);
        $user = $this->container->get('security.context')->getToken()->getUser();



        if ($request->getMethod() == "POST") {
            $model->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($model);
            $em->flush();
            $this->redirect($this->generateUrl('crowd_rise_frontOffice'));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFormulaireProjet.html.twig', array('f' => $form->createView()));
    }

    public function indexProjetAction() {

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjet.html.twig');
    }

    public function indexProjetByIdAction($id) {
        $em = $this->getDoctrine()->getManager();
        $probleme = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetById.html.twig', array("m" => $probleme));
    }

    public function indexModifierProjetAction($id) {

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getrepository('CrowdRiseBundle:Projet')->find($id);
        $form = $this->createForm(new ProjetType, $modele);
        $request = $this->get('request');

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($modele);
            $em->flush();
            return $this->redirect($this->generateUrl("crowd_rise_frontOffice"));
        }
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexModifierProjet.html.twig', array('f' => $form->createView()));
    }

    function supprimerProjetAction($id) {
        $em = $this->getDoctrine()->getManager();
        // findAll pour faire l'affichage 
        $model = $em->geTrepository('CrowdRiseBundle:Projet')->find($id);

        $em->remove($model);
        $em->flush();
        return $this->redirect($this->generateUrl("crowd_rise_frontOffice"));
    }

    public function erreurLoginAction() {
        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:erreurLogin.html.twig');
    }

    public function loginAcceuilAction() {
        return $this->render('CrowdRiseBundle:Security:loginAcceuil.html.twig');
    }

    public function profilAction() {
        return $this->render('CrowdRiseBundle:Profile:indexProfile.html.twig');
    }

    public function ProblemeResAction($id) {
        $em = $this->getDoctrine()->getManager();
        $probleme = $em->getRepository("CrowdRiseBundle:Probleme")->find($id);
        if ($probleme->getEtat() == "Non Resolu") {
            $probleme->setEtat("Resolu");
            $em->persist($probleme);
            $em->flush();
        } else if ($probleme->getEtat() == "Resolu") {
            $probleme->setEtat("Non Resolu");
            $em->persist($probleme);
            $em->flush();
        }
        //$em->persist($probleme);  

        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProbleme.html.twig', array("m" => $probleme));
    }

    public function searchUserAction() {


        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig');
//         $request = $this->container->get('request');
//
//        if ($request->isXmlHttpRequest()) {
//            $motcle = '';
//            $motcle = $request->request->get('motcle');
//
//            $em = $this->container->get('doctrine')->getEntityManager();
//
//            if ($motcle != '') {
//                $qb = $em->createQueryBuilder();
//
//                $qb->select('t')
//                        ->from('CrowdRiseBundle:fos_user', 't')
//                        ->where("t.username LIKE :motcle  or t.email LIKE :motcle ")
//                        ->orderBy('t.username', 'ASC')
//                        ->setParameter('motcle', '%' . $motcle . '%');
//
//                $query = $qb->getQuery();
//                $taches = $query->getResult();
//            } else {
//                $taches = $em->getRepository('CrowdRiseBundle:fos_user')->findAll();
//            }
//
//            return $this->container->get('templating')->renderResponse('CrowdRiseBundle:CrowdRiseFrontOffice:rechercheUtilisateur.html.twig', array(
//                        'users' => $users
//            ));
//        } else {
//            return $this->listeAction();
//        }
    }
// this function is develpped by Badis NSIRI
    public function AfficheImageIdeeAction($id) {
        $em = $this->getDoctrine()->getManager();

        $image_obj = $em->getRepository('CrowdRiseBundle:Idee')->find($id);

        $photo = $image_obj->getImageIdeeBlob();

        $response = new StreamedResponse(function () use ($photo) {

            echo stream_get_contents($photo);
        });

        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;
    }

    // this function is develpped by Badis NSIRI
    public function AfficheImageUserAction($id) {
        $em = $this->getDoctrine()->getManager();

        $image_obj = $em->getRepository('CrowdRiseBundle:User')->find($id);

        $photo = $image_obj->getImageUserBlob();

        $response = new StreamedResponse(function () use ($photo) {

            echo stream_get_contents($photo);
        });

        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;
    }

    public function rechercherIdeeAction() {
        $em = $this->getDoctrine()->getManager();

        $idees = $em->getRepository('CrowdRiseBundle:Idee')->findAll();
        $request = $this->get('request');
        $motcle = " ";
        if (($request->getMethod() === "POST")) {
            $motcle = $request->request->get('q');
            $idees = $em->getRepository('CrowdRiseBundle:Idee')->findIdeesByTheme($motcle);
            return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllIdeeByTheme.html.twig', array('idees' => $idees));
        }


        return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:showAllIdeeByTheme.html.twig', array('idees' => $idees));
    }

}
