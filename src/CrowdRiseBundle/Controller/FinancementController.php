<?php


 namespace CrowdRiseBundle\Controller;
use CrowdRiseBundle\Entity\Projet;
use CrowdRiseBundle\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// this class is develpped by Badis NSIRI
class FinancementController extends Controller {
   
    public function DonAction($id,$iduser){
        
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
        $user = $em->getRepository("CrowdRiseBundle:User")->find($iduser);
       
       
        $request=$this->get('request');
        if($request->getMethod()=="POST"){
            $don=$projet->getDon();
            $SommeTemporaire=$projet->getSommeTemporaire();
            $SommeUser=$user->getSomme();
            
            if($SommeUser>=$don)
                {
            $projet->setSommeTemporaire($don+$SommeTemporaire);
            $user->setSomme($SommeUser-$don); 
            $emProjet = $this->getDoctrine()->getManager();
            $emProjet->persist($projet);
            $emProjet->flush();
            $emUser = $this->getDoctrine()->getManager();
            $emUser->persist($user);
            $emUser->flush();
            $this->redirect($this->generateUrl('crowd_rise_frontOffice'));
            
              }
        
        else{
             $em = $this->getDoctrine()->getManager();
             $probleme = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
             return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetByIdAvecErreur.html.twig',array("m"=>$probleme));
            }
        }
          /******/
         
           return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig'); 
        
    }
    public function PretAction($id,$iduser){
           $em = $this->getDoctrine()->getManager();
          $projet = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
          
          $user = $em->getRepository("CrowdRiseBundle:User")->find($iduser); 
       
            
           
       
        $request=$this->get('request');
        if($request->getMethod()=="POST"){
            
            $pret=$projet->getPret();
            $SommeTemporaire=$projet->getSommeTemporaire();
            $SommeUser=$user->getSomme();
           
            if($SommeUser>=$pret){
            $projet->setSommeTemporaire($pret+$SommeTemporaire);
            $user->setSomme($SommeUser-$pret);
            
            
            $emProjet = $this->getDoctrine()->getManager();
            $emProjet->persist($projet);
            $emProjet->flush();
            $emUser = $this->getDoctrine()->getManager();
            $emUser->persist($user);
            $emUser->flush();
            
            $this->redirect($this->generateUrl('crowd_rise_frontOffice'));
            }
            else{
             $em = $this->getDoctrine()->getManager();
             $probleme = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
             return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetByIdAvecErreur.html.twig',array("m"=>$probleme));
            }
        }
          /******/
       return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig');
     //   return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:test.html.twig',array('id'=>$id,'iduser'=>$iduser));
        
    }
    public function RecompenseAction($id,$iduser){
           $em = $this->getDoctrine()->getManager();
          $projet = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
          
          $user = $em->getRepository("CrowdRiseBundle:User")->find($iduser); 
       
            
           
       
        $request=$this->get('request');
        if($request->getMethod()=="POST"){
            $recompense=$projet->getRecompense();
            $SommeTemporaire=$projet->getSommeTemporaire();
            $SommeUser=$user->getSomme();
            if($SommeUser>$recompense) {
            $projet->setSommeTemporaire($recompense+$SommeTemporaire);
            $user->setSomme($SommeUser-$recompense);
            
            
            $emProjet = $this->getDoctrine()->getManager();
            $emProjet->persist($projet);
            $emProjet->flush();
            
            $emUser = $this->getDoctrine()->getManager();
            $emUser->persist($user);
            $emUser->flush();
            
            $this->redirect($this->generateUrl('crowd_rise_frontOffice'));
        }
             else{
             $em = $this->getDoctrine()->getManager();
             $probleme = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
             return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetByIdAvecErreur.html.twig',array("m"=>$probleme));
            }
        }
          /******/
         
              return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig');
        
    }
    public function InvestissementAction(){
         $em = $this->getDoctrine()->getManager();
          $projet = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
          
          $user = $em->getRepository("CrowdRiseBundle:User")->find($iduser); 
       
            
           
       
        $request=$this->get('request');
        if($request->getMethod()=="POST"){
            $investissement=$projet->getInvestissement();
            $SommeTemporaire=$projet->getSommeTemporaire();
            $SommeUser=$user->getSomme();
            
            if($SommeUser>=$investissement){
            $projet->setSommeTemporaire($investissement+$SommeTemporaire);
            $SommeUser=$user->getSomme();
            $user->setSomme($SommeUser-$investissement);
            
            
            $emProjet = $this->getDoctrine()->getManager();
            $emProjet->persist($projet);
            $emProjet->flush();
            
            $emUser = $this->getDoctrine()->getManager();
            $emUser->persist($user);
            $emUser->flush();
            
            $this->redirect($this->generateUrl('crowd_rise_frontOffice'));
        }
            else{
             $em = $this->getDoctrine()->getManager();
             $probleme = $em->getRepository("CrowdRiseBundle:Projet")->find($id);
             return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexProjetByIdAvecErreur.html.twig',array("m"=>$probleme));
            }
        }
          /******/
         
           return $this->render('CrowdRiseBundle:CrowdRiseFrontOffice:indexFrontOffice.html.twig');
        
    }
}
