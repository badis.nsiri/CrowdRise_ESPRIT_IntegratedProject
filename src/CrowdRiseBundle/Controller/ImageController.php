<?php

namespace CrowdRiseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrowdRiseBundle\Entity\Image;
use CrowdRiseBundle\Form\ImageForm;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ImageController extends Controller {
  public function uploadAction()

 {
      
 $im=new Image();

 $form = $this->createForm(new ImageForm(), $im);
  

 $request = $this->get('request_stack')->getCurrentRequest();

 $form->handleRequest($request);

 if ($form->isValid()) 

 {

 $em = $this->getDoctrine()->getManager();

 $stream = fopen($im->getFile(),'rb');

 $im->setImg(stream_get_contents($stream));

 $em->persist($im);

 $em->flush();

 return $this->render('CrowdRiseBundle:image:index.html.twig', array());

 }

 return $this->render('CrowdRiseBundle:image:upload.html.twig',array('Form'=>$form->createView()));

 }
 public function listAction()

 {

 $em = $this->getDoctrine()->getManager();

 $image=$em->getRepository('CrowdRiseBundle:Image')->findAll();

 return $this->render('CrowdRiseBundle:image:list.html.twig',array('images'=>$image));

 
 
 }
 
 
 
 public function afficheAction($id)

 {

 $em = $this->getDoctrine()->getManager();

 $image=$em->getRepository('CrowdRiseBundle:Image')->find($id);

 return $this->render('CrowdRiseBundle:image:affiche.html.twig',array('images'=>$image));

 }
 public function photoAction($id)

 {

 $em = $this->getDoctrine()->getManager();

 $image_obj = $em->getRepository('MyAppEspritBundle:Image')->find($id);

 $photo=$image_obj->getImg();

 $response = new StreamedResponse(function () use ($photo) {

 echo stream_get_contents($photo);

 });

 $response->headers->set('Content-Type', 'image/jpeg');

 return $response;

 }
 
 
}
